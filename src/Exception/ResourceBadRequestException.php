<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 8:33 AM
 */

namespace MiamiOH\BannerApi\Exception;


use MiamiOH\BannerApi\Resource\ResourceErrorCollection;

class ResourceBadRequestException extends \Exception
{
    /**
     * @var ResourceErrorCollection
     */
    private $errors;

    public function __construct(ResourceErrorCollection $errors, $code = 0, \Exception $previous = null) {

        $this->errors = $errors;

        parent::__construct((string) $errors, $code, $previous);
    }

}