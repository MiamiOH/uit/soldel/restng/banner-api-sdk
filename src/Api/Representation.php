<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 11:11 AM
 */

namespace MiamiOH\BannerApi\Api;


class Representation
{

    /**
     * @var string
     */
    private $mediaType;
    /**
     * @var array
     */
    private $methods;

    public function __construct(string $mediaType, array $methods)
    {
        $this->mediaType = $mediaType;
        $this->methods = $methods;
    }

    public static function fromArray(array $data): Representation
    {
        return new Representation($data['X-Media-Type'], $data['methods']);
    }

    public function getMediaType(): string
    {
        return $this->mediaType;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }
}