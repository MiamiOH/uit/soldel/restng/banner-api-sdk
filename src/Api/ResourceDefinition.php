<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 10:27 AM
 */

namespace MiamiOH\BannerApi\Api;

class ResourceDefinition
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var RepresentationCollection
     */
    private $representations;

    public function __construct(string $name, RepresentationCollection $representations)
    {
        $this->name = $name;
        $this->representations = $representations;
    }

    public static function fromArray(array $data): ResourceDefinition
    {
        $representations = [];

        foreach ($data['representations'] as $representation) {
            $representations[] = Representation::fromArray($representation);
        }

        return new ResourceDefinition($data['name'], RepresentationCollection::fromArray($representations));
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRepresentations(): RepresentationCollection
    {
        return $this->representations;
    }
}