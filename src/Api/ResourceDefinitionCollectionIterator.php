<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 12:40 PM
 */

namespace MiamiOH\BannerApi\Api;


use phpDocumentor\Reflection\Types\Resource;

class ResourceDefinitionCollectionIterator implements \Iterator
{

    private $current = 0;

    /** @var  array */
    private $definitions;

    public function __construct(ResourceDefinitionCollection $collection)
    {
        $this->definitions = $collection->toArray();
    }

    public function current(): ResourceDefinition
    {
        return $this->definitions[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->definitions[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}