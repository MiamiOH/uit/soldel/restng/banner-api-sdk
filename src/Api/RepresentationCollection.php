<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 11:11 AM
 */

namespace MiamiOH\BannerApi\Api;


use Traversable;

class RepresentationCollection implements \Countable, \IteratorAggregate
{

    /**
     * @var array
     */
    private $representations;

    public function __construct(array $representations)
    {
        $this->ensure($representations);
        $this->representations = $representations;
    }

    private function ensure(array $representations): void
    {
        foreach ($representations as $representation) {
            if (!is_a($representation, Representation::class)) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public static function fromArray(array $representations): RepresentationCollection
    {
        return new RepresentationCollection($representations);
    }

    public function count(): int
    {
        return count($this->representations);
    }

    public function toArray(): array
    {
        return $this->representations;
    }

    public function getIterator(): RepresentationCollectionIterator
    {
        return new RepresentationCollectionIterator($this);
    }
}