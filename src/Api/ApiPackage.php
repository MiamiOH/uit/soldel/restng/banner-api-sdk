<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 4:19 PM
 */

namespace MiamiOH\BannerApi\Api;


class ApiPackage
{
    public const STUDENT = 'StudentApi';
    public const INTEGRATION = 'IntegrationApi';

    /**
     * @var string
     */
    private $name;
    /**
     * @var ResourceDefinitionCollection
     */
    private $definitions;

    public function __construct(string $name, ResourceDefinitionCollection $definitions)
    {
        $this->name = $name;
        $this->definitions = $definitions;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getResourceDefinitions(): ResourceDefinitionCollection
    {
        return $this->definitions;
    }
}