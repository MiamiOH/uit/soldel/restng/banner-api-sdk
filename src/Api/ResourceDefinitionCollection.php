<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 10:27 AM
 */

namespace MiamiOH\BannerApi\Api;

use Traversable;

class ResourceDefinitionCollection implements \Countable, \IteratorAggregate
{

    /**
     * @var array
     */
    private $definitions;

    public function __construct(array $definitions)
    {
        $this->ensure($definitions);
        $this->definitions = $definitions;
    }

    public static function fromArray(array $data): ResourceDefinitionCollection
    {
        $definitions = [];

        foreach ($data as $definitionData) {
            $definitions[] = ResourceDefinition::fromArray($definitionData);
        }

        return new ResourceDefinitionCollection($definitions);
    }

    private function ensure(array $definitions): void
    {
        foreach ($definitions as $definition) {
            if (!is_a($definition, ResourceDefinition::class)) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public function count(): int
    {
        return count($this->definitions);
    }

    public function toArray(): array
    {
        return $this->definitions;
    }

    public function getIterator(): ResourceDefinitionCollectionIterator
    {
        return new ResourceDefinitionCollectionIterator($this);
    }
}