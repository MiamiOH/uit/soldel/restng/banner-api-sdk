<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 8:10 AM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Exception\InvalidCollectionEntryException;
use Traversable;

class GuidCollection implements \Countable, \IteratorAggregate
{

    /**
     * @var Guid[]
     */
    private $guids;

    public static function fromAssocOfIds(array $guids): GuidCollection
    {
        $finalGuids = [];

        foreach ($guids as $guid) {
            $finalGuids[] = Guid::fromString($guid['id']);
        }

        return new GuidCollection($finalGuids);
    }

    public static function fromArrayOfIds(array $guids): GuidCollection
    {
        $finalGuids = [];

        foreach ($guids as $guid) {
            $finalGuids[] = Guid::fromString($guid);
        }

        return new GuidCollection($finalGuids);
    }

    public static function fromArrayOfGuids(array $guids) : GuidCollection
    {
        return new GuidCollection($guids);
    }

    public function __construct(array $guids)
    {
        $this->ensure($guids);
        $this->guids = $guids;
    }

    private function ensure(array $guids): void
    {
        foreach ($guids as $guid) {
            if (!$guid instanceof Guid) {
                throw new InvalidCollectionEntryException();
            }
        }
    }

    public function toArray(): array
    {
        return $this->guids;
    }

    public function collect(): array
    {
        $data = [];

        foreach ($this->guids as $guid) {
            $data[] = ['id' => $guid];
        }

        return $data;
    }

    public function count(): int
    {
        return count($this->guids);
    }

    public function getIterator(): GuidCollectionIterator
    {
        return new GuidCollectionIterator($this);
    }
}