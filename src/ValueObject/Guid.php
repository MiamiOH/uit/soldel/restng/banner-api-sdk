<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 3:56 PM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Exception\InvalidGuidException;

class Guid implements \JsonSerializable
{
    private $value;

    public static function fromString(string $value): Guid
    {
        return new Guid($value);
    }

    public function __construct(string $value)
    {
        $this->ensure($value);
        $this->value = $value;
    }

    private function ensure(string $value): void
    {
        if (!preg_match('/^\{?[a-f\d]{8}-(?:[a-f\d]{4}-){3}[a-f\d]{12}\}?$/i', $value)) {
            throw new InvalidGuidException();
        }
    }

    public function __toString() : string
    {
        return $this->value;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }
}