<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/12/17
 * Time: 7:15 AM
 */

namespace MiamiOH\BannerApi;


class GuidCollectionIterator implements \Iterator
{

    protected $current = 0;

    /**
     * @var Guid[]
     */
    protected $models;

    public function __construct(GuidCollection $collection)
    {
        $this->models = $collection->toArray();
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->models[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }

    public function current(): Guid
    {
        return $this->models[$this->current];
    }
}