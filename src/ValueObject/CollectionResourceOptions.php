<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 7:13 PM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Exception\InvalidResourceOptionException;

class CollectionResourceOptions extends ModelResourceOptions
{

    private $pageMaxSize = 0;
    private $pageOffset = 0;
    private $totalCount = 0;

    public function __construct(array $options)
    {
        parent::__construct($options);

        if (isset($options['pageMaxSize'])) {
            $this->pageMaxSize = $options['pageMaxSize'];
        }

        if (isset($options['pageOffset'])) {
            $this->pageOffset = $options['pageOffset'];
        }

        if (isset($options['totalCount'])) {
            $this->totalCount = $options['totalCount'];
        }
    }

    protected function ensure(array $options): void
    {
        parent::ensure($options);

        foreach (['pageMaxSize', 'pageOffset', 'totalCount'] as $option) {
            if (isset($options[$option]) && !is_int($options[$option])) {
                throw new InvalidResourceOptionException('"' . $option . '" must be an integer');
            }
        }
    }

    /**
     * @return int
     */
    public function getPageMaxSize(): int
    {
        return $this->pageMaxSize;
    }

    /**
     * @return int
     */
    public function getPageOffset(): int
    {
        return $this->pageOffset;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}
