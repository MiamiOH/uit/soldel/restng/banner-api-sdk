<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 8:27 AM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Exception\InvalidApiSourceException;

class ApiSource
{
    private $value;

    private static $allowedSources = [
        'student',
        'integration'
    ];

    public static function fromString(string $value): ApiSource
    {
        return new ApiSource($value);
    }

    public function __construct(string $value)
    {
        $this->ensure($value);
        $this->value = $value;
    }

    private function ensure(string $value): void
    {
        if (!in_array($value, self::$allowedSources, true)) {
            throw new InvalidApiSourceException();
        }
    }

    public function __toString() : string
    {
        return $this->value;
    }

}