<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 7:13 PM
 */

namespace MiamiOH\BannerApi;


use Carbon\Carbon;
use MiamiOH\BannerApi\Exception\InvalidResourceOptionException;

class ModelResourceOptions
{

    /**
     * @var Carbon
     */
    private $date;
    private $mediaType = '';
    private $requestId = '';
    private $message = '';

    public function __construct(array $options)
    {
        $this->ensure($options);

        if (isset($options['date'])) {
            $this->date = $options['date'];
        } else {
            $this->date = Carbon::now();
        }

        if (isset($options['mediaType'])) {
            $this->mediaType = $options['mediaType'];
        }

        if (isset($options['requestId'])) {
            $this->requestId = $options['requestId'];
        }

        if (isset($options['message'])) {
            $this->message = $options['message'];
        }

    }

    protected function ensure(array $options): void
    {
        if (isset($options['date']) && !$options['date'] instanceof Carbon) {
            throw new InvalidResourceOptionException('"date" must be class ' . Carbon::class);
        }
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getMediaType(): string
    {
        return $this->mediaType;
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->requestId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

}
