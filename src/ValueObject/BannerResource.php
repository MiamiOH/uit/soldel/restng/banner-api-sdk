<?php

/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 8:29 AM
 */
namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Exception\InvalidResourceException;
use MiamiOH\BannerApi\Exception\ResourceConfigurationNotLoadedException;
use MiamiOH\BannerApi\Exception\ResourceNoVersionException;
use MiamiOH\BannerApi\Exception\ResourceUnsupportedVersionException;

class BannerResource
{
    /**
     * @var string
     */
    private $resource;

    /**
     * @var int
     */
    private $version;

    /**
     * @var array|null
     */
    private static $allowedResources = null;

    public static function setResourceConfig(array $config): void
    {
        self::$allowedResources = $config;
    }

    public static function getResourceConfig(): ?array
    {
        return self::$allowedResources;
    }

    public static function resetResourceConfig(): void
    {
        self::$allowedResources = null;
    }

    public static function fromString(string $resource, int $version = null): BannerResource
    {
        return new BannerResource($resource, $version);
    }

    public function __construct(string $resource, int $version = null)
    {
        $this->ensureResource($resource);
        if (null === $version) {
            if (empty(self::$allowedResources[$resource]['default_version'])) {
                throw new ResourceNoVersionException();
            }
            $version = self::$allowedResources[$resource]['default_version'];
        }
        $this->ensureVersion($resource, $version);

        $this->resource = $resource;
        $this->version = $version;
    }

    private function ensureResource(string $resource): void
    {
        if (null === self::$allowedResources) {
            throw new ResourceConfigurationNotLoadedException();
        }

        if(!array_key_exists($resource, self::$allowedResources)) {
            throw new InvalidResourceException();
        }
    }

    public function ensureVersion(string $resource, int $version): void
    {
        if (empty(self::$allowedResources[$resource]['versions']) ||
                !in_array($version, self::$allowedResources[$resource]['versions'])) {
            throw new ResourceUnsupportedVersionException();
        }
    }

    public function source(): ApiSource
    {
        return ApiSource::fromString(self::$allowedResources[$this->resource]['source']);
    }

    public function __toString(): string
    {
        return $this->resource;
    }

    public function version(): int
    {
        return $this->version;
    }

    public function versionMimeType(): string
    {
        return sprintf('application/vnd.hedtech.integration.v%s+json', $this->version);
    }

}