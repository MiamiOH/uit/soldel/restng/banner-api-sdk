<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 8:08 AM
 */

namespace MiamiOH\BannerApi\Resource;


interface FindableInterface
{
    public function findOne();
    public function find();

}