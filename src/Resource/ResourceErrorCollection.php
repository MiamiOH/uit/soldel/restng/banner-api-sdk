<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:52 AM
 */

namespace MiamiOH\BannerApi\Resource;


use Carbon\Carbon;
use Psr\Http\Message\ResponseInterface;

class ResourceErrorCollection implements \Countable, \IteratorAggregate
{
    /**
     * @var string
     */
    private $requestId;
    /**
     * @var string
     */
    private $reason;
    /**
     * @var string
     */
    private $message;
    /**
     * @var Carbon
     */
    private $date;
    /**
     * @var ResourceError[]
     */
    private $errors;

    /*
     * Only create a new object if we clearly have an error response. Otherwise, the caller
     * will need to let the failed response bubble up as is.
     */
    public static function fromResponse(ResponseInterface $response): ResourceErrorCollection
    {
        /** @var array $details */
        $details = json_decode($response->getBody(), true);

        if (!(is_array($details) && is_array($details['errors']))) {
            throw new \InvalidArgumentException();
        }

        $errors = [];
        foreach ($details['errors'] as $error) {
            $errors[] = new ResourceError(
                $error['code'],
                $error['message'],
                $error['description']
            );
        }

        // The reason header is not present in resource 404 responses, possibly others
        $reason = 'Unknown';
        if ($response->hasHeader('X-Status-Reason')) {
            $reason = $response->getHeader('X-Status-Reason')[0];
        } elseif ($response->getStatusCode() === 404) {
            $reason = 'Not found';
        }

        return new ResourceErrorCollection(
            $response->getHeader('X-Request-ID')[0],
            $reason,
            $response->getHeader('X-hedtech-message')[0],
            Carbon::parse($response->getHeader('Date')[0]),
            $errors
        );
    }

    public function __construct(string $requestId, string $reason, string $message, Carbon $date, array $errors)
    {

        $this->ensure($errors);

        $this->requestId = $requestId;
        $this->reason = $reason;
        $this->message = $message;
        $this->date = $date;
        $this->errors = $errors;
    }

    private function ensure(array $errors): void
    {
        foreach ($errors as $error) {
            if (! $error instanceof ResourceError) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public function __toString(): string
    {
        return ($this->reason ? $this->reason . ': ' : '') . $this->message . ' (request ' . $this->requestId . ')';
    }

    public function getIterator(): ResourceErrorCollectionIterator
    {
        return new ResourceErrorCollectionIterator($this);
    }

    public function count(): int
    {
        return count($this->errors);
    }

    public function toArray(): array
    {
        return $this->errors;
    }

    // TODO add getters for error collection attributes

}