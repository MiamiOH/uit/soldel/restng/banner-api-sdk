<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/23/17
 * Time: 10:43 AM
 */

namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\Resource\BaseModel;
use MiamiOH\BannerApi\Resource\ModelVersion;
use MiamiOH\BannerApi\Resource\ModelVersionInterface;
use MiamiOH\BannerApi\Resource\ResourceModelInterface;

class AcademicLevelModel extends BaseModel
{
    /** @var  string */
    private $title;
    
    /** @var  string */
    private $code;

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        parent::setData($data);

        /* Required attributes */
        $this->setTitle($data['title']);

        /* Optional attributes */
        if (!empty($data['code'])) {
            $this->setCode($data['code']);
        }

    }
    
    // Getters
    public function getTitle(): string
    {
        return $this->title;
    }
    
    public function getCode(): string
    {
        return $this->code;
    }

    // Setters
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }
    
    public function asEthosDataModelArray(): array
    {
        $data = parent::asEthosDataModelArray();

        $data['title'] = $this->getTitle();
        $data['code'] = $this->getCode();
        
        return $data;

    }
}