<?php


namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\Resource;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class AcademicLevel extends Resource
{
    public function __construct(ResourceLoaderInterface $loader, int $version = null)
    {
        parent::__construct($loader, new BannerResource('academic-levels', $version));
    }

    public function findOne(): AcademicLevelModelFinder
    {
        return new AcademicLevelModelFinder($this);
    }
    
    public function find(): AcademicLevelCollectionFinder
    {
        return new AcademicLevelCollectionFinder($this);
    }

    public function getNewModel(): AcademicLevelModel
    {
        switch ($this->getVersion()) {
            default:
                // By default it is set to the base version of AcademicLevel Model i.e 6.
                $resourceModel = new AcademicLevelModel();
                break;
        }
        return $resourceModel;
    }

    public function getNewModelCollection(): AcademicLevelCollection
    {
        return new AcademicLevelCollection();
    }
}