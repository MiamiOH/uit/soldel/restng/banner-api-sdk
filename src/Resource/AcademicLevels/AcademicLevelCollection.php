<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/23/17
 * Time: 11:00 AM
 */

namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\Resource\BaseCollection;


class AcademicLevelCollection extends BaseCollection
{
    public function __construct()
    {
        $this->setModelClass(AcademicLevelModel::class);
    }

    public function getIterator(): AcademicLevelCollectionIterator
    {
        return new AcademicLevelCollectionIterator($this);
        
    }

}