<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/23/17
 * Time: 10:39 AM
 */

namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\BaseModelFinder;
use MiamiOH\BannerApi\Resource\ModelFinder;
use MiamiOH\BannerApi\Resource\ModelFinderInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class AcademicLevelModelFinder extends BaseModelFinder
{

    public function execute(): AcademicLevelModel
    {
        if (null === $this->guid) {
            throw new MissingUniqueIdentifierException();
        }

        /** @var AcademicLevelModel $academicLevels */
        $academicLevels = $this->loader
            ->getResourceById($this->resource, $this->guid);

        return $academicLevels;
    }

}