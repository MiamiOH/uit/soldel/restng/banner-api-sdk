<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 3:15 PM
 */

namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\Resource\BaseCollectionPager;

class AcademicLevelCollectionPager extends BaseCollectionPager
{

    /**
     * @var AcademicLevelCollectionFinder
     */
    protected $finder;

    public function __construct(AcademicLevelCollectionFinder $finder)
    {
        $this->finder = $finder;
        parent::__construct();
    }

    public function next(): ?AcademicLevelCollection
    {
        if (!$this->canRunNext()) {
            return null;
        }

        $collection = $this->finder
            ->withOffset($this->getOffset())
            ->withLimit($this->getLimit())
            ->execute();

        $this->postNext($collection);

        return $collection;
    }

}