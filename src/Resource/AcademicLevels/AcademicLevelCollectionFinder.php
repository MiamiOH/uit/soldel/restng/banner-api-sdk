<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/23/17
 * Time: 10:58 AM
 */

namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\BaseCollectionFinder;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class AcademicLevelCollectionFinder extends BaseCollectionFinder
{

    public function execute(): AcademicLevelCollection
    {
        /** @var AcademicLevelCollection $collection */
        $collection = $this->loader
            ->getResource($this->resource, $this->filter);

        return $collection;
    }

    public function pager(): AcademicLevelCollectionPager
    {
        return new AcademicLevelCollectionPager($this);
    }
}
