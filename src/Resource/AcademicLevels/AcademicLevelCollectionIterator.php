<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/23/17
 * Time: 11:03 AM
 */

namespace MiamiOH\BannerApi\Resource\AcademicLevel;


use MiamiOH\BannerApi\Resource\BaseCollectionIterator;

class AcademicLevelCollectionIterator extends BaseCollectionIterator
{

    public function __construct(AcademicLevelCollection $collection)
    {
        $this->models = $collection->toArray();
    }

    public function current(): AcademicLevelModel
    {
        return $this->models[$this->current];
    }

}