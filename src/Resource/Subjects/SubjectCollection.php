<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 10:45 AM
 */

namespace MiamiOH\BannerApi\Resource\Subject;


use MiamiOH\BannerApi\Resource\BaseCollection;

class SubjectCollection extends BaseCollection
{

    public function __construct()
    {
        $this->setModelClass(SubjectModel::class);
    }

    public function getIterator(): SubjectCollectionIterator
    {
        return new SubjectCollectionIterator($this);
    }

}