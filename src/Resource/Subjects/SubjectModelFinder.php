<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 10:35 AM
 */

namespace MiamiOH\BannerApi\Resource\Subject;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\BaseModelFinder;
use MiamiOH\BannerApi\Resource\ModelFinder;
use MiamiOH\BannerApi\Resource\ModelFinderInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class SubjectModelFinder extends BaseModelFinder
{

    public function execute(): SubjectModel
    {
        if (null === $this->guid) {
            throw new MissingUniqueIdentifierException();
        }

        /** @var SubjectModel $subject */
        $subject = $this->loader
            ->getResourceById($this->resource, $this->guid);

        return $subject;
    }

}