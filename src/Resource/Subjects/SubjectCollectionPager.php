<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 3:16 PM
 */

namespace MiamiOH\BannerApi\Resource\Subject;


use MiamiOH\BannerApi\Resource\BaseCollectionPager;

class SubjectCollectionPager extends BaseCollectionPager
{

    /**
     * @var SubjectCollectionFinder
     */
    protected $finder;

    public function __construct(SubjectCollectionFinder $finder)
    {
        $this->finder = $finder;
        parent::__construct();
    }

    public function next(): ?SubjectCollection
    {
        if (!$this->canRunNext()) {
            return null;
        }

        $collection = $this->finder
            ->withOffset($this->getOffset())
            ->withLimit($this->getLimit())
            ->execute();

        $this->postNext($collection);

        return $collection;
    }

}