<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 11:13 AM
 */

namespace MiamiOH\BannerApi\Resource\Subject;


use MiamiOH\BannerApi\Resource\BaseCollectionIterator;

class SubjectCollectionIterator extends BaseCollectionIterator
{

    public function __construct(SubjectCollection $collection)
    {
        $this->models = $collection->toArray();
    }

    public function current(): SubjectModel
    {
        return $this->models[$this->current];
    }

}