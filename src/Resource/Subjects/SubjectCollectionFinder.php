<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 11:14 AM
 */

namespace MiamiOH\BannerApi\Resource\Subject;

use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\BaseCollectionFinder;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class SubjectCollectionFinder extends BaseCollectionFinder
{

    public function execute(): SubjectCollection
    {
        /** @var SubjectCollection $collection */
        $collection = $this->loader
            ->getResource($this->resource, $this->filter);

        return $collection;
    }

    public function pager(): SubjectCollectionPager
    {
        return new SubjectCollectionPager($this);
    }
}