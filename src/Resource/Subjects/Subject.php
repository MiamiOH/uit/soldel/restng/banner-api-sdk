<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 9:13 AM
 */

namespace MiamiOH\BannerApi\Resource\Subject;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\FindableInterface;
use MiamiOH\BannerApi\Resource\Resource;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class Subject extends Resource
{
    public function __construct(ResourceLoaderInterface $loader, int $version = null)
    {
        parent::__construct($loader, new BannerResource('subjects', $version));
    }

    public function findOne(): SubjectModelFinder
    {
        return new SubjectModelFinder($this);
    }

    public function find(): SubjectCollectionFinder
    {
        return new SubjectCollectionFinder($this);
    }

    public function getNewModel(): SubjectModel
    {
        switch ($this->getVersion()) {
            default:
                // Base version of Subject resource is 6 which is supported by SubjectModel.
                $resourceModel = new SubjectModel();
                break;
        }
        return $resourceModel;
    }

    public function getNewModelCollection(): SubjectCollection
    {
       return new SubjectCollection();
    }

}