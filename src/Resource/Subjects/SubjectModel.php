<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 9:37 PM
 */

namespace MiamiOH\BannerApi\Resource\Subject;

use MiamiOH\BannerApi\Resource\BaseModel;
use MiamiOH\BannerApi\Resource\ModelVersionInterface;
use MiamiOH\BannerApi\Resource\ResourceModelInterface;

class SubjectModel extends BaseModel
{
    /**
     * @var string
     */
    protected $abbreviation = '';

    /**
     * @var string
     */
    protected $title = '';

    public function setData(array $data): void
    {
        parent::setData($data);
        // Required attributes
        $this->setTitle($data['title']);

        // Optional attributes
        if (!empty($data['abbreviation'])) {
            $this->setAbbreviation($data['abbreviation']);
        }
    }

    // Setters
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setAbbreviation(string $abbreviation): void
    {
        $this->abbreviation = $abbreviation;
    }
    
    // Getters
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAbbreviation(): string 
    {
        return $this->abbreviation;
    }

    public function asEthosDataModelArray(): array
    {
        $data = parent::asEthosDataModelArray();

        $data['abbreviation'] = $this->getAbbreviation();
        $data['title'] = $this->getTitle();
        return $data;
    }
}
