<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 8/1/17
 * Time: 10:51 AM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\BannerResource;

interface ResourceInterface
{
    public function getResource(): BannerResource;
    public function getLoader(): ResourceLoaderInterface;
    public function getNewModel();
    public function getNewModelCollection();
    public function getVersion();

}