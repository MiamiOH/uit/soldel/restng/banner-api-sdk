<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 9:57 PM
 */

namespace MiamiOH\BannerApi\Resource;


use Carbon\Carbon;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\ModelResourceOptions;

interface ModelInterface
{
    public function getId(): Guid;

    public function getRequestDate(): Carbon;
    public function getRequestMediaType(): string;
    public function getRequestId(): string;
    public function getRequestMessage(): string;

}