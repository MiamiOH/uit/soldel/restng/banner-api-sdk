<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:53 AM
 */

namespace MiamiOH\BannerApi\Resource;


class ResourceErrorCollectionIterator implements \Iterator
{
    protected $current = 0;

    /**
     * @var ResourceError[]
     */
    protected $errors;

    public function __construct(ResourceErrorCollection $errorCollection)
    {
        $this->errors = $errorCollection->toArray();
    }

    public function current(): ResourceError
    {
        return $this->errors[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->errors[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }}