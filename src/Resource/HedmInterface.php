<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 9:59 PM
 */

namespace MiamiOH\BannerApi\Resource;


interface HedmInterface
{
    public function asEthosDataModelArray(): array;
}