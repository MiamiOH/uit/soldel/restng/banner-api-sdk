<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 6:44 PM
 */

namespace MiamiOH\BannerApi\Resource;


interface CollectionFilterInterface extends \Countable
{
    public function addSort(string $name, string $direction): void;
    public function addCriteria(string $name, string $value): void;
    public function addParam(string $name, string $value): void;
    public function build(): string;
}