<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 8/1/17
 * Time: 10:49 AM
 */

namespace MiamiOH\BannerApi\Resource;


interface ModelVersionInterface
{
    public function setData(array $data): void;
    public function asEthosDataModelArray(): array;
}