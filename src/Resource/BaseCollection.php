<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:47 PM
 */

namespace MiamiOH\BannerApi\Resource;


use Carbon\Carbon;
use MiamiOH\BannerApi\Collection\BaseCollectionIterator;
use MiamiOH\BannerApi\CollectionResourceOptions;
use MiamiOH\BannerApi\Exception\InvalidCollectionEntryException;

abstract class BaseCollection implements CollectionInterface, HedmInterface, \JsonSerializable
{
    /**
     * @var CollectionResourceOptions
     */
    protected $options;

    /**
     * @var BaseModel[]
     */
    protected $models = [];

    protected $modelClass = BaseModel::class;

    public function setModelClass(string $class): void
    {
        $this->modelClass = $class;
    }

    public function initialize(array $models, CollectionResourceOptions $options): void
    {
        $this->validateModels($models, $this->modelClass);

        $this->models = $models;
        $this->options = $options;
    }

    protected function validateModels($models, $class): void
    {
        for ($i = 0, $iMax = count($models); $i < $iMax; $i++) {
            
            if (!is_a($models[$i], $class)) {
                throw new InvalidCollectionEntryException(get_class($models[$i]) . ' / ' . $class);
            }
        }
    }

    public function count(): int
    {
        return count($this->models);
    }

    public function toArray(): array
    {
        return $this->models;
    }

    public function getRequestDate(): Carbon
    {
        return $this->options->getDate();
    }

    public function getRequestMediaType(): string
    {
        return $this->options->getMediaType();
    }

    public function getRequestId(): string
    {
        return $this->options->getRequestId();
    }

    public function getRequestMessage(): string
    {
        return $this->options->getMessage();
    }

    public function getPageMaxSize(): int
    {
        return $this->options->getPageMaxSize();
    }

    public function getPageOffset(): int
    {
        return $this->options->getPageOffset();
    }

    public function getTotalCount(): int
    {
        return $this->options->getTotalCount();
    }

    // Resource classes must provide their own implementation of the collect
    // and toJson methods. Any composed classes in a model must also provide
    // a collect method.
    public function asEthosDataModelArray(): array
    {
        $data = [];
        foreach ($this->models as $model) {
            $data[] = $model->asEthosDataModelArray();
        }

        return $data;
    }

    public function jsonSerialize(): array
    {
        return $this->asEthosDataModelArray();
    }

}