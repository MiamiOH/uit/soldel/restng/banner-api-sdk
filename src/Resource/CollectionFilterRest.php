<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 6:45 PM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\BannerResource;

class CollectionFilterRest implements CollectionFilterInterface
{

    private $filters = [];
    private $sort = [];
    private $criteria = [];
    private $params = [];
    /**
     * @var BannerResource
     */
    private $resource;

    /**
     * @var array
     */
    private $allParams;

    public function __construct(BannerResource $resource)
    {
        $this->resource = $resource;
    }

    public function addSort(string $name, string $direction = 'asc'): void
    {
        $this->sort[$name] = $direction;
    }

    public function addCriteria(string $name, string $value): void
    {
        $this->criteria[$name] = $value;
    }

    public function addParam(string $name, string $value): void
    {
        $this->params[$name] = $value;
    }

    public function build(): string
    {
        if ($this->resource->version() <= 6) {
            return $this->buildVersion6();
        }

        return $this->buildVersion8();
    }

    private function buildVersion6(): string
    {
        $allParams =[];
      
        if (!empty($this->criteria)) {
            $allParams = array_merge($this->criteria);
        }
        if (!empty($this->params)) {
            $allParams = array_merge($allParams, $this->params);
        }
        if (!empty($this->sort)) {
            
            $allParams = array_merge($allParams,['sort'=> key($this->sort),'order' => $this->sort[key($this->sort)]]); 
        }
        
        return $this->createQueryString($allParams);
    }

    private function buildVersion8(): string
    {
        $allParams =[];

        if (!empty($this->criteria)) {
            $allParams = array_merge(['criteria' => json_encode($this->criteria)]);
        }
        if (!empty($this->params)) {
            $allParams = array_merge($allParams, $this->params);
        }
        if (!empty($this->sort)) {
            $allParams = array_merge($allParams,['sort' => json_encode($this->sort)]);
        }
        
        return $this->createQueryString($allParams);
    }

    private function createQueryString(array $params): string
    {
        return implode('&', array_map(
            function ($key, $value) {
                return implode('=', [$key, urlencode($value)]);
            },
            array_keys($params),
            array_values($params)
        ));
    }

    public function count(): int
    {
        return count($this->criteria) + count($this->sort) + count($this->params);
    }
}