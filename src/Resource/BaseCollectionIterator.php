<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 4:29 PM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\Resource\BaseCollection;
use MiamiOH\BannerApi\Resource\BaseModel;

abstract class BaseCollectionIterator implements \Iterator
{
    protected $current = 0;

    /**
     * @var BaseModel[]
     */
    protected $models;

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->models[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}