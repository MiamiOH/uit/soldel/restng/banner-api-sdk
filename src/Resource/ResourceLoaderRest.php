<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 1:59 PM
 */

namespace MiamiOH\BannerApi\Resource;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\Api\ApiPackage;
use MiamiOH\BannerApi\Api\ResourceDefinitionCollection;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\CollectionResourceOptions;
use MiamiOH\BannerApi\Exception\MissingPrototypeException;
use MiamiOH\BannerApi\Exception\ResourceBadRequestException;
use MiamiOH\BannerApi\Exception\ResourceModelNotFoundException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\LogManager;
use MiamiOH\BannerApi\ModelResourceOptions;
use Psr\Http\Message\ResponseInterface;

class ResourceLoaderRest implements ResourceLoaderInterface
{

    /**
     * @var Location
     */
    private $location;
    /**
     * @var Client
     */
    private $client;

    public function __construct(Location $location, Client $client)
    {
        $this->client = $client;
        $this->location = $location;
    }

    public function getCollectionFilter(BannerResource $resource): CollectionFilterInterface
    {
        return new CollectionFilterRest($resource);
    }

    public function getResourceById(ResourceInterface $resource, Guid $guid): BaseModel
    {
        $bannerResource = $resource->getResource();
        $url = $this->location->getAbsoluteLocation($bannerResource, $guid);

        LogManager::logger($bannerResource)->info('Getting resource: ' . $url);

        $request = $this->newRequest('GET', $url, $bannerResource->versionMimeType());
        $response = $this->makeRequest($request);

        return $this->newResourceModelFromResponse($resource, $response);
    }

    public function getResource(ResourceInterface $resource, CollectionFilterInterface $filter = null): BaseCollection
    {
        $bannerResource = $resource->getResource();
        $url = $this->location->getAbsoluteLocation($bannerResource);
        if (null !== $filter && count($filter)) {
            $url .= '?' . $filter->build();
        }

        LogManager::logger($bannerResource)->info('Getting resource: ' . $url);

        $request = $this->newRequest('GET', $url, $bannerResource->versionMimeType());
        $response = $this->makeRequest($request);

        return $this->newResourceCollectionFromResponse($resource, $response);
    }

    private function newRequest(string $method, string $url, string $versionMimeType): Request
    {
        $headers = [
            'Accept-Charset' => 'UTF-8',
            'Accept' => $versionMimeType,
        ];

//        if (in_array($method, ['PUT', 'POST', 'DELETE'])) {
//            $headers['Content-Type'] = $versionMimeType;
//        }

        return new Request($method, $url, $this->location->addAuthenticationHeaders($headers));
    }

    private function makeRequest(Request $request): Response
    {
        try {
            // REST requests/response will be logged by Guzzle. See the config
            // in ApiFactory for more.
            return $this->client->send($request);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                LogManager::logger('rest')->debug(\GuzzleHttp\Psr7\str($e->getResponse()));
                $this->throwApiErrorException($e->getResponse(), $e);
            }

            LogManager::logger('rest')->error($e);

            throw $e;
        }
    }

    private function throwApiErrorException(ResponseInterface $response, \Exception $e = null): void
    {

        if ($response->getStatusCode() === 404) {
            throw new ResourceModelNotFoundException();
        }

        // If the expected error collection cannot be created from the
        // response, quietly continue and let the caller throw the
        // original exception.
        try {
            $errors = ResourceErrorCollection::fromResponse($response);

            LogManager::logger('rest')->error($errors);

            throw new ResourceBadRequestException($errors, $response->getStatusCode(), $e);
        } catch (\TypeError $e) {
            // Don't do anything
        } catch (\InvalidArgumentException $e) {
            // Don't do anything
        }
    }

    private function newResourceModelFromResponse(ResourceInterface $resource, Response $response): BaseModel
    {
        $options = new ModelResourceOptions([
            'date' => Carbon::parse($response->getHeader('Date')[0]),
            'mediaType' => $response->getHeader('X-Media-Type')[0],
            'requestId' => $response->getHeader('X-Request-ID')[0],
            'message' => $response->getHeader('X-hedtech-message')[0],
        ]);

        return $this->newModelFromArray($resource, json_decode($response->getBody(), true), $options);
    }

    private function newModelFromArray(ResourceInterface $resource, array $data, ModelResourceOptions $options): BaseModel
    {
        /** @var BaseModel $model */
        $model = $resource->getNewModel();
        $model->initialize($data, $options);

        return $model;
    }

    public function newResourceCollectionFromResponse(ResourceInterface $resource, Response $response): BaseCollection
    {
        $options = new CollectionResourceOptions([
            'date' => Carbon::parse($response->getHeader('Date')[0]),
            'mediaType' => $response->getHeader('X-Media-Type')[0],
            'requestId' => $response->getHeader('X-Request-ID')[0],
            'message' => $response->getHeader('X-hedtech-message')[0],
            'pageMaxSize' => (int) $response->getHeader('X-hedtech-pageMaxSize')[0],
            'pageOffset' => (int) $response->getHeader('X-hedtech-pageOffset')[0],
            'totalCount' => (int) $response->getHeader('X-hedtech-totalCount')[0],
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        $models = [];
        /** @var array $modelData */
        foreach ($data as $modelData) {
            $models[] = $this->newModelFromArray($resource, $modelData, new ModelResourceOptions([]));
        }

        return $this->newCollectionFromArray($resource, $models, $options);
    }

    public function newCollectionFromArray(ResourceInterface $resource, array $models, CollectionResourceOptions $options): BaseCollection
    {
        /** @var BaseCollection $collection */
        $collection = $resource->getNewModelCollection();
        $collection->initialize($models, $options);

        return $collection;
    }

    public function getApiPackage(string $name): ApiPackage
    {
        $url = $this->location->getBaseUrl() . '/' . $name . '/api/resources';
        $mimeType = 'application/json';

        $request = $this->newRequest('GET', $url, $mimeType);
        $response = $this->makeRequest($request);

//        Carbon::parse($response->getHeader('Date')[0]),
//            'mediaType' => $response->getHeader('X-Media-Type')[0],
//            'requestId' => $response->getHeader('X-Request-ID')[0],
//            'message' => $response->getHeader('X-hedtech-message')[0],
//            'pageMaxSize' => (int) $response->getHeader('X-hedtech-pageMaxSize')[0],
//            'pageOffset' => (int) $response->getHeader('X-hedtech-pageOffset')[0],
//            'totalCount' => (int) $response->getHeader('X-hedtech-totalCount')[0],

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new ApiPackage($name, ResourceDefinitionCollection::fromArray($data));
    }
}
