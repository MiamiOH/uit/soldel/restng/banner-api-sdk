<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/30/17
 * Time: 7:37 AM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\Guid;

interface ModelFinderInterface
{
    public function withId(Guid $guid);
    public function execute();
}