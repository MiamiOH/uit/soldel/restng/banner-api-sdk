<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 10:17 AM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\BannerResource;

abstract class BaseCollectionFinder implements CollectionFinderInterface
{

    /**
     * @var BannerResource
     */
    protected $bannerResource;
    /**
     * @var ResourceLoaderInterface
     */
    protected $loader;
    /**
     * @var CollectionFilterInterface
     */
    protected $filter;

    protected $offset = 0;
    protected $limit = 0;

    /**
     * @var string
     */
    protected $attribute;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var ResourceInterface
     */
    protected $resource;

    public function __construct(ResourceInterface $resource)
    {
        $this->resource = $resource;
        $this->bannerResource = $resource->getResource();
        $this->loader = $resource->getLoader();

        $this->filter = $this->loader->getCollectionFilter($this->bannerResource);
    }

    /**
     * Array must be compatible with:
     *   https://resources.elluciancloud.com/bundle/ethos_data_model_ref_api_standards/page/API-behaviors.html?
     *
     * @param string $sortAttribute
     * @param string $direction
     * @return $this
     */
    public function withSort(string $sortAttribute, string $direction)
    {
        $this->filter->addSort($sortAttribute,$direction);
        
        return $this;
    }

    /**
     * @param string $offset
     * @return $this
     */
    public function withOffset(string $offset)
    {
        $this->offset = $offset;
        $this->filter->addParam('offset', $offset);
        return $this;
    }

    /**
     * @param string $attribute
     * @param string $value
     */
    public function withQuery(string $attribute, string $value)
    {
        $this->attribute = $attribute;
        $this->value = $value;
        $this->filter->addCriteria($this->attribute, $this->value);
        return $this;
    }


    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function withLimit(int $limit)
    {
        $this->limit = $limit;
        $this->filter->addParam('limit', $limit);
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

}