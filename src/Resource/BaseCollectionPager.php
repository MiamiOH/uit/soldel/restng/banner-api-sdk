<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 8:28 AM
 */

namespace MiamiOH\BannerApi\Resource;



use MiamiOH\BannerApi\Exception\PagedFindNotExecutedException;

abstract class BaseCollectionPager implements CollectionPagerInterface
{
    /**
     * @var CollectionFinderInterface
     */
    protected $finder;

    private $hasRun = false;

    private $offset = 0;
    private $limit = 10;
    private $total = 0;

    public function __construct()
    {
        if (null !== $this->finder) {
            $this->offset = $this->finder->getOffset();
            $this->limit = $this->finder->getLimit();
        }
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getTotalCount(): int
    {
        if (!$this->hasRun) {
            throw new PagedFindNotExecutedException();
        }

        return $this->total;
    }

    protected function canRunNext(): bool
    {
        if ($this->hasRun && $this->offset > $this->total) {
            return false;
        }

        return true;
    }

    protected function postNext(CollectionInterface $collection): void
    {
        $this->offset += $this->limit;
        $this->total = $collection->getTotalCount();

        $this->hasRun = true;
    }

}