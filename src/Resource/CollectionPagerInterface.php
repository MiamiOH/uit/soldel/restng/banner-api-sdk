<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 8:20 AM
 */

namespace MiamiOH\BannerApi\Resource;


interface CollectionPagerInterface
{
    public function getOffset(): int;
    public function getLimit(): int;
    public function getTotalCount(): int;

    public function next();
}