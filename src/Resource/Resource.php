<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 9:47 PM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\BannerResource;

abstract class Resource implements FindableInterface, ResourceInterface
{
    /**
     * @var BannerResource
     */
    protected $bannerResource;
    /**
     * @var ResourceLoaderInterface
     */
    protected $loader;

    public function __construct(ResourceLoaderInterface $loader, BannerResource $resource)
    {
        $this->loader = $loader;
        $this->bannerResource = $resource;
    }

    public function getResource(): BannerResource
    {
        return $this->bannerResource;
    }

    public function getLoader(): ResourceLoaderInterface
    {
        return $this->loader;
    }

    public function getName(): string
    {
        return $this->bannerResource;
    }

    public function getSource(): string
    {
        return $this->bannerResource->source();
    }

    public function getVersion(): int
    {
        return $this->bannerResource->version();
    }

}