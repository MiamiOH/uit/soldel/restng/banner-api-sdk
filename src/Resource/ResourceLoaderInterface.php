<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 1:58 PM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\Api\ApiPackage;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;

interface ResourceLoaderInterface
{
    public function getCollectionFilter(BannerResource $resource): CollectionFilterInterface;

    public function getResourceById(ResourceInterface $resource, Guid $guid): BaseModel;
    public function getResource(ResourceInterface $resource, CollectionFilterInterface $filter): BaseCollection;

    public function getApiPackage(string $name): ApiPackage;
}