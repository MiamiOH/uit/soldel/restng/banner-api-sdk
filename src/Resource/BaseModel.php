<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:42 PM
 */

namespace MiamiOH\BannerApi\Resource;


use Carbon\Carbon;
use MiamiOH\BannerApi\Api;
use MiamiOH\BannerApi\Exception\AlreadyInitializedException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\ModelResourceOptions;

abstract class BaseModel implements ModelInterface, HedmInterface, \JsonSerializable
{
    /**
     * @var ModelResourceOptions
     */
    protected $options;

    protected $initialized = false;
    
    /**
     * @var Guid
     */
    protected $id;

    public function __construct(array $data = null, ModelResourceOptions $options = null)
    {
        $this->id = new Guid(Api::NIL_GUID);
        
        if (null !== $data) {
            $this->initialize($data, $options);
        }
    }

    public function initialize(array $data, ModelResourceOptions $options = null): void
    {
        if ($this->initialized) {
            throw new AlreadyInitializedException();
        }

        if (null === $options) {
            $options = new ModelResourceOptions([]);
        }

        $this->setData($data);
        $this->options = $options;

        $this->initialized = true;
    }

    // This should be overridden by specific models.
    public function setData(array $data): void
    {
        $this->setId($data['id']);
    }

    // This should be overridden by specific models
    public function asEthosDataModelArray(): array
    {
         return [ 'id' => $this->getId() ];
    }

    // Setter
    public function setId(string $id=null): void
    {
        if (null !== $id) {
            $this->id = new Guid($id);
        }    
    }
    public function getId(): Guid
    {
        return $this->id;
    }

    public function getRequestDate(): Carbon
    {
        return $this->options->getDate();
    }

    public function getRequestMediaType(): string
    {
        return $this->options->getMediaType();
    }

    public function getRequestId(): string
    {
        return $this->options->getRequestId();
    }

    public function getRequestMessage(): string
    {
        return $this->options->getMessage();
    }

    public function jsonSerialize(): array
    {
        return $this->asEthosDataModelArray();
    }

}
