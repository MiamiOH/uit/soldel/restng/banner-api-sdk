<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/30/17
 * Time: 2:29 PM
 */

namespace MiamiOH\BannerApi\Resource;


use Carbon\Carbon;

interface CollectionInterface extends \Countable, \IteratorAggregate
{
    public function getRequestDate(): Carbon;
    public function getRequestMediaType(): string;
    public function getRequestId(): string;
    public function getRequestMessage(): string;
    public function getPageMaxSize(): int;
    public function getPageOffset(): int;
    public function getTotalCount(): int;
}