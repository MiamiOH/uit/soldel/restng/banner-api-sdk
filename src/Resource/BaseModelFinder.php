<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 10:17 AM
 */

namespace MiamiOH\BannerApi\Resource;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;

abstract class BaseModelFinder implements ModelFinderInterface
{
    /**
     * @var BannerResource
     */
    protected $bannerResource;
    /**
     * @var ResourceLoaderInterface
     */
    protected $loader;
    /**
     * @var Guid
     */
    protected $guid;
    /**
     * @var ResourceInterface
     */
    protected $resource;

    public function __construct(ResourceInterface $resource)
    {
        $this->bannerResource = $resource->getResource();
        $this->loader = $resource->getLoader();
        $this->resource = $resource;
    }

    /**
     * @param Guid $guid
     * @return $this
     */
    public function withId(Guid $guid)
    {
        $this->guid = $guid;
        return $this;
    }
}