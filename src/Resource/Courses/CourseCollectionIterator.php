<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/20/17
 * Time: 8:15 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\BaseCollectionIterator;

/**
 * Class CourseCollectionIterator
 * @package MiamiOH\BannerApi\Resource\Course
 *
 * The CourseCollectionIterator class provides a concrete iterator which
 * return a concrete model. The primary value is helping the IDE.
 */
class CourseCollectionIterator extends BaseCollectionIterator
{

    public function __construct(CourseCollection $collection)
    {
        $this->models = $collection->toArray();
    }

    /**
     * @return CourseModel
     *
     * Override the parent current() method to return a concrete type.
     */
    public function current(): CourseModel
    {
        return $this->models[$this->current];
    }

}