<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 10:19 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\BaseModelFinder;
use MiamiOH\BannerApi\Resource\ModelFinderInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

/**
 * Class CourseModelFinder
 * @package MiamiOH\BannerApi\Resource\Course
 *
 * The CourseModelFinder implements finding a course by id.
 */
class CourseModelFinder extends BaseModelFinder
{

    /**
     * @return CourseModel
     * @throws \MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException
     *
     * The execute method confirms the required identifier and uses the loader
     * to find and return the item.
     */
    public function execute(): CourseModel
    {
        if (null === $this->guid) {
            throw new MissingUniqueIdentifierException();
        }

        /** @var CourseModel $course */
        $course = $this->loader
            ->getResourceById($this->resource, $this->guid);

        return $course;
    }

}