<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 10:30 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use Carbon\Carbon;
use MiamiOH\BannerApi\Api;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\GuidCollection;
use MiamiOH\BannerApi\Resource\BaseCollectionFinder;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

/**
 * Class CourseCollectionFinder
 * @package MiamiOH\BannerApi\Resource\Course
 *
 * Collection filter classes provide an interface to add filters and execute a
 * resource query.
 */
class CourseCollectionFinder extends BaseCollectionFinder
{

    /*
     * Methods to add URL filter parameters available for the resource. These must be
     * implemented in a fluent pattern.
     */
    
    /**
     * @param string $number
     * @return CourseCollectionFinder
     *
     * Number (A numbering scheme that distinguishes courses within a subject. Typically, this is
     * an integer)
     */
    public function withNumber(string $number): CourseCollectionFinder
    {
        $this->filter->addCriteria('number', $number);
        return $this;
    }

    /**
     * @param string $title
     * @return CourseCollectionFinder
     *
     * Title (The full name of a course as it appears in a course catalog (for example, 'Calculus'))
     */
    public function withTitle(string $title): CourseCollectionFinder
    {
        $this->filter->addCriteria('title', $title);
        return $this;
    }

    /**
     * @param Carbon $scheduleStartOn
     * @return CourseCollectionFinder
     *
     * Schedule Start On (The starting date on which a course is available to have sections scheduled.
     * When combined with the Course Effective Ending Date, this defines the time period a course is
     * available for scheduling)
     */
    public function withScheduleStartOn(Carbon $scheduleStartOn): CourseCollectionFinder
    {
        $this->filter->addCriteria('scheduleStartOn', $scheduleStartOn->format(Api::DATE_FORMAT));
        return $this;
    }

    /**
     * @param Carbon $scheduleEndOn
     * @return CourseCollectionFinder
     *
     * Schedule End On (The ending date on which a course is no longer available to have sections
     * scheduled. When combined with the Course Effective Starting Date, this defines the time
     * period a course is available for scheduling)
     */
    public function withScheduleEndOn(Carbon $scheduleEndOn): CourseCollectionFinder
    {
        $this->filter->addCriteria('scheduleEndOn', $scheduleEndOn->format(Api::DATE_FORMAT));
        return $this;
    }

    /**
     * @param Guid $subjectId
     * @return CourseCollectionFinder
     *
     * Subject (The branch of knowledge such as 'Mathematics' or 'Biology' associated with a course.)
     */
    public function withSubject(Guid $subjectId): CourseCollectionFinder
    {
        $this->filter->addCriteria('subject', $subjectId);
        return $this;
    }

    /**
     * @param GuidCollection $instructionalMethodIds
     * @return CourseCollectionFinder
     *
     * Instructional Methods (The methods, styles, or formats in which a course is taught
     * (for example, 'Lecture', 'Lab'))
     */
    public function withInstructionalMethods(GuidCollection $instructionalMethodIds): CourseCollectionFinder
    {
        $this->filter->addCriteria('instructionalMethods', implode(',', $instructionalMethodIds->toArray()));
        return $this;

    }

    /**
     * @param GuidCollection $academicLevelIds
     * @return CourseCollectionFinder
     *
     * Academic Levels (The academic level (for example, 'Under Graduate' or 'Graduate') associated
     * with a course)
     */
    public function withAcademicLevels(GuidCollection $academicLevelIds): CourseCollectionFinder
    {
        $this->filter->addCriteria('academicLevels',implode(',', $academicLevelIds->toArray()));
        return $this;
    }

    /**
     * @param GuidCollection $owningInstitutionUnits
     * @return CourseCollectionFinder
     *
     * Owning Institution Units (A list of units of the educational institution (optionally,
     * hierarchical) that own or are responsible for a course)
     */
    public function withOwningInstitutionUnits(GuidCollection $owningInstitutionUnits): CourseCollectionFinder
    {
        $this->filter->addCriteria('owningInstitutionUnits', implode(',', $owningInstitutionUnits->toArray()));
        return $this;
    }

    /**
     * @param Guid $topicId
     * @return CourseCollectionFinder
     *
     * Global identifier for Topic
     */
    public function withTopic(Guid $topicId): CourseCollectionFinder
    {
        $this->filter->addCriteria('topic',$topicId);
        return $this;
    }

    /**
     * @param GuidCollection $categories
     * @return CourseCollectionFinder
     *
     * The categories to which the course may belong (for example - Vocational, Co-op
     * Work Experience, Lab, Music, etc.)
     */
    public function withCategories(GuidCollection $categories): CourseCollectionFinder
    {
        $this->filter->addCriteria('categories',implode(',',$categories->toArray()));
        return $this;
    }

    /**
     * @return CourseCollection
     *
     * The execute method will confirm any collection filtering required for the
     * resource and use the loader to find the collection.
     */
    public function execute(): CourseCollection
    {
        /** @var CourseCollection $collection */
        $collection = $this->loader
            ->getResource($this->resource, $this->filter);

        return $collection;
    }

    public function pager(): CourseCollectionPager
    {
        return new CourseCollectionPager($this);
    }
    
}