<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:48 PM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\BaseCollection;

/**
 * Class CourseCollection
 * @package MiamiOH\BannerApi\Resource\Course
 *
 * The CourseCollection class extends the base BaseCollection in order set up
 * resource specific concrete implementations. The primary value is helping
 * the IDE understand the classes its dealing with.
 */
class CourseCollection extends BaseCollection
{

    public function __construct()
    {
        /*
         * The collection needs to know what concrete class it will contain.
         */
        $this->setModelClass(CourseModel::class);
    }

    /**
     * @return CourseCollectionIterator
     *
     * Implement an interator. This requires specific knowledge of the concrete
     * types involved and therefore cannot be done in the BaseCollection class.
     */
    public function getIterator(): CourseCollectionIterator
    {
        return new CourseCollectionIterator($this);
    }

}