<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 9:59 PM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\FindableInterface;
use MiamiOH\BannerApi\Resource\Resource;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

/**
 * Class Course
 * @package MiamiOH\BannerApi\Resource\Course
 *
 * The Course class represents the API resource to the user. It's primary purpose
 * is to set the resource and provide concrete classes resource methods.
 */
class Course extends Resource
{
    /**
     * The constructor accepts an object implementing the ResourceLoaderInterface. The parent
     * constructor must be called with the loader option argument.
     *
     * @param ResourceLoaderInterface $loader
     * @param int|null $version
     */
    public function __construct(ResourceLoaderInterface $loader, int $version = null)
    {
        // Call the parent constructor with the specific resource represented by this class.
        parent::__construct($loader, new BannerResource('courses', $version));
    }

    /**
     * @return CourseModelFinder
     *
     * The findOne method returns a ModelFinder implementation for the resource.
     */
    public function findOne(): CourseModelFinder
    {
        return new CourseModelFinder($this);
    }

    /**
     * @return CourseCollectionFinder
     *
     * The find method returns a BaseCollectionFinder implementation for the resource.
     */
    public function find(): CourseCollectionFinder
    {
        return new CourseCollectionFinder($this);
    }

    public function getNewModel(): CourseModel
    {
        switch ($this->getVersion()) {
            case 8:
                $resourceModel = new CourseModelV8();
                break;
            default:
                // Base version of Course resource is 6 which is supported by CourseModel.
                $resourceModel = new CourseModel();
                break;
        }
        return $resourceModel;
    }

    public function getNewModelCollection(): CourseCollection
    {
        return new CourseCollection();
    }
}