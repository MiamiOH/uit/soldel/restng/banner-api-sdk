<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 8/1/17
 * Time: 9:38 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


class CourseModelV8 extends CourseModel
{
    public function setData(array $data): void
    {
        parent::setData($data);
        
        if (!empty($data['billing'])) {
            $this->setBilling(new CourseBilling(
                (int) $data['billing']['minimum'],
                (int) $data['billing']['maximum'],
                (int) $data['billing']['increment']
            ));
        }
        if (!empty($data['reportingDetail'])) {
            $hourDetails = [];

            foreach (['lab', 'lecture', 'contact'] as $hourCategory) {
                if (null !== $data['reportingDetail'][$hourCategory]) {
                    $hourDetails[$hourCategory] = new CourseReportingDetailHours(
                        (int) $data['reportingDetail'][$hourCategory]['minimum'],
                        (int) $data['reportingDetail'][$hourCategory]['maximum'],
                        (int) $data['reportingDetail'][$hourCategory]['increment']
                    );
                }
            }

            $this->setReportingDetail(new CourseReportingDetail(
                $data['reportingDetail']['type'],
                $hourDetails,
                $data['reportingDetail']['courseWeight']
            ));
        }
        
    }
    
    public function asEthosDataModelArray(): array
    {
        $data = parent::asEthosDataModelArray();

        if (null !== $this->getBilling()) {
            $data['billing'] = $this->getBilling()->asEthosDataModelArray();
        }
        if (null !== $this->getReportingDetail()) {
            $data['reportingDetail'] = $this->getReportingDetail()->asEthosDataModelArray();
        }
        
        return $data;
        
    }
}