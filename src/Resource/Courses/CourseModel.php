<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:44 PM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use Carbon\Carbon;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\GuidCollection;
use MiamiOH\BannerApi\Resource\BaseModel;
use MiamiOH\BannerApi\Api;

/**
 * Class CourseModel
 * @package MiamiOH\BannerApi\Resource\Course
 *
 * The CourseModel class implements the Course HEDM. It can be initialized from
 * a HEDM returned from the courses resource and represent itself as JSON suitable
 * for use with the resource.
 *
 * Concrete model classes should NOT have a constructor. The class must be usable
 * with an empty constructor. The parent initialize() method is used to provide
 * data to the created object.
 *
 * Getter and setter methods should exist for each attribute using names consiste
 * with the HEDM. Supporting classes should be used to represent complex data.
 */
class CourseModel extends BaseModel
{
    private $title = '';
    /**
     * @var string
     */
    private $description = '';
    /**
     * @var Guid
     */
    private $subjectId;
    /**
     * @var Guid
     */
    private $topicId;
    /**
     * @var GuidCollection
     */
    private $categoryIds;
    /**
     * @var GuidCollection
     */
    private $courseLevelIds;
    /**
     * @var GuidCollection
     */
    private $instructionalMethodIds;
    /**
     * @var CourseOwningInstitutionUnitCollection
     */
    private $owningInstitutionUnits;
    /**
     * @var Carbon
     */
    private $schedulingStartOn;
    /**
     * @var Carbon
     */
    private $schedulingEndOn;
    /**
     * @var string
     */
    private $number = '';
    /**
     * @var GuidCollection
     */
    private $academicLevelIds;
    /**
     * @var GuidCollection
     */
    private $gradeSchemeIds;
    /**
     * @var CourseCreditCollection
     */
    private $credits;
    /**
     * @var CourseBilling
     */
    private $billing;
    /**
     * @var CourseReportingDetail
     */
    private $reportingDetail;

    public function setData(array $data): void
    {
        // Required attributes
        parent::setData($data);
        $this->setTitle($data['title']);
        $this->setSubject(new Guid($data['subject']['id']));
        $this->setSchedulingStartOn(new Carbon($data['schedulingStartOn']));
        $this->setNumber($data['number']);

        // Optional attributes
        if (!empty($data['description'])) {
            $this->setDescription($data['description']);
        }
        if (!empty($data['topic'])) {
            $this->setTopic(new Guid($data['topic']['id']));
        }
        if (!empty($data['categories'])) {
            $this->setCategories(GuidCollection::fromAssocOfIds($data['categories']));
        }
        if (!empty($data['courseLevels'])) {
            $this->setCourseLevels(GuidCollection::fromAssocOfIds($data['courseLevels']));
        }
        if (!empty($data['instructionalMethods'])) {
            $this->setInstructionalMethods(GuidCollection::fromAssocOfIds($data['instructionalMethods']));
        }
        if (!empty($data['owningInstitutionUnits'])) {
            $units = [];
            foreach ($data['owningInstitutionUnits'] as $owningUnit) {
                $units[] = new CourseOwningInstitutionUnit(
                    new Guid($owningUnit['institutionUnit']['id']),
                    (int) $owningUnit['ownershipPercentage']
                );
            }
            $this->setOwningInstitutionUnits(new CourseOwningInstitutionUnitCollection($units));
        }
        if (!empty($data['schedulingEndOn'])) {
            $this->setSchedulingEndOn(new Carbon($data['schedulingEndOn']));
        }
        if (!empty($data['academicLevels'])) {
            $this->setAcademicLevels(GuidCollection::fromAssocOfIds($data['academicLevels']));
        }
        if (!empty($data['gradeSchemes'])) {
            $this->setGradeSchemes(GuidCollection::fromAssocOfIds($data['gradeSchemes']));
        }
        if (!empty($data['credits'])) {
            $credits = [];
            foreach ($data['credits'] as $credit) {
                $credits[] = new CourseCredit(
                    new Guid($credit['creditCategory']['detail']['id']),
                    $credit['creditCategory']['creditType'],
                    $credit['measure'],
                    (int) $credit['minimum'],
                    (int) $credit['maximum'],
                    (int) $credit['increment']
                );
            }
            $this->setCredits(new CourseCreditCollection($credits));
        }
    }

    public function asEthosDataModelArray(): array
    {
        $data = parent::asEthosDataModelArray();
        $data['title'] = $this->getTitle();
        $data['subject'] = ['id' => $this->getSubject()];
        $data['schedulingStartOn'] = $this->getSchedulingStartOn()->format(Api::DATE_FORMAT);
        $data['number'] = $this->getNumber();

        // Optional attributes
        if (null !== $this->getDescription()) {
            $data['description'] = $this->getDescription();
        }
        if (null !== $this->getTopic()) {
            $data['topic']['id'] = $this->getTopic();
        }
        if (null !== $this->getCategories()) {
            $data['categories'] = $this->getCategories()->collect();
        }
        if (null !== $this->getCourseLevels()) {
            $data['courseLevels'] = $this->getCourseLevels()->collect();
        }
        if (null !== $this->getInstructionalMethods()) {
            $data['instructionalMethods'] = $this->getInstructionalMethods()->collect();
        }
        if (null !== $this->getOwningInstitutionUnits()) {
            $data['owningInstitutionUnits'] = $this->getOwningInstitutionUnits()->asEthosDataModelArray();
        }
        if (null !== $this->getSchedulingEndOn()) {
            $data['schedulingEndOn'] = $this->getSchedulingEndOn()->format(Api::DATE_FORMAT);
        }
        if (null !== $this->getAcademicLevels()) {
            $data['academicLevels'] = $this->getAcademicLevels()->collect();
        }
        if (null !== $this->getGradeSchemes()) {
            $data['gradeSchemes'] = $this->getGradeSchemes()->collect();
        }
        if (null !== $this->getCredits()) {
            $data['credits'] = $this->getCredits()->asEthosDataModelArray();
        }

        return $data;
    }
    // Setters
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function setSubject(Guid $subject): void
    {
        $this->subjectId = $subject;
    }

    public function setTopic(Guid $topic): void
    {
        $this->topicId = $topic;
    }

    public function setCategories(GuidCollection $categoryIds): void
    {
        $this->categoryIds = $categoryIds;
    }

    public function setCourseLevels(GuidCollection $courseLevelIds): void
    {
        $this->courseLevelIds = $courseLevelIds;
    }

    public function setInstructionalMethods(GuidCollection $instructionalMethodIds): void
    {
        $this->instructionalMethodIds = $instructionalMethodIds;
    }

    public function setOwningInstitutionUnits(CourseOwningInstitutionUnitCollection $owningInstitutionUnits): void
    {
        $this->owningInstitutionUnits = $owningInstitutionUnits;
    }

    public function setSchedulingStartOn(Carbon $schedulingStartOn): void
    {
        $this->schedulingStartOn = $schedulingStartOn;
    }

    public function setSchedulingEndOn(Carbon $schedulingEndOn): void
    {
        $this->schedulingEndOn = $schedulingEndOn;
    }

    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function setAcademicLevels(GuidCollection $academicLevelIds): void 
    {
        $this->academicLevelIds = $academicLevelIds;
    }

    public function setGradeSchemes(GuidCollection $gradeSchemeIds): void
    {
        $this->gradeSchemeIds = $gradeSchemeIds;
    }

    public function setCredits(CourseCreditCollection $credits): void
    {
        $this->credits = $credits;
    }

    public function setBilling(CourseBilling $billing): void
    {
        $this->billing = $billing;
    }

    public function setReportingDetail(CourseReportingDetail $reportingDetail): void
    {
        $this->reportingDetail = $reportingDetail;
    }
    
    // Getters
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getSubject(): Guid
    {
        return $this->subjectId;
    }

    public function getTopic(): Guid
    {
        return $this->topicId;
    }

    public function getCategories(): GuidCollection
    {
        return $this->categoryIds;
    }

    public function getCourseLevels(): GuidCollection
    {
        return $this->courseLevelIds;
    }

    public function getInstructionalMethods(): GuidCollection
    {
        return $this->instructionalMethodIds;
    }

    public function getOwningInstitutionUnits(): CourseOwningInstitutionUnitCollection
    {
        return $this->owningInstitutionUnits;
    }

    public function getSchedulingStartOn(): Carbon
    {
        return $this->schedulingStartOn;
    }

    public function getSchedulingEndOn(): Carbon
    {
        return $this->schedulingEndOn;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getAcademicLevels(): GuidCollection
    {
        return $this->academicLevelIds;
    }

    public function getGradeSchemes(): GuidCollection
    {
        return $this->gradeSchemeIds;
    }

    public function getCredits(): CourseCreditCollection
    {
        return $this->credits;
    }

    public function getBilling(): CourseBilling
    {
        return $this->billing;
    }

    public function getReportingDetail(): CourseReportingDetail
    {
        return $this->reportingDetail;
    }

}
