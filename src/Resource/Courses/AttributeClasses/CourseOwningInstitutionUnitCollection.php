<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 9:10 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\HedmInterface;

class CourseOwningInstitutionUnitCollection implements HedmInterface, \Countable, \IteratorAggregate
{

    /**
     * @var CourseOwningInstitutionUnit[]
     */
    private $units;

    public function __construct(array $units)
    {
        $this->ensure($units);
        $this->units = $units;
    }

    private function ensure(array $units): void
    {
        foreach ($units as $unit) {
            if (! $unit instanceof CourseOwningInstitutionUnit) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public function count(): int
    {
        return count($this->units);
    }

    public function toArray(): array
    {
        return $this->units;
    }

    public function getIterator()
    {
        return new CourseOwningInstitutionUnitCollectionIterator($this);
    }

    public function asEthosDataModelArray(): array
    {
        $data = [];
        foreach ($this->units as $unit) {
            $data[] = $unit->asEthosDataModelArray();
        }

        return $data;
    }
}