<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 9:08 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\HedmInterface;

class CourseOwningInstitutionUnit implements HedmInterface
{

    /**
     * @var Guid
     */
    private $guid;
    /**
     * @var int
     */
    private $percentage;

    public function __construct(Guid $guid, int $percentage)
    {
        $this->guid = $guid;
        $this->percentage = $percentage;
    }

    // TODO add getters

    public function asEthosDataModelArray(): array
    {
        return [
            'institutionUnit' => [
                'id' => $this->guid
            ],
            'ownershipPercentage' => $this->percentage
        ];
    }

}