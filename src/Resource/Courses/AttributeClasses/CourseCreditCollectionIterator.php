<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 11:44 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


class CourseCreditCollectionIterator implements \Iterator
{
    protected $current = 0;

    /**
     * @var CourseCredit[]
     */
    protected $entries;

    public function __construct(CourseCreditCollection $collection)
    {
        $this->entries = $collection->toArray();
    }

    public function current(): CourseCredit
    {
        return $this->entries[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->entries[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }

}