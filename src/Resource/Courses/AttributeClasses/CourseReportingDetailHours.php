<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 12:57 PM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\HedmInterface;

class CourseReportingDetailHours implements HedmInterface
{

    /**
     * @var int
     */
    private $minimum;
    /**
     * @var int
     */
    private $maximum;
    /**
     * @var int
     */
    private $increment;

    public function __construct(int $minimum, int $maximum, int $increment)
    {
        $this->minimum = $minimum;
        $this->maximum = $maximum;
        $this->increment = $increment;
    }

    // TODO add getters

    public function asEthosDataModelArray(): array
    {
        return [
            'minimum' => $this->minimum,
            'maximum' => $this->maximum,
            'increment' => $this->increment,
        ];
    }

}