<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 9:25 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


class CourseOwningInstitutionUnitCollectionIterator implements \Iterator
{
    protected $current = 0;

    /**
     * @var CourseOwningInstitutionUnit[]
     */
    protected $entries;

    public function __construct(CourseOwningInstitutionUnitCollection $collection)
    {
        $this->entries = $collection->toArray();
    }

    public function current(): CourseOwningInstitutionUnit
    {
        return $this->entries[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->entries[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }

}