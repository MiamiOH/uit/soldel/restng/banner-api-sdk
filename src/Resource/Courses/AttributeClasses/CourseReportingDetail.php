<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 12:56 PM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\HedmInterface;

class CourseReportingDetail implements HedmInterface
{

    /**
     * @var string
     */
    private $type;
    /**
     * @var CourseReportingDetailHours[]
     */
    private $detailHours;
    /**
     * @var int
     */
    private $weight;

    public function __construct(string $type, array $detailHours = [], int $weight = null)
    {
        $this->type = $type;
        $this->detailHours = $detailHours;
        $this->weight = $weight;
    }

    // TODO add getters

    public function asEthosDataModelArray(): array
    {
        $reportingModel = [
            'type' => $this->type,
        ];

        if (null !== $this->weight) {
            $reportingModel['courseWeight'] = $this->weight;
        }

        foreach ($this->detailHours as $hourCategory => $hourDetail) {
            $reportingModel[$hourCategory] = $hourDetail->asEthosDataModelArray();
        }

        return $reportingModel;
    }

}