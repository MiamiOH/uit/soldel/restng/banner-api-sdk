<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 11:43 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\HedmInterface;

class CourseCreditCollection implements HedmInterface, \Countable, \IteratorAggregate
{
    /**
     * @var CourseCredit[]
     */
    private $credits = [];

    public function __construct(array $units)
    {
        $this->ensure($units);
        $this->credits = $units;
    }

    private function ensure(array $units): void
    {
        foreach ($units as $unit) {
            if (! $unit instanceof CourseCredit) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public function count(): int
    {
        return count($this->credits);
    }

    public function toArray(): array
    {
        return $this->credits;
    }

    public function getIterator()
    {
        return new CourseCreditCollectionIterator($this);
    }

    public function asEthosDataModelArray(): array
    {
        $data = [];
        foreach ($this->credits as $credit) {
            $data[] = $credit->asEthosDataModelArray();
        }

        return $data;
    }
}