<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 11:42 AM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\HedmInterface;

class CourseCredit implements HedmInterface
{

    /**
     * @var Guid
     */
    private $guid;
    /**
     * @var string
     */
    private $category;
    /**
     * @var string
     */
    private $measure;
    /**
     * @var int
     */
    private $minimum;
    /**
     * @var int
     */
    private $maximum;
    /**
     * @var int
     */
    private $increment;

    public function __construct(Guid $guid, string $category, string $measure, int $minimum, int $maximum, int $increment)
    {
        $this->guid = $guid;
        $this->category = $category;
        $this->measure = $measure;
        $this->minimum = $minimum;
        $this->maximum = $maximum;
        $this->increment = $increment;
    }

    // TODO add getters

    public function asEthosDataModelArray(): array
    {
        return [
            'creditCategory' => [
                'creditType' => $this->category,
                'detail' => [
                    'id' => $this->guid
                ]
            ],
            'measure' => $this->measure,
            'minimum' => $this->minimum,
            'maximum' => $this->maximum,
            'increment' => $this->increment
        ];
    }

}