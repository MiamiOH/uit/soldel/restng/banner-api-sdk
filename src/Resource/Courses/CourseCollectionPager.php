<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 3:13 PM
 */

namespace MiamiOH\BannerApi\Resource\Course;


use MiamiOH\BannerApi\Resource\BaseCollectionPager;

class CourseCollectionPager extends BaseCollectionPager
{

    /**
     * @var CourseCollectionFinder
     */
    protected $finder;

    /**
     * The constructor is declared to take the concrete CollectionFinder class expected
     * by this pager. The parent constructor must be called.
     *
     * @param CourseCollectionFinder $finder
     */
    public function __construct(CourseCollectionFinder $finder)
    {
        $this->finder = $finder;
        parent::__construct();
    }

    /**
     * Implements the next page function and returns a concrete collection of the
     * appropriate type or null if no next page exists.
     *
     * @return CourseCollection|null
     *
     */
    public function next(): ?CourseCollection
    {
        // The canRunNext() method returns false if next is not a valid behavior given
        // the current internal state.
        if (!$this->canRunNext()) {
            return null;
        }

        // Use the previously set finder to get the next page of results.
        $collection = $this->finder
            ->withOffset($this->getOffset())
            ->withLimit($this->getLimit())
            ->execute();

        // The postNext() method must be called to set the new internal state from the
        // new collection.
        $this->postNext($collection);

        return $collection;
    }

}