<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/30/17
 * Time: 7:37 AM
 */

namespace MiamiOH\BannerApi\Resource;


interface CollectionFinderInterface
{
    public function withQuery(string $attribute, string $value);
    public function withSort(string $sortAttribute, string $direction);
    public function withLimit(int $limit);
    public function getLimit(): int;
    public function withOffset(string $offset);
    public function getOffset(): int;

    public function pager();
    public function execute();
}