<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:52 AM
 */

namespace MiamiOH\BannerApi\Resource;



class ResourceError
{

    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $description;

    public function __construct(string $code, string $message, string $description)
    {
        $this->code = $code;
        $this->message = $message;
        $this->description = $description;
    }

    // TODO add getters for error attributes
}