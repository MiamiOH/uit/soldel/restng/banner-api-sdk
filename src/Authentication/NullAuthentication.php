<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:59 PM
 */

namespace MiamiOH\BannerApi\Authentication;


class NullAuthentication implements AuthenticationInterface
{

    public function addAuthenticationHeaders(array $headers): array
    {
        return $headers;
    }
}