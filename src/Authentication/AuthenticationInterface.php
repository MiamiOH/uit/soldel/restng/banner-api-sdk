<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 1:00 PM
 */

namespace MiamiOH\BannerApi\Authentication;


interface AuthenticationInterface
{
    public function addAuthenticationHeaders(array $headers): array;
}