<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:59 PM
 */

namespace MiamiOH\BannerApi\Authentication;


use Carbon\Carbon;
use GuzzleHttp\Client;
use MiamiOH\BannerApi\LogManager;

class EihAuthentication implements AuthenticationInterface
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $token;

    /**
     * @var Client
     */
    private $client;
    private $jsonWebToken = '';
    /**
     * @var Carbon
     */
    private $jwtExpires;

    public function __construct(Client $client, string $url, string $token)
    {
        $this->url = $url;
        $this->token = $token;

        $this->jwtExpires = Carbon::now();

        $this->client = $client;
    }

    public function addAuthenticationHeaders(array $headers): array
    {
        $this->checkJwt();

        LogManager::logger('eihAuth')->info('Adding bearer token');
        $headers['Authorization'] = 'Bearer ' . $this->jsonWebToken;

        return $headers;
    }

    private function checkJwt(): void
    {
        if ($this->jwtExpired()) {
            $requestOptions = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                ],
            ];

            $response = $this->client->post(implode('/', [$this->url, 'auth']), $requestOptions);
            $this->jsonWebToken = $response->getBody()->getContents();
            $this->jwtExpires = Carbon::now()->addMinutes(5);
            LogManager::logger('eihAuth')->info('Fetched new JWT, expires at ' . $this->jwtExpires);
        }
    }

    private function jwtExpired(): bool
    {
        return $this->jwtExpires->lte(Carbon::now());
    }
}