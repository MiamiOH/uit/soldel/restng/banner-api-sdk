<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 12:59 PM
 */

namespace MiamiOH\BannerApi\Authentication;


use MiamiOH\BannerApi\LogManager;

class BannerApiAuthentication implements AuthenticationInterface
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function addAuthenticationHeaders(array $headers): array
    {
        LogManager::logger('bannerApiAuth')->info('Adding basic auth for ' . $this->username);
        $headers['Authorization'] = 'Basic ' . base64_encode($this->username . ':' . $this->password);
        return $headers;
    }
}