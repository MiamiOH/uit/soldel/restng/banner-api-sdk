<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 4:51 PM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Authentication\AuthenticationInterface;
use MiamiOH\BannerApi\Exception\InvalidResourceSourceException;

class Location
{
    protected $baseUrl = '';
    protected $sourcePath = [];

    /**
     * @var AuthenticationInterface
     */
    protected $authentication;

    public function __construct(string $baseUrl, AuthenticationInterface $authentication, array $sourcePath = [])
    {
        $this->baseUrl = $baseUrl;
        $this->sourcePath = $sourcePath;
        $this->authentication = $authentication;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getAbsoluteLocation(BannerResource $resource, Guid $guid = null): string
    {
        $source = (string) $resource->source();

        if (!array_key_exists($source, $this->sourcePath)) {
            throw new InvalidResourceSourceException();
        }

        $parts = [$this->baseUrl, $this->sourcePath[$source]];

        $parts[] = (string) $resource;

        if ($guid) {
            $parts[] = $guid;
        }

        return implode('/', $parts);
    }

    public function addAuthenticationHeaders(array $headers): array
    {
        return $this->authentication->addAuthenticationHeaders($headers);
    }
}