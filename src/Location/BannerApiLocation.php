<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 4:54 PM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Authentication\BannerApiAuthentication;

class BannerApiLocation extends Location
{

    public function __construct(string $baseUrl, BannerApiAuthentication $authentication)
    {
        $sourcePath = [
            'student' => 'StudentApi/api',
        ];

        parent::__construct($baseUrl, $authentication, $sourcePath);
    }
}