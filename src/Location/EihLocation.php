<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/13/17
 * Time: 4:54 PM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Authentication\EihAuthentication;

class EihLocation extends Location
{

    public function __construct(string $baseUrl, EihAuthentication $authentication)
    {
        $sourcePath = [
            'student' => 'api',
        ];

        parent::__construct($baseUrl, $authentication, $sourcePath);

    }
}