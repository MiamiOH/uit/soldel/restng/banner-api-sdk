<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:34 PM
 */

namespace MiamiOH\BannerApi;


use Cascade\Cascade;
use Monolog\Logger;
use Monolog\Registry;

class LogManager
{
    /**
     * Monolog Cascade configuration
     *
     * This can be json, yml, a php file or a php array
     *
     * @var mixed|null
     */
    private static $configuration;
    private static $level = 'ERROR';
    private static $logFile = '';

    private static $defaultHandlers = [
        'console' => [
            'class' => 'Monolog\Handler\StreamHandler',
            'level' => '',
            'stream' => 'php://stderr'
        ],
        'file' => [
            'class' => 'Monolog\Handler\StreamHandler',
            'level' => '',
            'stream' => './banner_api.log',
        ]
    ];

    private static $defaultConfiguration = [
        'handlers' => [
        ],
        'loggers' => [
            'default' => [
                'handlers' => []
            ],
        ],
    ];

    private static $initialized = false;

    public static function reset(): void
    {
        Cascade::fileConfig(['loggers' => []]);
        self::$configuration = null;
        self::$level = 'ERROR';
        self::$logFile = '';
        self::$initialized = false;
    }

    public static function setConfiguration($config): void
    {
        self::$configuration = $config;
    }

    public static function setLogLevel(string $level): void
    {
        self::$level = $level;
    }

    public static function getLogLevel(): string
    {
        return self::$level;
    }

    public static function setLogFile(string $file): void
    {
        self::$logFile = $file;
    }

    public static function getLogFile(): string
    {
        return self::$logFile;
    }

    public static function logger(string $name = 'default'): Logger
    {
        self::configure();

        /*
         * If the logger is not preconfigured, we will use the default
         * and then clone it with the expected name.
         */
        $preConfigured = Registry::hasLogger($name);

        $requestName = $preConfigured ? $name : 'default';
        $logger = Cascade::getLogger($requestName);

        if (!$preConfigured) {
            $logger = $logger->withName($name);
            Registry::addLogger($logger, $name);
        }

        return $logger;
    }

    private static function configure(): void
    {
        if (self::$initialized) {
            return;
        }

        /*
         * If there is a file set, use file handler
         *   otherwise use console handler
         *
         * Set the log level for handler
         *
         * Add handler to logger
         *
         * Any more complex log config requires full configuration
         */
        if (null !== self::$configuration) {
            $configuration = self::$configuration;
        } else {
            $configuration = self::$defaultConfiguration;
            $handler = self::$logFile ? 'file' : 'console';
            $configuration['handlers'][$handler] = self::$defaultHandlers[$handler];
            $configuration['handlers'][$handler]['level'] = self::$level;
            if ($handler === 'file') {
                $configuration['handlers'][$handler]['stream'] = self::$logFile;
            }
            $configuration['loggers']['default']['handlers'][] = $handler;
        }

        Cascade::fileConfig($configuration);
        self::$initialized = true;
    }
}