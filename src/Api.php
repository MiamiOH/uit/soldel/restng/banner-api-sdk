<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 1:20 PM
 */

namespace MiamiOH\BannerApi;


use MiamiOH\BannerApi\Api\ApiPackage;
use MiamiOH\BannerApi\Api\ResourceDefinitionCollection;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevel;
use MiamiOH\BannerApi\Resource\Course\Course;
use MiamiOH\BannerApi\Resource\Subject\Subject;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;

class Api
{
    /**
     * This date format is used when generating date values to send to a
     * Banner API resource. The format appears to be consistent with the
     * W3C format (amoung others), but by consistently using this constant
     * to format dates, we can change the entire SDK if necessary.
     */
    const DATE_FORMAT = \DateTime::W3C;
    const NIL_GUID = '00000000-0000-0000-0000-000000000000';

    /**
     * @var ResourceLoaderInterface
     */
    private $loader;

    public function __construct(ResourceLoaderInterface $loader)
    {
        $this->loader = $loader;
    }

    public function getStudentApiPackage(): ApiPackage
    {
        return $this->loader->getApiPackage(ApiPackage::STUDENT);
    }

    public function getIntegrationApiPackage(): ApiPackage
    {
        return $this->loader->getApiPackage(ApiPackage::INTEGRATION);
    }

    // TODO add optional version to all get resource methods
    public function getCoursesResource(int $version = null): Course
    {
        return new Course($this->loader, $version);
    }

    public function getSubjectsResource(int $version = null): Subject
    {
        return new Subject($this->loader, $version);
    }
    
    public function getAcademicLevelsResource(int $version = null): AcademicLevel
    {
        return new AcademicLevel($this->loader, $version);
    }

}