<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/17
 * Time: 1:17 PM
 */

namespace MiamiOH\BannerApi;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use MiamiOH\BannerApi\Authentication\BannerApiAuthentication;
use MiamiOH\BannerApi\Authentication\EihAuthentication;
use MiamiOH\BannerApi\Authentication\NullAuthentication;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use Psr\Log\LogLevel;

class ApiFactory
{

    public static function getHttpClient(array $clientOptions = []): Client
    {
        return new Client($clientOptions);
    }

    public static function getNullAuthentication(): NullAuthentication
    {
        return new NullAuthentication();
    }

    public static function getBannerApiAuthentication(string $username, string $password): BannerApiAuthentication
    {
        return new BannerApiAuthentication($username, $password);
    }

    public static function getEihAuthentication(string $url, string $token): EihAuthentication
    {
        return new EihAuthentication(self::getHttpClient(), $url, $token);
    }

    public static function getBannerApiLocation(string $url, BannerApiAuthentication $authentication): Location
    {
        return new BannerApiLocation($url, $authentication);
    }

    public static function getNullLocation(string $url, NullAuthentication $authentication): Location
    {
        return new NullLocation($url, $authentication);
    }

    public static function getEihLocation(string $url, EihAuthentication $authentication): Location
    {
        return new EihLocation($url, $authentication);
    }

    public static function getResourceLoaderRest(Location $location, bool $enableLogging = true): ResourceLoaderRest
    {
        $clientOptions = [];

        if ($enableLogging) {
            $stack = HandlerStack::create();
            $stack->push(
                Middleware::log(
                    LogManager::logger('rest'),
                    new MessageFormatter("{request}\n{response}"),
                    LogLevel::DEBUG
                )
            );
            $clientOptions['handler'] = $stack;
        }

        return new ResourceLoaderRest($location, self::getHttpClient($clientOptions));
    }

    public static function getApi(ResourceLoaderInterface $loader): Api
    {
        return new Api($loader);
    }

    public static function getBannerApi(string $baseUrl, string $username, string $password): Api
    {
        $auth = ApiFactory::getBannerApiAuthentication($username, $password);
        $location = ApiFactory::getBannerApiLocation($baseUrl, $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);
        return new Api($loader);
    }

    public static function getEihApi(string $baseUrl, string $token): Api
    {
        $auth = ApiFactory::getEihAuthentication($baseUrl, $token);
        $location = ApiFactory::getEihLocation($baseUrl, $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);
        return new Api($loader);
    }
}