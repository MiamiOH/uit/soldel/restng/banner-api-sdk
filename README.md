# Banner API SDK

The Banner API SDK (Software Development Kit) provides a PHP library enabling access to Banner business objects using either the Banner APIs directly (student, integration, etc) or via the Ellucian Integration Hub (EIH) proxy service. The EIH proxy service acts as a simple proxy to the Banner APIs, differing only in the location and authentication mechanism. The SDK makes switching between the two as easy as instanciating different location and authentication objects. All other usage should be the same.

## What to Expect

Our goal with this SDK is to provide PHP developers with seamless access to Banner APIs via both direct access or EIH. Our guiding principle is to implement the API resources, not extend or enhance them. Therefore classes in the SDK are restricted to data provided by the corresponding Banner API as defined in the relevant Higher Education Data Model (HEDM).

The practical implication is that the SDK provides no convenience methods for loading related models or implmenting processing logic. For example, if you load a section model from the Banner API sections resource, it will contain the GUID of the related course. The Banner API does not provide a convenience method for automatically loading the related course model, therefore, the SDK will not provide such a convenience method either.

## Installation

The SDK will be available for installation via composer. Add the appropriate repository to your composer.json file:

```json
"repositories": [
    {
        "type": "composer",
        "url": "https://satis.itapps.miamioh.edu/miamioh"
    }
]
```

Then add the package to the require section:

```json
"require": {
    "miamioh/banner-api-sdk": "dev-master"
}
```

The package can then be managed by running composer install or update.

## Usage

The Banner APIs are available directly or through the Ellucian Integration Hub. The goal of the SDK is provide consistent access to the API regardless of the access point. You choose an access point by creating specific API objects. All subsequent interactions with API resources are not impacted by the access point selected.

Your application must include the composer autoload file or resolve the class loading through another mechanism.

### Configuration

While the SDK can support several versions of resources, the Banner APIs deploy no more than two concurrently. Therefore the actual versions enabled in the SDK, as well as the default, should be set at run time based on the Banner API being consumed. 

The BannerResource::setResourceConfig static method takes an array of resource configurations. A sample YAML file is included (examples/resources-dist.yml) and will be kept updated for current releases. This file is used for unit tests and example scripts.

To set the configuration, load the array using whatever means are appropriate and pass it to the setResourceConfig method before making any SDK calls:

```php
BannerResource::setResourceConfig(
    yaml_parse_file(__DIR__ . '/resources-dist.yml')['resources']
);
```

### Creating an API Object

Both the direct Banner API and EIH require a base URL and credentials. The Banner API requires a valid username and password while EIH requires a consumer application API token. You are responsible for managing those values in an secure manner for your application and deployment.

To use the direct Banner API access point, use the ApiFactory::getBannerApi() method:

```php
$api = ApiFactory::getBannerApi($baseUrl, $username, $password);
```

To use the EIH access point, use the ApiFactory::GetEihApi() method:

```php
$api = ApiFactory::getEihApi($baseUrl, $token);
```

The resulting API object is of the same type, but configured with appropriate location and authentication for the desired access point.

### Accessing API Resources

Use the API object to get the specific resource you want to use:

```php
$coursesResource = $api->getCoursesResource();
```
You can also get the specific version resource by passing version to the above method. For example:

```php
$version = 8;
$coursesResource = $api->getCoursesResource($version);
```

#### GET Individual Models

The resource object can load individual models by GUID. Note that a GUID value object is required.

```php
$courseId = '82e3958e-1365-4566-8c9b-6eb668b16177';

$course = $coursesResource->findOne()->withId(new Guid($courseId))->execute();

print "Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
```

At this time, the only observed identifier for models is GUID. If Ellucian makes other identifiers available for loading individual models, the Finder() class will be updated.

#### GET Collections

The resource returns a collection of models for any request without a GUID. Resources may implemented specific filtering options and some support ad hoc queries (https://resources.elluciancloud.com/bundle/ethos_data_model_ref_api_standards/page/API-filtering.html). The resulting collection will contain zero, one or many resource model objects, but will always be a valid collection.

```php
$courseList = $coursesResource->find()->withNumber('101')->execute();

print "Course List for number 101\n";
foreach ($courseList as $course) {
    print "Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
}
```

##### Paging Collections
Collections are paged and appear to have a default max entry limit of 10. If you don't specify a limit, you will get at most 10 models back regardless of the number matching your request. You can easily change both the limit and the initial offset via the Finder object. The resulting collection also knows the total matching entries. For example:

```php
$courseList = $courseResource->find()->withNumber('101')->withOffset(20)->withLimit(30)->execute();

print "Total: " . $courseList->getTotalCount() . "\n";
foreach ($courseList as $course) {
    print "$count Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
}
```
 You can also request a Pager object to handle the paged requests for you. Note that the total count is not available until after the first request is made.
 
 ```php
 $pager = $courseResource->find()->withNumber('101')->withOffset($offset)->withLimit($limit)->pager();
 
 $count = 0;
 while ($courseList = $pager->next()) {
     if ($count === 0) {
        print "Total: " . $courseList->getTotalCount() . "\n";
     }
     
     foreach ($courseList as $course) {
         $count++;
         print "$count Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
     }
 }
```

### Logging

The SDK uses Monolog (https://github.com/Seldaek/monolog) for PSR-3 (https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md) compatible logging. We are also using the Monolog Cascade extension (https://github.com/theorchard/monolog-cascade) to make log configuration a bit easier.

The default log configuration creates a logger with the log level set to ERROR and sent to STDERR. You may use any of the following options to change the log behavior.

**IMPORTANT!** The log configuration must be done prior to the first use of a logger. You should make any log configuration changes before using the ApiFactory class or manually creating any SDK objects.

#### Changing the Log Level

The log level used by the default configuration can be easily changed via the LogManager class using the string representation of one of the supported log levels (https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md#log-levels).

```php
LogManager::setLogLevel('DEBUG');
```

#### Logging to a file

The default logging is to STDERR only. You can enable logging to a file by setting a log file name. This will change the default logger to be file based, rather than using STDERR.

```php
LogManager::setLogFile('banner.log');
```

#### Providing a Complete Configuration

The SDK uses the Monolog Cascade extension (https://github.com/theorchard/monolog-cascade) to configure loggers. You can easily provide a complete configuration to tailor the log behavior to your needs. Note that this replaces the entire default configuration.

```php
LogManager::setConfiguration($config);
```

The $config value is passed directly to Cascade::fileConfig() and therefore must be fully compatible with that expectation. The Cascade config class can use Yaml, JSON, a PHP file returning an array or a PHP array. Refer to the Cascade documentation for details regarding the configuration.

You must use this method if you want to configure specific loggers with non-default options. Otherwise all loggers inherit the default options.

#### Using Loggers

Monolog uses channels to identify loggers and distinguish log entries. The LogManager class defines a 'default' logger which is used as a base for all other named loggers when using the default configuration. To get a logger in your class, use the SDK's LogManager class:

```php
$logger = MiamiOH\BannerApi\LogManager::logger('my_logger');

// or

MiamiOH\BannerApi\LogManager::logger('my_logger')->error('some error happened');
```

The logger method returns a configured Monolog Logger object.

The SDK defines some channels for specific logging purposes. For example, the MiamiOH\BannerApi\Resource\ResourceLoaderRest class uses the 'rest' channel for it's log entries.

Each resource should use it's BannerResource value object as the channel name, which will cause the resource name to appear in the log messages for that resource. The resource base classes will use this technique to ensure log entries are easily associated with the appropriate resource.

You may use the Monolog Cascade configuration to configure different behaviors for the named loggers if desired.

## Examples

The SDK distribution includes some basic examples to demonstrate its use. You can find these in the examples directory at the root of the project. Note that these examples use the Banner API by default. You may change this within your project, but please do not commit any such changes to the repo. These examples also use Miami's development Banner instance by default, so access and data are subject to certain restrictions and may possibly change.

The Banner API SDK, and all examples, require PHP version >=7.1.

### Courses Resource as a Reference

The development team used the Courses resource for the initial implementation. It it has been crafted as a reference for how to use and implement a resource. We use it throughout the documentation and examples. We encourage you to look at the source code for addtional comments and implementation details.

### Running the examples

You will need to provide proper Banner API credentials or an EIH token for your needs. Copy the config-dist.ini file to config.ini and add the appropriate values. Run php from the command line at the root of your project, specifying the example script.

```php
php examples/basic.php
```

### Basic Example

The basic.php script is a commented example of how to aquire a configured Api object and fetch a resource model. It prints a few attributes of the fetched model to the console.

### Paging Example

The paging.php script demonstrates using the offset and limit methods to fetch pages of models from a resource. it also demonstrates the use of a Pager object to hide the offset and limit tracking from the consumer app.

### Logging Example

Logging should be a first class consideration within the SDK classes. Developers using the SDK should find relevant, valuable information regarding behavior and errors in log messages. However, the SDK cannot make too many assumptions regarding the runtime environment and appropriate log configuration. The example/logging.php script shows how to configure and use the SDK logging facility.

```php
php examples/logging.php
```

If you create the file examples/logconfig.yml, the logging.php script will use it to configure the logger.

## Contributing

The Banner API SDK will not provide a complete implementation of the Banner resources with it's initial release, or even with any near term updates. Each Solution Delivery team intending to use the SDK will likely need to add one or more resources, or enhance existing resources, to accomplish their goals. Contributing should be easy, but strict guidelines should be followed due to the nature of an SDK.

The SDK has two goals and you need to understand both to be successful. First and foremost, the SDK provides access to the Banner APIs. Developers can include the SDK as a composer dependency and easily access API resources in their application. The second goal of the SDK is to enable an IDE (for example, PhpStorm) to provide maximum value to developers. Some implementation decisions and requirements were made to inform the IDE of concrete parameter and return types so it, in turn, can inform the developer.
 
 While accomplishing both of these goals requires slightly more work, we believe the investment will pay off as developers gain increasing value from the SDK.

### Requirements

The Banner API SDK requires PHP 7.1 or later. You can use the Solution Delivery local development VMs or the shared PHP development server.

### Workspace Setup

You will have the best experience using PhpStorm, but any text editor will work.

Clone the project from GitLab and set up your editor.

```
git clone git@git.itapps.miamioh.edu:solution-delivery-dev/banner-api-sdk.git
```

The SDK uses a number of dependencies via composer. Change into your working directory and run composer.

```
cd banner-api-sdk
composer install
```

That should be all you need to do in order to begin development. Any contributions must include proper unit tests, however, so you should make sure the tests all pass prior to making any changes.

### Classmap

The SDK uses the composer classmap functionality for generating autoload configuration. This not only decouples the class namespace from the file system, but also makes the resulting classmap static and therefore cacheable. When you add new classes as part of development, you will need to have composer generate a new autoload file before PHP can find them. In the project root, run:

```php
composer dump-autoload
```

### Tests

The Banner API SDK project contains unit tests which should provide comprehensive coverage. Units test are written in PHPUnit. Run the tests from the root of the Banner API SDK project directory.

```php
vendor/bin/phpunit
```

Please note that unit tests must be declared in the namespace MiamiOH\BannerApi\Tests to keep them separate from the operational classes.

### Guidelines

We expect that many people will contribute to this SDK over time. The following guidelines are intended to maintain a high level of sustainability and consistency. You should be familiar with these guidelines before starting a project. Only changes consistent with these guidelines will be accepted into the SDK for release.

**Use GitLab issues, branches and merge requests for submitting changes.** Any change starts with an issue. Issues should be created for any new feature, bug or other change. We recommend using GitLab to create the branch and merge request so GitLab can track the relationship. The merge request will be used for a code review and conversation regarding the change.

**Observe PHP7 best practices and SolDel PHP coding conventions.** The guidelines can currently be found on GitLab at: https://git.itapps.miamioh.edu/solution-delivery-dev/soldel-coding-guidelines/blob/master/guidelines/php/guidelines.MD

**Ensure full test coverage by practicing test first development.** Bugs or regressions in an SDK can have far wider impact than in an individual application. Complete test coverage helps minimize possible problems from changes. Using test first development and only writing code to pass the test helps ensure correct coverage.

**Place all source files related to a resource within a directory named for the API resource in the Resource directory.** We will likely implement dozens of resources and organzing the source code is important. Keeping all source files related to a single resource together makes them easy to find and work with. If classes applying to more than a single resource are identified, a 'common' directory should be created to contain them.

**Public methods must have DocBlock comments describing the method.** The DocBlock must minimally contain a summary, description of the method and descriptions of all parameters. Note that defining parameters in the DocBlock does not replace proper type declarations in the method signature.

**Restrict models to data returned from the corresponding resource.** The purpose of the SDK is to provide access to the data and features Ellucian offers via the Banner APIs. Resources must not fetch, calculate or otherwise generate data not found in the corresponding HEDM. SDK classes must not provide convenience methods to fetch related models or implement processing logic.

**Name getter, setter, filter and other methods for the relevant HEDM attribute.** Any method which manipulates HEDM model objects or attributes of those models must be named after the relevant model or attribute. The following examples demonstrate this guideline, but are not comprehensive. Please use appropriate judgement.

```php
CourseCollection::addCourse(CourseModel $course): void;
CouresCollectionFinder::withSubject(Guid $subjectId): CourseCollectionFinder;
CourseModel::getSubject(): Guid;
CourseModel::setSubject(Guid $subjectId): void;
 ```

**Use fluent methods for configuring complex operations.** The fluent method pattern allows developers to easily build complex operations while reducing the risk of future SDK changes. The CollectionFinder classes are useful example of the benefit the fluent pattern brings. The possible filter arguments vary by resource and may change over time. By using the fluent pattern 'with' methods, we can easily add filters without impacting current usage. Developers can also easily use filters in any combination. For example:

```php
$courseList = $courseResource->find()
              ->withAcademicLevels($levelId)
              ->withSubject($subjectId)
              ->execute();
```

The fluent method pattern not only makes future changes easy to make with minimum risk, but is also highly readable by developers.

**Avoid using plain arrays in favor of supporting classes and collections of objects.** The HEDM returned by a resource often contains complex data structures as the attribute values. For example, the 'credits' attribute of the course model contains a reference to a creditCategory resource, composed of an ID and type string, along with minimum, maximum and increment values specific to the course. The CourseModel class should use a supporting class (i.e. CourseCredit) to represent this data. Primatives and arrays of primatives are acceptable as model attribute values.

**Use the Carbon date library for all date values.** The Carbon library provides easy, consistent and testable date handling through the SDK. Any date value must be implemented as a Carbon object. When outputting a date value to be sent to the Banner APIs (filter, model, etc), use $date->format(Api::DATE_FORMAT) to ensure the correct format.

**Create and use specific exceptions where applicable.** Exceptions must be thrown for any error condition. If an existing base or SDK exception is not appropriate, create a new exception in the Exceptions directory and use it. Meaningful, granular exceptions allow developers to understand problems and handle error conditions.

**Include appropriate log entries.** Most resources may not need logging beyond the SDK REST request/response logging. However, anything that may be useful for debugging or operational support should be logged appropriately. 

### Defining a Resource

The Banner APIs follow a consistent convention for requests and response. This allows the SDK to implement all common functionality in base classes. Individual resources extend the base classes to provide concrete implementations and resource specific features.

The SDK must know about valid API sources and resources via two value object classes. This allows for reliable validation with minimal overhead.

#### ApiSource Value Object

The Banner APIs are implemented via specific API packages, which causes the path to vary by resource. The Ellucian Integration Hub masks this by assigning resources to source systems. In order for the SDK to work with both the Banner APIs and EIH, each resource must have a valid source.

The ApiSource value object represents sources throughout the SDK. If a new source is added to the Banner APIs, an appropriate valid source must be added to this class.

The path used to access the source is part of the Location class since it will vary between the Banner API and EIH implementations. Each Location class must be updated when a new source is added.

#### BannerResource Value Object

Ellucian defines API resources by name and each resource is implemented via a specific source. The BannerResource value objects represents the resource throughout the SDK and provides the source which implements it.

The BannerResource class must be configured with supported resource information. This is done at run time via the setResourceConfig() class method. The deployment of an application using the SDK is responsible for this configuration, but for SDK development we use the examples/resources-dist.yml file. This file is loaded by the PHPUnit bootstrap file as well as each example script.

When you add a new resource, or change versions of a resource, you must update the resources-dist.yml file as well. An example resource in YAML looks like:

```yaml
  courses:
    source: student
    default_version: 8
    versions: [6,8]
```
The hash key must be the Banner API resource name and is used to construct the URL to the resource. The default version will be used if the SDK consumer does not specify a version. The versions must be an array of versions enabled in the Banner API instance being used.

#### Api Class Methods

The Api class should have a single method which creates and returns a new concrete Resource object. Consumers are expected to use this method to acquire the resource. The Api method name should reflect the Banner API resource name. The 'courses' resource method name would be 'getCoursesResource' for example.

### Resource Implementation

An API resource is implemented by creating a directory named after the Banner resource and located in the Resource directory. The following class files need to be created to provide concrete implementations of the resource. Any supporting classes related to the resource should also be contained in this directory.

If supporting classes which apply to multiple resources are identified, a 'common' directory should be created and used accordingly.

Unit test classes for the resource should be organized in a matching directory under tests/Resource/{resourceName}.

Refer to the Courses resource for a reference implementation with annotations and comments.

#### Resource Class

Resources require an implementation of the abstract Resource class. This class should be named for the singular of the resource being implemented. The sub-class sets the BannerResource for the class and implements FindableInterface methods to provide concrete ModelFinder and CollectionFinder classes.

#### Resource Model

Resources must have a class implementing the abstract BaseModel class. This class should be named for the singulur of the resource with 'Model' appended (i.e. CourseModel). The class must define a setData method which accepts a plan array following the Ellucian HEDM representation of the model. The class must validate the data and set its internal state. Getter and setter methods must also be created consistent with the model. The class must implement an asEthosDataModelArray() method to return a plan array representation of the model.

Any complex (non-primative or array of primatives) values in the model must be represented by (possibly resource specific) classes. GUIDs and arrays of GUIDs are supported by the GUID and GUIDCollection classes. All other array structures must be converted to appropriate classes for validation and use.

#### Resource Collection

Resources must have a class implementing the abstract BaseCollection class. This class should be named for the singulur of the resource with 'Collection' appended (i.e. CourseCollection). The class must setModelClass() method to set the concrete class it will contain. The class must also implement a getIterator() method to return a concrete iterator type.

The supporting iterator class must also by created by extending BaseCollectionIterator and providing a constructor and current() method with appropriate types.

#### Resource Model Finder

Resources must have a class implementing the abstract BaseModelFinder class. This class should be named for the singular of the resource with "ModelFinder" appended (i.e. CourseModelFinder). The class will implement execute() method. The execute method will use the composed resource loader to find the model. 

The current means of loading a single model is limited to GUID. This is consistent with Ellician's API pattern. All find and filter operations are performed via the resource collection.

#### Resource Collection Finder

Resources must have a class implementing the abstract BaseCollectionFinder class. This class should be named for the singular of the resource with "CollectionFinder" appended (i.e. CourseCollectionFinder). The class will implement the execute() method along with methods for any filters supported by the resource. The execute method will use the composed resource loader to find matching models and return a collection. A pager() method must also be implemented to return an instance of the resource CollectionPager class.

#### Resource Collection Pager

Resources must have a class implementing the abstract BaseCollectionPager class. This class should be named for the singluar of the resource with "CollectionPager" appended (i.e. CourseCollectionPager). The class will implement a constructor accepting the concrete resource CollectionFinder class. The next() method must be implemented to verify next is a valid navigation, execute the finder and update the internal state before returning a concrete resource collection.

#### Resource Specific Versions

Whenever there is a Banner Ethos API Upgrade, there can be additional versions introduced for the resources. For example, In the Ethos 9.9 Upgrade, Course resource has 2 versions (i.e. version 6 and 8) implemented. For every new version release, the Ethos documentation should be referred and additional changes to the resource should be implemented in a new version specific class, which extends the Resource model class. For example, Course version 8 is implemented in CourseModelV8 which extends the CourseModel class(it represents base version 6). Billing and ReportingDetail were two new attributes introduced in the Course v8 version.

#### Resource Specific Supporting Classes

HEDM resource models may contain complex data structures which are not composed data models themselves. These data structures must represented by resource specific classes to aid in resource usage. These classes should be created in the same directory as the reource which they belong to. The classes should be named for the singular of the resource and the singular of the model element represented. For example, the course credits class name would be CourseCredits. Collection classes should have the word "Collection" appended, for example CourseCreditCollection.

The course credits data is represented in the HEDM as:

```php
        [
            'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
            ...
            'credits' => [
                [
                    'creditCategory' => [
                        'creditType' => 'institution',
                        'detail' => [
                            'id' => 'bd582974-7421-4502-8fd4-b3966e08923e'
                        ]
                    ],
                    'measure' => 'credit',
                    'minimum' => '1',
                    'maximum' => '4',
                    'increment' => '1'
                ]
            ],
            ...
        ];
```

This must be implemented as a CourseCreditCollection containing CourseCredit objects. The collection and model must implement the HedmInterface so they can be properly composed as part of the Course HEDM data model.

The internal represenation of the data is up to the implementation. In the case of course credits, the creditCategory key is a reference to another resource which has been composed into this data model, including only the creditType and id for convenience. The CourseCredits class flattens that internally, but is able to produce the proper array structure when asked.

Any HEDM attribute which is not a simple scalar or array of scalars must be implemented with such a class. It is prudent, but not required, to implement model and collection classes for an array of scalars if there is any benefit. As we gain experience, we may find that supporting classes can be reused (via a 'common' source directory) or may benefit from one or more base class or interfaces.
