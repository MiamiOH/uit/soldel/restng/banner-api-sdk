<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 11:05 AM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\Authentication\NullAuthentication;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\InvalidResourceSourceException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Location;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{

    private $baseUrl = '';
    private $studentApiPath = '';
    private $resourceName = '';
    /**
     * @var Location
     */
    private $location;
    /**
     * @var BannerResource
     */
    private $resource;

    public function setUp(): void
    {
        $this->baseUrl = 'https://example.org';
        $this->studentApiPath = 'path/to/studentApi';
        $this->resourceName = 'api-test-ok';
        $this->location = new Location($this->baseUrl, new NullAuthentication(), ['student' => $this->studentApiPath]);
        $this->resource = new BannerResource($this->resourceName);
    }

    public function testCanGetBaseUrl(): void
    {
        $this->assertEquals($this->baseUrl, $this->location->getBaseUrl());
    }

    public function testCanGetAbsoluteLocation(): void
    {
        $this->assertEquals(
            implode('/', [$this->baseUrl, $this->studentApiPath, $this->resourceName]),
            $this->location->getAbsoluteLocation($this->resource)
        );
    }

    public function testCanGetAbsoluteLocationWithGuid(): void
    {
        $guid = new Guid('82e3958e-1365-4566-8c9b-6eb668b16177');
        $this->assertEquals(
            implode('/', [$this->baseUrl, $this->studentApiPath, $this->resourceName, $guid]),
            $this->location->getAbsoluteLocation($this->resource, $guid)
        );
    }

    public function testCanNotGetLocationForUnknownApiType(): void
    {
        $this->location = new Location($this->baseUrl, new NullAuthentication(), ['person' => 'path/to/api']);

        $this->expectException(InvalidResourceSourceException::class);
        $this->location->getAbsoluteLocation($this->resource);
    }

    public function testUsesNullAuthenticationByDefault(): void
    {
        $headers = ['X-my-header' => 'some-header-value'];
        $this->assertEquals($headers, $this->location->addAuthenticationHeaders($headers));
    }

}
