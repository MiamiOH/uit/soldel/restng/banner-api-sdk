<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 12:51 PM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\Authentication\EihAuthentication;
use MiamiOH\BannerApi\EihLocation;
use PHPUnit\Framework\TestCase;

class EihLocationTest extends TestCase
{

    /**
     * @var EihAuthentication
     */
    private $eihAuthentication;

    public function setUp(): void
    {
        $this->eihAuthentication = new EihAuthentication(ApiFactory::getHttpClient(), 'https://integrateapi.elluciancloud.com', 'a0dcdffa-5319-4459-b353-8c9625561933');
    }

    public function testCanBeCreatedWithAuthentication(): void
    {
        $this->assertInstanceOf(
            EihLocation::class,
            new EihLocation('https://example.com', $this->eihAuthentication)
        );
    }
}
