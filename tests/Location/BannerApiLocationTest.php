<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 12:51 PM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\Authentication\BannerApiAuthentication;
use MiamiOH\BannerApi\BannerApiLocation;
use PHPUnit\Framework\TestCase;

class BannerApiLocationTest extends TestCase
{

    /**
     * @var BannerApiAuthentication
     */
    private $bannerApiAuthentication;

    public function setUp(): void
    {
        $this->bannerApiAuthentication = new BannerApiAuthentication('saisusr', 'secr3t');
    }

    public function testCanBeCreatedWithAuthentication(): void
    {
        $this->assertInstanceOf(
            BannerApiLocation::class,
            new BannerApiLocation('https://example.com', $this->bannerApiAuthentication)
        );
    }
}
