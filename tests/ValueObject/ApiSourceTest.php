<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 8:25 AM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\ApiSource;
use MiamiOH\BannerApi\Exception\InvalidApiSourceException;
use PHPUnit\Framework\TestCase;

class ApiSourceTest extends TestCase
{
    public function testCanBeCreatedFromString(): void
    {
        $this->assertInstanceOf(ApiSource::class, ApiSource::fromString('student'));
    }

    public function testCanNotBeCreatedWithInvalidValue(): void
    {
        $this->expectException(InvalidApiSourceException::class);
        ApiSource::fromString('unkown');
    }
}
