<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 3:56 PM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\Exception\InvalidGuidException;
use MiamiOH\BannerApi\Guid;
use PHPUnit\Framework\TestCase;

class GuidTest extends TestCase
{
    public function testCanNotBeCreatedFromInvalidString()
    {
        $this->expectException(InvalidGuidException::class);
        new Guid('bob');
    }

    public function testCanBeCreatedFromString(): void
    {
        $this->assertInstanceOf(Guid::class, new Guid('0d97179e-488e-4717-9510-6672f2a625f7'));
    }

    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals('0d97179e-488e-4717-9510-6672f2a625f7', new Guid('0d97179e-488e-4717-9510-6672f2a625f7'));
    }

    public function testCanBeSerializedToJson(): void
    {
        $this->assertEquals(
            json_encode(['id' => '0d97179e-488e-4717-9510-6672f2a625f7']),
            json_encode(['id' => new Guid('0d97179e-488e-4717-9510-6672f2a625f7')])
        );
    }
}
