<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 8:36 AM
 */

namespace MiamiOH\BannerApi\Tests;

use Carbon\Carbon;
use MiamiOH\BannerApi\CollectionResourceOptions;
use MiamiOH\BannerApi\Exception\InvalidResourceOptionException;
use PHPUnit\Framework\TestCase;

class CollectionResourceOptionsTest extends TestCase
{
    private $options = [];
    /**
     * @var CollectionResourceOptions
     */
    private $resourceOptions;

    public function setUp(): void
    {
        $this->options = [
            'date' => Carbon::now(),
            'mediaType' => 'json',
            'requestId' => 'abc123',
            'message' => 'test resource',
            'pageMaxSize' => 10,
            'pageOffset' => 0,
            'totalCount' => 23,
        ];

        $this->resourceOptions = new CollectionResourceOptions($this->options);
    }

    public function testCanBeCreatedWithOptions(): void
    {
        $this->assertInstanceOf(CollectionResourceOptions::class, $this->resourceOptions);
    }

    public function testCanGetDate(): void
    {
        $this->assertEquals($this->options['date'], $this->resourceOptions->getDate());
    }

    public function testCanGetMediaType(): void
    {
        $this->assertEquals($this->options['mediaType'], $this->resourceOptions->getMediaType());
    }

    public function testCanGetRequestId(): void
    {
        $this->assertEquals($this->options['requestId'], $this->resourceOptions->getRequestId());
    }

    public function testCanGetMessage(): void
    {
        $this->assertEquals($this->options['message'], $this->resourceOptions->getMessage());
    }

    public function testCanGetPageMaxSize(): void
    {
        $this->assertEquals($this->options['pageMaxSize'], $this->resourceOptions->getPageMaxSize());
    }

    public function testCanGetPageOffset(): void
    {
        $this->assertEquals($this->options['pageOffset'], $this->resourceOptions->getPageOffset());
    }

    public function testCanGetTotalCount(): void
    {
        $this->assertEquals($this->options['totalCount'], $this->resourceOptions->getTotalCount());
    }

    public function testRequiresPageMaxSizeToBeInteger(): void
    {
        $this->options = [
            'pageMaxSize' => '10',
        ];

        $this->expectException(InvalidResourceOptionException::class);
        new CollectionResourceOptions($this->options);
    }

    public function testRequiresPageOffsetToBeInteger(): void
    {
        $this->options = [
            'pageOffset' => '10',
        ];

        $this->expectException(InvalidResourceOptionException::class);
        new CollectionResourceOptions($this->options);
    }

    public function testRequiresTotalCountToBeInteger(): void
    {
        $this->options = [
            'totalCount' => '10',
        ];

        $this->expectException(InvalidResourceOptionException::class);
        new CollectionResourceOptions($this->options);
    }


    public function testDateMustBeCarbon(): void
    {
        $options = [
            'date' => new \stdClass(),
        ];

        $this->expectException(InvalidResourceOptionException::class);
        new CollectionResourceOptions($options);
    }
}
