<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 8:26 AM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\ApiSource;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\InvalidResourceException;
use MiamiOH\BannerApi\Exception\ResourceConfigurationNotLoadedException;
use MiamiOH\BannerApi\Exception\ResourceNoVersionException;
use MiamiOH\BannerApi\Exception\ResourceUnsupportedVersionException;
use PHPUnit\Framework\TestCase;

class BannerResourceTest extends TestCase
{

    private static $resourceConfig = [];

    public static function setUpBeforeClass(): void
    {
        self::$resourceConfig = BannerResource::getResourceConfig();
    }

    public function setUp(): void
    {
        BannerResource::setResourceConfig([
            'api-test-ok'        => ['source' => 'student', 'default_version' => 6, 'versions' => [4,6,8]],
            'api-test-noversion' => ['source' => 'student'],
        ]);
    }

    public static function tearDownAfterClass(): void
    {
        BannerResource::setResourceConfig(self::$resourceConfig);
    }

    /*
     * The resource configuration must be loaded for a large number of tests to run.
     * It's not practical to do that in every class, so it's in the bootstrap. We
     * can reset the configuration to simulate the default state.
     */
    public function testResourceRequiresConfiguration(): void
    {
        BannerResource::resetResourceConfig();
        $this->assertNull(BannerResource::getResourceConfig());
        $this->expectException(ResourceConfigurationNotLoadedException::class);
        BannerResource::fromString('api-test-ok');
    }

    public function testCanBeCreatedFromString(): void
    {
        $this->assertInstanceOf(BannerResource::class, BannerResource::fromString('api-test-ok'));
    }

    public function testCanNotBeCreatedFromUnknownResource(): void
    {
        $this->expectException(InvalidResourceException::class);
        BannerResource::fromString('bubba');
    }

    public function testCanGetSourceOfResource(): void
    {
        $resource = BannerResource::fromString('api-test-ok');
        $this->assertInstanceOf(ApiSource::class, $resource->source());
        $this->assertEquals('student', $resource->source());
    }

    public function testCanGetVersionOfResource(): void
    {
        $resource = BannerResource::fromString('api-test-ok');
        $this->assertEquals(6, $resource->version());
    }

    public function testExpectsDefaultVersion():void
    {
        $this->expectException(ResourceNoVersionException::class);
        BannerResource::fromString('api-test-noversion');
    }

    public function testCanRequestOtherVersion(): void
    {
        $resource = BannerResource::fromString('api-test-ok', 8);
        $this->assertEquals(8, $resource->version());
    }

    public function testRequestVersionMustBeSupported(): void
    {
        $this->expectException(ResourceUnsupportedVersionException::class);
        BannerResource::fromString('api-test-ok', 7);
    }

    public function testCanGetResourceVersionMimeType(): void
    {
        $resource = BannerResource::fromString('api-test-ok', 6);
        $this->assertEquals('application/vnd.hedtech.integration.v6+json', $resource->versionMimeType());
    }
}
