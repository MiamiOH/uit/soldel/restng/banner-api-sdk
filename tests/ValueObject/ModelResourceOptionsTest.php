<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 8:36 AM
 */

namespace MiamiOH\BannerApi\Tests;

use Carbon\Carbon;
use MiamiOH\BannerApi\Exception\InvalidResourceOptionException;
use MiamiOH\BannerApi\ModelResourceOptions;
use PHPUnit\Framework\TestCase;

class ModelResourceOptionsTest extends TestCase
{
    private $options = [];
    /**
     * @var ModelResourceOptions
     */
    private $resourceOptions;

    public function setUp(): void
    {
        $this->options = [
            'date' => Carbon::now(),
            'mediaType' => 'json',
            'requestId' => 'abc123',
            'message' => 'test resource',
        ];

        $this->resourceOptions = new ModelResourceOptions($this->options);
    }

    public function testCanBeCreatedWithOptions(): void
    {
        $this->assertInstanceOf(ModelResourceOptions::class, $this->resourceOptions);
    }

    public function testCanGetDate(): void
    {
        $this->assertEquals($this->options['date'], $this->resourceOptions->getDate());
    }

    public function testCanGetMediaType(): void
    {
        $this->assertEquals($this->options['mediaType'], $this->resourceOptions->getMediaType());
    }

    public function testCanGetRequestId(): void
    {
        $this->assertEquals($this->options['requestId'], $this->resourceOptions->getRequestId());
    }

    public function testCanGetMessage(): void
    {
        $this->assertEquals($this->options['message'], $this->resourceOptions->getMessage());
    }

    public function testDateMustBeCarbon(): void
    {
        $this->options = [
            'date' => new \stdClass(),
        ];

        $this->expectException(InvalidResourceOptionException::class);
        new ModelResourceOptions($this->options);
    }
}
