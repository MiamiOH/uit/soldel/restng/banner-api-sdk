<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 8:14 AM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\Exception\InvalidCollectionEntryException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\GuidCollection;
use PHPUnit\Framework\TestCase;

class GuidCollectionTest extends TestCase
{
    public function testCanBeCreatedFromArrayOfStrings(): void
    {
        $guids = ['28530025-9551-4d23-af78-51d94fd12cee'];
        $this->assertInstanceOf(GuidCollection::class, GuidCollection::fromArrayOfIds($guids));
    }

    public function testCanBeCreatedFromArrayOfGuids(): void
    {
        $guids = [new Guid('28530025-9551-4d23-af78-51d94fd12cee')];
        $this->assertInstanceOf(GuidCollection::class, GuidCollection::fromArrayOfGuids($guids));
    }

    public function testCanBeCreatedFromAssocOfStringIds(): void
    {
        $guids = [
            ['id' => '28530025-9551-4d23-af78-51d94fd12cee']
        ];
        $this->assertInstanceOf(GuidCollection::class, GuidCollection::fromAssocOfIds($guids));
    }

    public function testRejectsNonGuidEntries(): void
    {
        $guids = [new \stdClass()];

        $this->expectException(InvalidCollectionEntryException::class);
        $this->assertInstanceOf(GuidCollection::class, GuidCollection::fromArrayOfGuids($guids));
    }

    public function testCanBeUsedAsArray(): void
    {
        $guids = [new Guid('28530025-9551-4d23-af78-51d94fd12cee')];

        $collection = GuidCollection::fromArrayOfGuids($guids);
        foreach ($collection->toArray() as $guid) {
            $this->assertInstanceOf(Guid::class, $guid);
        }
    }

    public function testCanBeCollectedAsArray(): void
    {
        $guids = [new Guid('28530025-9551-4d23-af78-51d94fd12cee')];

        $collection = GuidCollection::fromArrayOfGuids($guids);

        $this->assertEquals([['id' => '28530025-9551-4d23-af78-51d94fd12cee']], $collection->collect());
    }

    public function testCanBeCounted()
    {
        $guids = [
            new Guid('28530025-9551-4d23-af78-51d94fd12cee'),
            new Guid('34530025-9551-4d23-af78-51d94fd12cef'),
        ];

        $collection = GuidCollection::fromArrayOfGuids($guids);

        $this->assertCount(2, $collection);
    }

    public function testCanBeIterated(): void
    {
        $guids = [
            new Guid('28530025-9551-4d23-af78-51d94fd12cee'),
            new Guid('34530025-9551-4d23-af78-51d94fd12cef'),
        ];

        $collection = GuidCollection::fromArrayOfGuids($guids);

        foreach ($collection as $guid) {
            $this->assertInstanceOf(Guid::class, $guid);
        }
    }

    public function testCanBeIteratedAsAssoc(): void
    {
        $guids = [
            new Guid('28530025-9551-4d23-af78-51d94fd12cee'),
            new Guid('34530025-9551-4d23-af78-51d94fd12cef'),
        ];

        $collection = GuidCollection::fromArrayOfGuids($guids);

        foreach ($collection as $key => $guid) {
            $this->assertInstanceOf(Guid::class, $guid);
        }
    }
}
