<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/26/17
 * Time: 10:00 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\CollectionFilterRest;
use PHPUnit\Framework\TestCase;

class CollectionFilterRestTest extends TestCase
{

    public function testCanBeCreatedWithResource(): void
    {
        $this->assertInstanceOf(
            CollectionFilterRest::class,
            new CollectionFilterRest(new BannerResource('api-test-ok'))
        );
    }

    public function testCanAddSort(): void
    {
        $filter = new CollectionFilterRest(new BannerResource('api-test-ok'));
        $filter->addSort('attribute', 'asc');
        $this->assertCount(1, $filter);
    }

    public function testCanAddCriteria(): void
    {
        $filter = new CollectionFilterRest(new BannerResource('api-test-ok'));
        $filter->addCriteria('attribute', 'value');
        $this->assertCount(1, $filter);
    }

    public function testCanAddParam(): void
    {
        $filter = new CollectionFilterRest(new BannerResource('api-test-ok'));
        $filter->addParam('attribute', 'value');
        $this->assertCount(1, $filter);
    }

    public function testCanBuildVersion6Filter(): void
    {
        $filter = new CollectionFilterRest(new BannerResource('api-test-ok', 6));
        $filter->addSort('attribute1');
        $filter->addCriteria('attribute1', 'value1');
        $filter->addCriteria('attribute2', 'value2');
        $filter->addParam('attribute3', 'value3');
        $this->assertEquals(
                'attribute1=value1&attribute2=value2&attribute3=value3&sort=attribute1&order=asc',
            $filter->build()
        );
    }

    public function testCanBuildVersion8Filter(): void
    {
        $filter = new CollectionFilterRest(new BannerResource('api-test-ok', 8));
        $filter->addSort('attribute1');
        $filter->addCriteria('attribute1', 'value1');
        $filter->addCriteria('attribute2', 'value2');
        $filter->addParam('attribute3', 'value3');
        $this->assertEquals(
            'criteria='.urlencode(json_encode(['attribute1'=>'value1','attribute2'=>'value2'])).'&attribute3=value3&sort='.
            urlencode(json_encode(['attribute1' => 'asc'])),
            $filter->build()
        );
    }
}
