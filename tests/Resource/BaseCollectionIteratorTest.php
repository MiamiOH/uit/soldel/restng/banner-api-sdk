<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/30/17
 * Time: 9:31 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use MiamiOH\BannerApi\Collection\BaseCollectionIterator;
use MiamiOH\BannerApi\Resource\BaseModel;
use PHPUnit\Framework\TestCase;

class BaseCollectionIteratorTest extends TestCase
{

    /**
     * @var \MiamiOH\BannerApi\Tests\Resource\BaseCollectionIterator
     */
    private $iterator;

    public function setUp(): void
    {
        $this->iterator = new \MiamiOH\BannerApi\Tests\Resource\BaseCollectionIterator(
            [$this->getMockForAbstractClass(BaseModel::class)]
        );
    }

    public function testCanBeIterated(): void
    {
        foreach ($this->iterator as $key => $value) {
            $this->assertNotNull($value);
        }
    }
}
