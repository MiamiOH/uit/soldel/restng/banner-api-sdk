<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/30/17
 * Time: 9:36 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource;


class BaseCollectionIterator extends \MiamiOH\BannerApi\Resource\BaseCollectionIterator
{

    public function __construct(array $models)
    {
        $this->models = $models;
    }

    public function current()
    {
        return $this->models[$this->current];
    }
}