<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 1:29 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\Api\ApiPackage;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\ResourceBadRequestException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Resource\BaseCollection;
use MiamiOH\BannerApi\Resource\BaseModel;
use MiamiOH\BannerApi\Resource\ResourceInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Resource\Resource;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

class ResourceLoaderRestTest extends TestCase
{
    private $container = [];

    /**
     * @var ResourceInterface
     */
    private $testResource;
    /**
     * @var Guid
     */
    private $testGuid;

    private $model;
    /**
     * @var BaseCollection $collection
     */
    private $collection;
    
    private $index;
    
    public function setUp(): void
    {
        $this->index = 0;
        $this->testResource = $this->getMockBuilder(Resource::class)
            ->setMethods(['getResource', 'getNewModel', 'getNewModelCollection', 'find', 'findOne'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->testGuid = new Guid('0d97179e-488e-4717-9510-6672f2a625f7');
        
        $this->testResource->method('getNewModel')->will($this->returnCallback([$this, 'getMockedNewModel']));
        $this->collection = $this->getMockForAbstractClass(BaseCollection::class);
        
        $this->collection->setModelClass(BaseModel::class);
        $this->testResource->method('getNewModelCollection')->willReturn($this->collection);
        $this->testResource->method('getResource')->willReturn(new BannerResource('api-test-ok'));
    }

    public function testCanBeCreated(): void
    {
        $client = $this->newHttpClientWithResponses([]);

        $loader = $this->newResourceLoader($client);

        $this->assertInstanceOf(ResourceLoaderRest::class, $loader);
    }

    public function testGetDataByResourceId(): void
    {
        $data = [
            'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
            'content' => 'This is my content'
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(200, [], json_encode($data)),
        ]);

        $loader = $this->newResourceLoader($client);

        /** @var BaseModel $test */
        $test = $loader->getResourceById($this->testResource, $this->testGuid);

        $this->assertCount(1, $this->container);
        $this->assertEquals('0d97179e-488e-4717-9510-6672f2a625f7', $test->getId());
    }

    public function testGetDataByResource(): void
    {
        $data = [
            [
                'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
                'content' => 'This is my content'
            ],
            [
                'id' => '0d97179e-488e-4717-9510-6672f2a625f8',
                'content' => 'This is more content'
            ],
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(200, [], json_encode($data)),
        ]);

        $loader = $this->newResourceLoader($client);

        /** @var BaseCollection $test */
        $test = $loader->getResource($this->testResource);

        $this->assertCount(1, $this->container);
        
        $this->index = 0;
        
        foreach ($test->toArray() as $model) {
            $this->assertInstanceOf(get_class($this->model[$this->index++]), $model);
        }
    }

    public function testGetResourceWithCorrectMimeType(): void
    {
        $data = [
            [
                'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
                'content' => 'This is my content'
            ],
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(200, [], json_encode($data)),
        ]);

        $loader = $this->newResourceLoader($client);
        
        $loader->getResource($this->testResource);

        /** @var Request $request */
        $request = $this->container[0]['request'];

        $this->assertEquals(
            [$this->testResource->getResource()->versionMimeType()],
            $request->getHeader('Accept')
        );
    }

    public function testPostResourceWithCorrectMimeType(): void
    {
        $data = [
            [
                'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
                'content' => 'This is my content'
            ],
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(200, [], json_encode($data)),
        ]);

        $loader = $this->newResourceLoader($client);

        $this->markTestIncomplete(
            'The require POST is not yet implemented.'
        );
        $loader->getResource($this->testResource);

        /** @var Request $request */
        $request = $this->container[0]['request'];

        $this->assertEquals(
            [$this->testResource->getResource()->versionMimeType()],
            $request->getHeader('Content-Type')
        );
    }

    public function testErrorResponseThrowsException(): void
    {
        $data = [
            'errors' => [
                [
                    'code' => 'Global.SchemaValidation.Error',
                    'message' => 'A mapping rule does not exist for SECTIONDETAIL.STATUS.V4',
                    'description' => 'Errors parsing input JSON.'
                ]
            ]
        ];

        $headers = [
            'date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
            'X-Request-ID' => 'cefb16dc-9397-4b1d-968d-d972474fea04',
            'X-Status-Reason' => 'Validation failed',
            'X-hedtech-message' => 'A mapping rule does not exist for SECTIONDETAIL.STATUS.V4'
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(400, $headers, json_encode($data)),
        ]);

        $loader = $this->newResourceLoader($client);

        $this->expectException(ResourceBadRequestException::class);

        $loader->getResource($this->testResource);

    }

    public function testUnknownErrorResponseThrowsException(): void
    {
        $data = '<p>Warning: something broke</p>';

        $headers = [
            'date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(500, $headers, $data),
        ]);

        $loader = $this->newResourceLoader($client);

        $this->expectException(ServerException::class);

        $loader->getResource($this->testResource);

    }

    public function testCanGetApiPackageByName(): void
    {
        $data = '[
            {
                "name": "academic-catalogs",
                "representations": [
                    {
                        "X-Media-Type": "application/vnd.hedtech.integration.v6+json",
                        "methods": [
                            "get"
                        ]
                    },
                    {
                        "X-Media-Type": "application/json",
                        "methods": [
                            "get"
                        ]
                    }
                ]
            },
            {
                "name": "academic-credentials",
                "representations": [
                    {
                        "X-Media-Type": "application/vnd.hedtech.integration.v6+json",
                        "methods": [
                            "get",
                            "post",
                            "put"
                        ]
                    },
                    {
                        "X-Media-Type": "application/json",
                        "methods": [
                            "get",
                            "post",
                            "put"
                        ]
                    }
                ]
            }
        ]';

        $headers = [
            'Date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
            'X-Max-Page-Size' => 2,
            'X-Media-Type' => 'application/json',
        ];

        $client = $this->newHttpClientWithResponses([
            new Response(200, $headers, $data),
        ]);

        $loader = $this->newResourceLoader($client);

        $this->assertInstanceOf(ApiPackage::class, $loader->getApiPackage(ApiPackage::STUDENT));

    }

    private function newResourceLoader(Client $client): ResourceLoaderRest
    {
        /** @var Location|PHPUnit_Framework_MockObject_MockObject $location */
        $location = $this->createMock(Location::class);
        $location->method('addAuthenticationHeaders')
            ->will($this->returnArgument(0));
        $location->method('getAbsoluteLocation')
            ->willReturn('https://example.com');

        return new ResourceLoaderRest($location, $client);
    }

    private function newHttpClientWithResponses(array $responses): Client
    {
        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }

    public function getMockedNewModel(): BaseModel
    {
        $this->model = [
            $this->getMockForAbstractClass(BaseModel::class),
            $this->getMockForAbstractClass(BaseModel::class)
        ];
        return $this->model[$this->index++];
    }
}
