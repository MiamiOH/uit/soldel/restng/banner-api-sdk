<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 3:46 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use Carbon\Carbon;
use MiamiOH\BannerApi\Api;
use MiamiOH\BannerApi\Collection\BaseCollectionIterator;
use MiamiOH\BannerApi\Exception\AlreadyInitializedException;
use MiamiOH\BannerApi\ModelResourceOptions;
use MiamiOH\BannerApi\Resource\BaseModel;
use PHPUnit\Framework\TestCase;

class BaseModelTest extends TestCase
{

    /**
     * @var BaseModel
     */
    private $model;

    /**
     * @var ModelResourceOptions
     */
    private $options;

    public function setUp(): void
    {
        $this->model = $this->getMockForAbstractClass(BaseModel::class);
        $this->options = new ModelResourceOptions([
            'date' => Carbon::createFromDate(2012, 1, 1),
            'mediaType' => 'json',
            'requestId' => 'abc123',
            'message' => 'test resource',
        ]);
    }

    public function testCanModelBeInitialized(): void
    {
        $data = ['id' => '9f091a31-b7e5-43b0-8391-8079b2fa4f96'];
        $this->model->initialize($data, $this->options);
        $this->assertEquals($data['id'],$this->model->getId());

    }
    public function testCanGetRequestDate(): void
    {
        $this->model->initialize([], $this->options);
        $this->assertEquals(Carbon::createFromDate(2012, 1, 1), $this->model->getRequestDate());
    }

    public function testCanOnlyBeInitialziedOnce(): void
    {
        $this->model->initialize([], $this->options);

        $this->expectException(AlreadyInitializedException::class);
        $this->model->initialize([], $this->options);
    }
    public function testCanGetRequestMediaType(): void
    {
        $this->model->initialize([], $this->options);
        $this->assertEquals('json', $this->model->getRequestMediaType());
    }

    public function testCanGetRequestId(): void
    {
        $this->model->initialize([], $this->options);
        $this->assertEquals('abc123', $this->model->getRequestId());
    }

    public function testCanGetRequestMessage(): void
    {
        $this->model->initialize([], $this->options);
        $this->assertEquals('test resource', $this->model->getRequestMessage());
    }

    public function testCanBeCreatedWithNilGuid(): void
    {
        $this->assertEquals(Api::NIL_GUID, $this->model->getId());
    }

    public function testCanBeCollectedAsArray(): void
    {
        $data = ['id' => '0d97179e-488e-4717-9510-6672f2a625f7'];
        $this->model->initialize($data, $this->options);
        $this->assertEquals($data, $this->model->asEthosDataModelArray());
    }

    public function testCanBeConvertedtoJson(): void
    {
        $data = ['id' => '0d97179e-488e-4717-9510-6672f2a625f7'];
        $this->model->initialize($data, $this->options);
        $this->assertEquals(json_encode($data), json_encode($this->model));
    }
}
