<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 9:16 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\BaseCollectionFinder;
use MiamiOH\BannerApi\Resource\BaseCollectionPager;
use MiamiOH\BannerApi\Resource\CollectionFilterInterface;
use MiamiOH\BannerApi\Resource\Resource;
use MiamiOH\BannerApi\Resource\ResourceInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;
use PHPUnit\Framework\TestCase;

class BaseCollectionFinderTest extends TestCase
{

    /**
     * @var BaseCollectionFinder
     */
    private $finder;

    /**
     * @var CollectionFilterInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $filter;
    
    /** @var  ResourceInterface */
    private $resource;

    public function setUp()
    {
//        $resource = BannerResource::fromString('api-test-ok');
        $loader = $this->getMockBuilder(ResourceLoaderInterface::class)
            ->setMethods(['getCollectionFilter', 'getResourceById', 'getResource', 'getApiPackage'])
            ->getMock();

        $bannerResource = BannerResource::fromString("api-test-ok");
        
        $this->resource = $this->getMockBuilder(Resource::class)
                ->setMethods(['getLoader', 'getResource'])
                ->disableOriginalConstructor()
                ->getMockForAbstractClass();
        
        $this->resource->method('getLoader')->willReturn($loader);
        $this->resource->method('getResource')->willReturn($bannerResource);
        
        $this->filter = $this->getMockBuilder(CollectionFilterInterface::class)
            ->setMethods(['addSort', 'build', 'count','addCriteria', 'addParam'])
            ->getMock();

        $loader->method('getCollectionFilter')->willReturn($this->filter);

        $this->finder = $this->getMockBuilder(BaseCollectionFinder::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $reflectedClass = new \ReflectionClass(BaseCollectionFinder::class);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($this->finder, $this->resource);
    }

    public function testCanBeCreatedWithFinder(): void
    {
        $this->assertInstanceOf(BaseCollectionFinder::class, $this->finder);
    }

    public function testCanSetQueryOnFilter(): void
    {
        $attribute = 'title';
        $value = 'Some Title';

        $this->filter
            ->expects($this->once())
            ->method('addCriteria')
            ->with($this->equalTo('title'), $this->equalTo($value));

        $this->finder->withQuery($attribute, $value);
    }

    public function testCanSetSortOnFilter(): void
    {
        $sort = 'title';
        $direction = 'asc';

        $this->filter
            ->expects($this->once())
            ->method('addSort')
            ->with($this->equalTo('title'), $this->equalTo($direction));

        $this->finder->withSort($sort, $direction);
    }

    public function testCanSetOffsetOnFilter(): void
    {
        $this->filter
            ->expects($this->once())
            ->method('addParam')
            ->with($this->equalTo('offset'), $this->equalTo(10));

        $this->finder->withOffset(10);
    }

    public function testCanGetOffset(): void
    {
        $this->finder->withOffset(10);
        $this->assertEquals(10, $this->finder->getOffset());
    }

    public function testCanSetLimitOnFilter(): void
    {
        $this->filter
            ->expects($this->once())
            ->method('addParam')
            ->with($this->equalTo('limit'), $this->equalTo(100));

        $this->finder->withLimit(100);
    }

    public function testCanGetLimit(): void
    {
        $this->finder->withLimit(10);
        $this->assertEquals(10, $this->finder->getLimit());
    }

}
