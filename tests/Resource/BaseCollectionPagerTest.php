<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 8:30 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\PagedFindNotExecutedException;
use MiamiOH\BannerApi\Resource\BaseCollection;
use MiamiOH\BannerApi\Resource\BaseCollectionFinder;
use MiamiOH\BannerApi\Resource\BaseCollectionPager;
use MiamiOH\BannerApi\Resource\CollectionFilterInterface;
use MiamiOH\BannerApi\Resource\CollectionInterface;
use MiamiOH\BannerApi\Resource\CollectionPagerInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use PHPUnit\Framework\TestCase;

class BaseCollectionPagerTest extends TestCase
{

    /**
     * @var BaseCollectionPager
     */
    private $pager;

    public function setUp(): void
    {
        $this->pager = $this->getMockBuilder(BaseCollectionPager::class)
            ->getMockForAbstractClass();
    }

    public function testCanBeCreatedWithFinder(): void
    {
        $this->assertInstanceOf(BaseCollectionPager::class, $this->pager);
    }

    public function testInitialOffsetIsZero(): void
    {
        $this->assertEquals(0, $this->pager->getOffset());
    }

    public function testInitialLimitIsTen(): void
    {
        $this->assertEquals(10, $this->pager->getLimit());
    }

    public function testCanNotGetCountBeforeExecuting(): void
    {
        $this->expectException(PagedFindNotExecutedException::class);
        $this->pager->getTotalCount();
    }

}
