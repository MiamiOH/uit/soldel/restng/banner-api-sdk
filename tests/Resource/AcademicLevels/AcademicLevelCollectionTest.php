<?php

namespace MiamiOH\BannerApi\Tests\Resource\AcademicLevels;

use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollection;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelModel;
use PHPUnit\Framework\TestCase;
use MiamiOH\BannerApi\CollectionResourceOptions;


class AcademicLevelCollectionTest extends TestCase
{
    /**
     * @var AcademicLevelCollection
     */
    private $collection;

    /**
     * @var CollectionResourceOptions
     */
    private $options;

    /**
     * @var int
     */
    private $version;

    public function setUp(): void
    {
        $this->collection = new AcademicLevelCollection();
        $this->options = new CollectionResourceOptions([]);
        $this->version = 6;
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(AcademicLevelCollection::class, $this->collection);
    }

    public function testCanBeInitializedWithModels(): void
    {
        $models = [new AcademicLevelModel()];

        $this->collection->initialize($models, $this->options);

        $this->assertCount(1, $this->collection);
    }

    public function testCanBeIteratedAsArray(): void
    {
        $this->collection->initialize([
            new AcademicLevelModel(),
            new AcademicLevelModel(),
        ], $this->options);

        foreach ($this->collection as $model) {
            $this->assertInstanceOf(AcademicLevelModel::class, $model);
        }
    }


}