<?php

namespace MiamiOH\BannerApi\Tests\Resource\AcademicLevels;


use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevel;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelModelFinder;
use PHPUnit\Framework\TestCase;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollectionFinder;


class AcademicLevelTest extends TestCase
{
    /**
     * @var AcademicLevel
     */
    private $resource;

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();
        $location = ApiFactory::getNullLocation('https://example.com', $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);

        $this->resource = new AcademicLevel($loader);
    }

    public function testResourceHasCorrectName(): void
    {
        $this->assertEquals('academic-levels', $this->resource->getName());
    }

    public function testResourceHasCorrectSource(): void
    {
        $this->assertEquals('student', $this->resource->getSource());
    }

    public function testCanGetCourseModelFinder(): void
    {
        $this->assertInstanceOf(AcademicLevelModelFinder::class, $this->resource->findOne());
    }

    public function testCanGetCourseCollectionFinder(): void
    {
        $this->assertInstanceOf(AcademicLevelCollectionFinder::class, $this->resource->find());
    }

}
