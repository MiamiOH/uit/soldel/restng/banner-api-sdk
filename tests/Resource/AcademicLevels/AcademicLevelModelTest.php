<?php


namespace MiamiOH\BannerApi\Tests\Resource\AcademicLevels;


use MiamiOH\BannerApi\ModelResourceOptions;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelModel;
use PHPUnit\Framework\TestCase;

class AcademicLevelModelTest extends TestCase
{
    /**
     * @var AcademicLevelModel
     */
    private $academicLevels;

    /**
     * @var ModelResourceOptions
     */
    private $options;

    private $testData = [];

    public function setUp(): void
    {
        $this->academicLevels = new AcademicLevelModel();
        $this->options = new ModelResourceOptions([]);
        $this->testData = [
            'id' => '1074c948-4799-48b7-ac1b-8658d881d3f3',
            'title' => 'Graduate',
            'code' => 'GR',
            ];
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(AcademicLevelModel::class, $this->academicLevels);
    }

    public function testCanBeCreatedAsNewModel(): void
    {
        unset($this->testData['id']);

        $this->assertInstanceOf(AcademicLevelModel::class, new AcademicLevelModel());
    }

    public function testCanBeInitializedWithData(): void
    {
        $this->academicLevels->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['id'], $this->academicLevels->getId());
    }

    public function testCanGetTitleFromModel(): void
    {
        $this->academicLevels->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['title'], $this->academicLevels->getTitle());
    }

    public function testCanGetCodeFromModel(): void
    {
        $this->academicLevels->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['code'], $this->academicLevels->getCode());
    }

    public function testCanBeCollectedAsArray(): void
    {
        $this->academicLevels->initialize($this->testData, $this->options);
        $this->assertEquals($this->testData, $this->academicLevels->asEthosDataModelArray());
    }

    public function testCanBeConvertedtoJson(): void
    {
        $this->academicLevels->initialize($this->testData, $this->options);
        $this->assertEquals($this->testData, json_decode(json_encode($this->academicLevels), true));
    }

}