<?php


namespace MiamiOH\BannerApi\Tests\Resource\AcademicLevels;

use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollectionPager;
use MiamiOH\BannerApi\Resource\ResourceInterface;
use MiamiOH\BannerApi\Tests\TestClassBase;
use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollection;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollectionFinder;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevel;


class AcademicLevelCollectionFinderTest extends TestClassBase
{
    /**
     * @var Location
     */
    private $location;

    /** @var array  */
    private $testData = [];
    
    /** @var  ResourceInterface */
    private $resource;

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();

        $this->location = ApiFactory::getNullLocation('https://example.com', $auth);

        $this->testData = [
            [
                'id' => '1074c948-4799-48b7-ac1b-8658d881d3f3',
                'title' => 'Graduate',
                'code' => 'GR',
            ]
        ];
    }

    public function testCanBeCreatedWithLocation(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $this->resource = new AcademicLevel($loader);
        $finder = new AcademicLevelCollectionFinder($this->resource);

        $this->assertInstanceOf(AcademicLevelCollectionFinder::class, $finder);
    }

    public function testCanGetAcademicLevelCollection(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        $this->resource = new AcademicLevel($loader);

        $finder = new AcademicLevelCollectionFinder($this->resource);

        $collection = $finder->execute();

        $this->assertInstanceOf(AcademicLevelCollection::class, $collection);
    }

    public function testCanAcademicLevelCollectionUseAdHocQueryFilter()
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        $this->resource = new AcademicLevel($loader);

        $finder = new AcademicLevelCollectionFinder($this->resource);
        
        $collection = $finder->withQuery('title', 'Graduate')->execute();
        
        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'title=');
    }

    public function testCanAcademicLevelCollectionGetPager(): void
    {
        $loader = $this->newLoaderWithResponses([]);
        $this->resource = new AcademicLevel($loader);
        $finder = new AcademicLevelCollectionFinder($this->resource);

        $pager = $finder->withLimit(30)->withOffset(0)->pager();
        $this->assertInstanceOf(AcademicLevelCollectionPager::class, $pager);
    }

    private function newLoaderWithResponses(array $responses): ResourceLoaderRest
    {
        $client = $this->newHttpClientWithResponses($responses);
        return new ResourceLoaderRest($this->location, $client);

    }
}