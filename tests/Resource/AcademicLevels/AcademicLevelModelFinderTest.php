<?php


namespace MiamiOH\BannerApi\Tests\Resource\AcademicLevels;

use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevel;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelModel;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelModelFinder;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Tests\TestClassBase;


class AcademicLevelModelFinderTest extends TestClassBase
{
    /**
     * @var BannerResource
     */
    private $resource;
    /**
     * @var Location
     */
    private $location;

    /** @var array  */
    private $testData = [];

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();

        $this->location = ApiFactory::getNullLocation('https://example.com', $auth);
        $this->resource = BannerResource::fromString('api-test-ok');

        $this->testData = [
            'id' => '1074c948-4799-48b7-ac1b-8658d881d3f3',
            'title' => 'Graduate',
            'code' => 'GR',
        ];
    }

    public function testCanBeCreatedWithLocation(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $restInterfaceResource = new AcademicLevel($loader);
        $finder = new AcademicLevelModelFinder($restInterfaceResource);

        $this->assertInstanceOf(AcademicLevelModelFinder::class, $finder);
    }

    public function testCanNotExecuteWithoutUniqueIdentifier(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $restInterfaceResource = new AcademicLevel($loader);
        $finder = new AcademicLevelModelFinder($restInterfaceResource);

        $this->expectException(MissingUniqueIdentifierException::class);
        $finder->execute();
    }

    public function testReturnsExpectedModel(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        $restInterfaceResource = new AcademicLevel($loader);

        $finder = new AcademicLevelModelFinder($restInterfaceResource);
        $id = Guid::fromString($this->testData['id']);

        $this->assertInstanceOf(AcademicLevelModel::class, $finder->withId($id)->execute());
    }

    public function testCanFindWithGuid(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $restInterfaceResource = new AcademicLevel($loader);

        $finder = new AcademicLevelModelFinder($restInterfaceResource);

        $id = Guid::fromString($this->testData['id']);
        $academicLevel = $finder->withId($id)->execute();

        $this->assertEquals($this->testData['id'], $academicLevel->getId());
    }

    private function newLoaderWithResponses(array $responses): ResourceLoaderRest
    {
        $client = $this->newHttpClientWithResponses($responses);
        return new ResourceLoaderRest($this->location, $client);

    }
    
    
}