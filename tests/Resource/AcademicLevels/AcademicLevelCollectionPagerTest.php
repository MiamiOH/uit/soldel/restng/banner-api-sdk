<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 3:42 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\AcademicLevels;

use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollection;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollectionFinder;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevelCollectionPager;
use MiamiOH\BannerApi\Tests\Resource\TestCollectionPagerBase;

class AcademicLevelCollectionPagerTest extends TestCollectionPagerBase
{

    /**
     * @var AcademicLevelCollectionFinder|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $finder;

    public function setUp(): void
    {
        $this->finderClass = AcademicLevelCollectionFinder::class;
        $this->collectionClass = AcademicLevelCollection::class;
        $this->pagerClass = AcademicLevelCollectionPager::class;
    }
}
