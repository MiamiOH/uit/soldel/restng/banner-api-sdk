<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 9:15 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\Course\CourseOwningInstitutionUnit;
use PHPUnit\Framework\TestCase;

class CourseOwningInstitutionUnitTest extends TestCase
{
    public function testCanBeCreatedWithGuid(): void
    {
        $this->assertInstanceOf(
            CourseOwningInstitutionUnit::class,
            new CourseOwningInstitutionUnit(new Guid('61820135-a36b-4308-99f7-28697ab7a0b1'), 50)
        );
    }

    public function testCanBeCollectedAsArray(): void
    {
        $unit = new CourseOwningInstitutionUnit(new Guid('61820135-a36b-4308-99f7-28697ab7a0b1'), 50);
        $data = $unit->asEthosDataModelArray();
        $this->assertEquals('61820135-a36b-4308-99f7-28697ab7a0b1', $data['institutionUnit']['id']);
        $this->assertEquals(50, $data['ownershipPercentage']);
    }

}
