<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 12:17 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\Course\CourseCredit;
use MiamiOH\BannerApi\Resource\Course\CourseCreditCollection;
use PHPUnit\Framework\TestCase;

class CourseCreditCollectionTest extends TestCase
{
    /**
     * @var CourseCreditCollection
     */
    private $collection;

    public function setUp(): void
    {
        $credits = [
            new CourseCredit(
                new Guid('bd582974-7421-4502-8fd4-b3966e08923e'),
                'institution',
                'credit',
                1,
                4,
                1
            ),
        ];

        $this->collection = new CourseCreditCollection($credits);
    }

    public function testCanBeCreatedFromArray(): void
    {
        $this->assertInstanceOf(CourseCreditCollection::class, $this->collection);
    }

    public function testCanNotBeCreatedWithInvalidObjects(): void
    {
        $units = [
            new \stdClass()
        ];

        $this->expectException(\InvalidArgumentException::class);
        new CourseCreditCollection($units);
    }

    public function testCanBeCounted(): void
    {
        $this->assertCount(1, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        foreach ($this->collection->toArray() as $unit) {
            $this->assertInstanceOf(CourseCredit::class, $unit);
        }
    }

    public function testCanBeIteratedAsArray(): void
    {
        foreach ($this->collection as $unit) {
            $this->assertInstanceOf(CourseCredit::class, $unit);
        }
    }

    public function testCanBeIteratedAsAssoc(): void
    {
        foreach ($this->collection as $key => $unit) {
            $this->assertInstanceOf(CourseCredit::class, $unit);
        }
    }

    public function testCanBeCollectedAsArray(): void
    {
        $this->assertTrue(is_array($this->collection->asEthosDataModelArray()));
    }

}
