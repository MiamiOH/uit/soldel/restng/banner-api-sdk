<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 12:57 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Resource\Course\CourseReportingDetailHours;
use PHPUnit\Framework\TestCase;

class CourseReportingDetailTypeTest extends TestCase
{
    public function testCanBeCreatedWithValues(): void
    {
        $this->assertInstanceOf(
            CourseReportingDetailHours::class,
            new CourseReportingDetailHours(2, 6, 1)
        );
    }

    public function testCanBeCollectedAsArray(): void
    {
        $detail = new CourseReportingDetailHours(2, 6, 1);
        $data = $detail->asEthosDataModelArray();
        $this->assertEquals(2, $data['minimum']);
        $this->assertEquals(6, $data['maximum']);
        $this->assertEquals(1, $data['increment']);
    }

}
