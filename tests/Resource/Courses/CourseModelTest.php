<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 3:46 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use Carbon\Carbon;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\GuidCollection;
use MiamiOH\BannerApi\ModelResourceOptions;
use MiamiOH\BannerApi\Resource\Course\CourseBilling;
use MiamiOH\BannerApi\Resource\Course\CourseCreditCollection;
use MiamiOH\BannerApi\Resource\Course\CourseModel;
use MiamiOH\BannerApi\Resource\Course\CourseOwningInstitutionUnitCollection;
use MiamiOH\BannerApi\Resource\Course\CourseReportingDetail;
use PHPUnit\Framework\TestCase;

class CourseModelTest extends TestCase
{

    /**
     * @var CourseModel
     */
    private $course;

    /**
     * @var ModelResourceOptions
     */
    private $options;

    private $testData = [];

    public function setUp(): void
    {
        $this->course = new CourseModel();
        $this->options = new ModelResourceOptions([]);
        $this->testData = [
            'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
            'title' => 'My Courses',
            'description' => 'This is a great course.',
            'subject' => ['id' => '28530025-9551-4d23-af78-51d94fd12cee'],
            'topic' => [
                'id' => '8599a236-958d-4710-97da-cafa9efd3b7e'
            ],
            'categories' => [
                ['id' => 'be6ceea4-c2f8-4b41-a784-c8d7df8067ce'],
                ['id' => '529493fa-c8f4-422f-a45e-4d5b26dd4c1b'],
            ],
            'courseLevels' => [
                [
                    'id' => 'e33898de-6302-4756-8f0c-5f6c5218e02e'
                ]
            ],
            'instructionalMethods' => [
                [
                    'id' => '3a768eea-cbda-4926-a82d-831cb89092aa'
                ]
            ],
            'owningInstitutionUnits' => [
                [
                    'institutionUnit' => [
                        'id' => '61820135-a36b-4308-99f7-28697ab7a0b1'
                    ],
                    'ownershipPercentage' => '50'
                ],
                [
                    'institutionUnit' => [
                        'id' => '00cd9680-9090-4b38-92ce-7b72d50421ba'
                    ],
                    'ownershipPercentage' => '50'
                ]
            ],
            'schedulingStartOn' => '2014-10-16T00:00:00+00:00',
            'schedulingEndOn' => '2014-11-30T23:59:59+00:00',
            'number' => '101',
            'academicLevels' => [
                [
                    'id' => '11cdd494-de5b-460e-9ddf-4e84bad6f596'
                ]
            ],
            'gradeSchemes' => [
                [
                    'id' => 'f14a648d-c757-4c7a-b283-1fa74e561d63'
                ]
            ],
            'credits' => [
                [
                    'creditCategory' => [
                        'creditType' => 'institution',
                        'detail' => [
                            'id' => 'bd582974-7421-4502-8fd4-b3966e08923e'
                        ]
                    ],
                    'measure' => 'credit',
                    'minimum' => '1',
                    'maximum' => '4',
                    'increment' => '1'
                ]
            ]
        ];
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(CourseModel::class, $this->course);
    }

    public function testCanBeCreatedAsNewModel(): void
    {
        unset($this->testData['id']);

        $this->assertInstanceOf(CourseModel::class, new CourseModel());
    }

    public function testCanBaseModelBeCreated(): void
    {
         $test = new CourseModel($this->testData);

        $this->assertEquals($this->testData['id'], $test->getId()); 
    }

    public function testCanBeInitializedWithData(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['id'], $this->course->getId());
    }

    public function testCanGetTitleFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['title'], $this->course->getTitle());
    }

    public function testCanGetDescriptionFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['description'], $this->course->getDescription());
    }

    public function testCanGetSubjectFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['subject']['id'], $this->course->getSubject());
    }

    public function testCanGetTopicFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['topic']['id'], $this->course->getTopic());
    }

    public function testCanGetCatetoriesFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(GuidCollection::class, $this->course->getCategories());
    }

    public function testCanGetCourseLevelsFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(GuidCollection::class, $this->course->getCourseLevels());
    }

    public function testCanGetInstructionalMethodsFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(GuidCollection::class, $this->course->getInstructionalMethods());
    }

    public function testCanGetOwningInstitutionUnitsFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);
        $units = $this->course->getOwningInstitutionUnits();

        $this->assertInstanceOf(
            CourseOwningInstitutionUnitCollection::class,
            $units
        );

        $this->assertCount(count($this->testData['owningInstitutionUnits']), $units);
    }

    public function testCanGetSchedulingStartOnFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(
            Carbon::class,
            $this->course->getSchedulingStartOn()
        );
    }

    public function testCanGetSchedulingEndOnFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(
            Carbon::class,
            $this->course->getSchedulingEndOn()
        );
    }

    public function testCanGetNumberFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['number'], $this->course->getNumber());
    }

    public function testCanGetAcademicLevelsFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(GuidCollection::class, $this->course->getAcademicLevels());
    }

    public function testCanGetGradeSchemesFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);

        $this->assertInstanceOf(GuidCollection::class, $this->course->getGradeSchemes());
    }

    public function testCanGetCreditsFromModel(): void
    {
        $this->course->initialize($this->testData, $this->options);
        $credits = $this->course->getCredits();

        $this->assertInstanceOf(
            CourseCreditCollection::class,
            $credits
        );

        $this->assertCount(count($this->testData['credits']), $credits);
    }

    public function testCanBeCollectedAsArrayCourse(): void
    {
        $this->course->initialize($this->testData, $this->options);
        $this->assertEquals($this->testData, $this->course->asEthosDataModelArray());
    }

    public function testCanBeConvertedtoJson(): void
    {
        $this->course->initialize($this->testData, $this->options);
        $this->assertEquals($this->testData, json_decode(json_encode($this->course), true));
    }

}
