<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 9:15 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\Course\CourseOwningInstitutionUnit;
use MiamiOH\BannerApi\Resource\Course\CourseOwningInstitutionUnitCollection;
use PHPUnit\Framework\TestCase;
use function Sodium\crypto_box_publickey_from_secretkey;

class CourseOwningInstitutionUnitCollectionTest extends TestCase
{

    /**
     * @var CourseOwningInstitutionUnitCollection
     */
    private $collection;

    public function setUp(): void
    {
        $units = [
            new CourseOwningInstitutionUnit(new Guid('61820135-a36b-4308-99f7-28697ab7a0b1'), 50)
        ];

        $this->collection = new CourseOwningInstitutionUnitCollection($units);
    }

    public function testCanBeCreatedFromArray(): void
    {
        $this->assertInstanceOf(
            CourseOwningInstitutionUnitCollection::class,
            $this->collection
        );
    }

    public function testCanNotBeCreatedWithInvalidObjects()
    {
        $units = [
            new \stdClass()
        ];

        $this->expectException(\InvalidArgumentException::class);
        new CourseOwningInstitutionUnitCollection($units);
    }

    public function testCanBeCounted(): void
    {
        $this->assertCount(1, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        foreach ($this->collection->toArray() as $unit) {
            $this->assertInstanceOf(CourseOwningInstitutionUnit::class, $unit);
        }
    }

    public function testCanBeIteratedAsArray(): void
    {
        foreach ($this->collection as $unit) {
            $this->assertInstanceOf(CourseOwningInstitutionUnit::class, $unit);
        }
    }

    public function testCanBeIteratedAsAssoc(): void
    {
        foreach ($this->collection as $key => $unit) {
            $this->assertInstanceOf(CourseOwningInstitutionUnit::class, $unit);
        }
    }

    public function testCanBeCollectedAsArray(): void
    {
        $this->assertTrue(is_array($this->collection->asEthosDataModelArray()));
    }

}
