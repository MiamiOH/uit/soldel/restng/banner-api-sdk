<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 11:46 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\Course\CourseCredit;
use PHPUnit\Framework\TestCase;

class CourseCreditTest extends TestCase
{
    public function testCanBeCreatedWithValues()
    {
        $this->assertInstanceOf(
            CourseCredit::class,
            new CourseCredit(
                new Guid('bd582974-7421-4502-8fd4-b3966e08923e'),
                'institution',
                'credit',
                1,
                4,
                1
            )
        );
    }

    public function testCanBeCollectedAsArray(): void
    {
        $credit = new CourseCredit(
            new Guid('bd582974-7421-4502-8fd4-b3966e08923e'),
            'institution',
            'credit',
            1,
            4,
            1
        );

        $data = $credit->asEthosDataModelArray();
        $this->assertEquals('bd582974-7421-4502-8fd4-b3966e08923e', $data['creditCategory']['detail']['id']);
        $this->assertEquals('institution', $data['creditCategory']['creditType']);
        $this->assertEquals('credit', $data['measure']);
        $this->assertEquals(1, $data['minimum']);
        $this->assertEquals(4, $data['maximum']);
        $this->assertEquals(1, $data['increment']);
    }

}
