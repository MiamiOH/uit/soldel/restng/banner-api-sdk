<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 12:41 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Resource\Course\Course;
use MiamiOH\BannerApi\Resource\Course\CourseBilling;
use PHPUnit\Framework\TestCase;

class CourseBillingTest extends TestCase
{
    public function testCanBeCreatedWithValues(): void
    {
        $this->assertInstanceOf(
            CourseBilling::class,
            new CourseBilling(1, 5, 1)
        );
    }

    public function testCanBeCollectedAsArray(): void
    {
        $billing = new CourseBilling(1, 5, 1);

        $data = $billing->asEthosDataModelArray();
        $this->assertEquals(1, $data['minimum']);
        $this->assertEquals(5, $data['maximum']);
        $this->assertEquals(1, $data['increment']);
    }

}
