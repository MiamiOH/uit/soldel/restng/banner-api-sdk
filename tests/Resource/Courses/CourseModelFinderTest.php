<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 11:00 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException;
use MiamiOH\BannerApi\Exception\ResourceModelNotFoundException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Resource\Course\CourseModel;
use MiamiOH\BannerApi\Resource\Course\CourseModelFinder;
use MiamiOH\BannerApi\Resource\ResourceInterface;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Tests\TestClassBase;
use MiamiOH\BannerApi\Resource\Course\Course;

class CourseModelFinderTest extends TestClassBase
{
    /**
     * @var ResourceInterface
     */
    private $resource;
    /**
     * @var Location
     */
    private $location;

    private $testData = [];

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();

        $this->location = ApiFactory::getNullLocation('https://example.com', $auth);
        //$this->resource = BannerResource::fromString('api-test-ok');
        
        $this->testData = [
            'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
            'title' => 'My Courses',
            'subject' => ['id' => '28530025-9551-4d23-af78-51d94fd12cee'],
            'schedulingStartOn' => '2014-10-16T00:00:00+00:00',
            'number' => '101',
        ];
    }

    public function testCanBeCreatedWithLocation(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $resourceInterface = new Course($loader);
        $finder = new CourseModelFinder($resourceInterface);

        $this->assertInstanceOf(CourseModelFinder::class, $finder);
    }

    public function testCanNotExecuteWithoutUniqueIdentifier(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $resourceInterface = new Course($loader);
        $finder = new CourseModelFinder($resourceInterface);

        $this->expectException(MissingUniqueIdentifierException::class);
        $finder->execute();
    }

    public function testReturnsExpectedModel(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        $resourceInterface = new Course($loader);

        $finder = new CourseModelFinder($resourceInterface);
        $id = Guid::fromString($this->testData['id']);

        $this->assertInstanceOf(CourseModel::class, $finder->withId($id)->execute());
    }

    public function testCanFindWithGuid(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $restInterfaceResource = new Course($loader);

        $finder = new CourseModelFinder($restInterfaceResource);

        $id = Guid::fromString($this->testData['id']);
        $course = $finder->withId($id)->execute();

        $this->assertEquals($this->testData['id'], $course->getId());
    }

    public function testGivesCorrectResponseWhenFindOneNotFound()
    {
        $responseData = [
            'errors' => [
                [
                    'code' => 'Global.SchemaValidation.Error',
                    'message' => 'Course not found',
                    'description' => 'Errors parsing input JSON.'
                ]
            ]
        ];

        $responseHeaders = [
            'date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
            'X-Request-ID' => 'cefb16dc-9397-4b1d-968d-d972474fea04',
            'X-hedtech-message' => 'Course not found'
        ];

        $loader = $this->newLoaderWithResponses([
            new Response(404, $responseHeaders, json_encode($responseData)),
        ]);

        $restInterfaceResource = new Course($loader);

        $finder = new CourseModelFinder($restInterfaceResource);
        $id = Guid::fromString($this->testData['id']);

        $this->expectException(ResourceModelNotFoundException::class);
        $finder->withId($id)->execute();
    }

    private function newLoaderWithResponses(array $responses): ResourceLoaderRest
    {
        $client = $this->newHttpClientWithResponses($responses);
        return new ResourceLoaderRest($this->location, $client);

    }
}
