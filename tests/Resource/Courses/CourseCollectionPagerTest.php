<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 3:40 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Resource\Course\CourseCollection;
use MiamiOH\BannerApi\Resource\Course\CourseCollectionFinder;
use MiamiOH\BannerApi\Resource\Course\CourseCollectionPager;
use MiamiOH\BannerApi\Tests\Resource\TestCollectionPagerBase;

class CourseCollectionPagerTest extends TestCollectionPagerBase
{

    /**
     * @var CourseCollectionFinder|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $finder;

    public function setUp(): void
    {
        $this->finderClass = CourseCollectionFinder::class;
        $this->collectionClass = CourseCollection::class;
        $this->pagerClass = CourseCollectionPager::class;
    }

}
