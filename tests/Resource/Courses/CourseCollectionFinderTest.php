<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/16/17
 * Time: 3:23 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\GuidCollection;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Resource\Course\Course;
use MiamiOH\BannerApi\Resource\Course\CourseCollection;
use MiamiOH\BannerApi\Resource\Course\CourseCollectionFinder;
use MiamiOH\BannerApi\Resource\Course\CourseCollectionPager;
use MiamiOH\BannerApi\Resource\Course\CourseCreditCollection;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Tests\TestClassBase;

class CourseCollectionFinderTest extends TestClassBase
{
    /**
     * @var BannerResource
     */
    private $resource;
    /**
     * @var Location
     */
    private $location;

    private $testData = [];
    
    private $version;

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();

        $this->location = ApiFactory::getNullLocation('https://example.com', $auth);
        //$this->resource = BannerResource::fromString('api-test-ok');

        $this->version = 6;    
        $this->testData = [
            [
                'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
                'title' => 'My Courses',
                'subject' => ['id' => '28530025-9551-4d23-af78-51d94fd12cee'],
                'schedulingStartOn' => '2014-10-16T00:00:00+00:00',
                'schedulingEndOn' => '2014-10-17T00:00:00+00:00',
                'number' => '101',
                'instructionalMethods' => [
                    [
                    'id'=>'33f5100c-c15b-40e7-897d-f455cdf9c918'
                    ],
                    [
                        'id'=>'33f5100c-c15b-40e7-897d-f455cdf9c918'
                    ]
                ],
                'academicLevels' => [
                    [
                        'id'=>'d4eadd35-0bd2-4907-a896-66169754354d'
                    ],
                    [
                        'id'=>'36ed7ee5-afa6-48be-bf52-5edb12bf6f61'
                    ]
                ],
                'owningInstitutionUnits' => [
                    [
                        'institutionUnit' => [
                                'id'=>'b5714270-c21b-4f94-adc9-214f352feabe',
                        ],
                        'ownershipPercentage' => '50'
                    ],
                    [
                        'institutionUnit' => [
                            'id'=>'1ab99cfb-e5f6-46fb-9b9e-3f601ced3ab4',
                        ],
                        'ownershipPercentage' => '50'
                    ]
                ],
                'topic' => [
                    'id' => 'ee7d346b-76f6-4431-a22d-74f330c62f22'
                ],
                'categories' => [
                    [
                        'id' => '9fdf5a93-01a9-4eba-8b8f-53499452c7b6'
                    ],
                    [
                        'id' => '477c628e-f101-4653-8d5f-6bae9c61afad'
                    ]
                ]
            ]
        ];
    }

    public function testCanCourseCollectionFinderBeCreatedWithLocation(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $this->resource = new Course($loader);
        $finder = new CourseCollectionFinder($this->resource);

        $this->assertInstanceOf(CourseCollectionFinder::class, $finder);
    }

    public function testCanCourseCollectionFinderGetCollection(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        
        $this->resource = new Course($loader);
        $finder = new CourseCollectionFinder($this->resource);

        $collection = $finder->execute();

        $this->assertInstanceOf(CourseCollection::class, $collection);
    }
    
    /*
     * Test for getting course collection using the filter and passing Number as the URL filter parameter
     */
    public function testCanGetCourseCollectionWithNumberFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $finder->withNumber('401')->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'number=401');
    }

    /*
     * Test for getting course collection using the filter and passing Title as the URL filter parameter
     */
    public function testCanGetCourseCollectionWithTitleFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $finder->withTitle('My Courses')->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'title=My+Courses');
    }

    /*
     * Test for getting course collection using the filter and passing SchedulingStartOn as the URL filter parameter
     */
    public function testCanGetCollectionWithScheduleStartOnFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);
        
        $scheduleStartOn = new Carbon('2014/10/16');

        $finder->withScheduleStartOn($scheduleStartOn)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'scheduleStartOn=2014-10-16T00%3A00%3A00-04%3A00');
    }

    /*
     * Test for getting course collection using the filter and passing SchedulingEndOn as the URL filter parameter
     */
    public function testCanGetCollectionWithScheduleEndOnFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);
        
        $scheduleEndOn = new Carbon('2014/10/16');

        $finder->withScheduleEndOn($scheduleEndOn)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'scheduleEndOn=2014-10-16T00%3A00%3A00-04%3A00');
    }

    /*
     * Test for getting course collection using the filter and passing Subject[id] as the URL filter parameter
     */
    public function testCanGetCollectionWithSubjectFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);
        
        $subjectId = new Guid('28530025-9551-4d23-af78-51d94fd12cee');

        $finder->withSubject($subjectId)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'subject=28530025-9551-4d23-af78-51d94fd12cee');
    }
    
    /*
     * Test for getting course collection using the filter and passing InstructionalMethods[id] as the URL filter parameter
     */
    public function testCanGetCollectionWithInstructionalMethodsFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200,[],json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $instructionalMethods = GuidCollection::fromArrayOfGuids([
            new Guid('33f5100c-c15b-40e7-897d-f455cdf9c918'),
            new Guid('d4eadd35-0bd2-4907-a896-66169754354d'),
        ]);
        
        $finder->withInstructionalMethods($instructionalMethods)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1,
            'instructionalMethods=33f5100c-c15b-40e7-897d-f455cdf9c918%2Cd4eadd35-0bd2-4907-a896-66169754354d');
    }
    
    /*
     * Test for getting course collection using the filter and passing AcademicLevel[id] as the URL filter parameter
     */
    public function testCanGetCollectionWithAcademicLevelsFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200,[],json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);
        
        $academicLevels = GuidCollection::fromArrayOfGuids([
            new Guid('d4eadd35-0bd2-4907-a896-66169754354d'),
            new Guid('33f5100c-c15b-40e7-897d-f455cdf9c918'),
            ]
        );
        
        $finder->withAcademicLevels($academicLevels)->execute();
        
        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1,
            'academicLevels=d4eadd35-0bd2-4907-a896-66169754354d%2C33f5100c-c15b-40e7-897d-f455cdf9c918');
    }
    
    /*
     * Test for getting course collection using the filter and passing owningInstitutionUnits{institutionUnit[id],ownershipPercentage) as the URL filter parameter 
     */
    public function testCanGetCollectionWithOwningInstitutionUnitsFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200,[],json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);
        
        $owningInstitutionUnits = GuidCollection::fromArrayOfGuids([
            new Guid('d4eadd35-0bd2-4907-a896-66169754354d'), 
            new Guid('33f5100c-c15b-40e7-897d-f455cdf9c918')
        ]);
        
        $finder->withOwningInstitutionUnits($owningInstitutionUnits)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1,
            'owningInstitutionUnits=d4eadd35-0bd2-4907-a896-66169754354d%2C33f5100c-c15b-40e7-897d-f455cdf9c918');
    }
    
    /*
     * Test for getting course collection using the filter and passing Topic[id] as the URL filter parameter
     */
    public function testCanGetCollectionWithTopicFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $topicId = new Guid('ee7d346b-76f6-4431-a22d-74f330c62f22');

        $finder->withTopic($topicId)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'topic=ee7d346b-76f6-4431-a22d-74f330c62f22');
    }
    
    /*
     * Test for getting course collection using the filter and passing Categories[id] as the URL filter parameter
     */
    public function testCanGetCollectionWithCategoriesFilter(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $categories = GuidCollection::fromArrayOfGuids([            
            new Guid('9fdf5a93-01a9-4eba-8b8f-53499452c7b6'),
            new Guid ('477c628e-f101-4653-8d5f-6bae9c61afad'),
        ]);

        $finder->withCategories($categories)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'categories=9fdf5a93-01a9-4eba-8b8f-53499452c7b6%2C477c628e-f101-4653-8d5f-6bae9c61afad');
    }

    // TODO move abstract base class tests out of course
    public function testCanCourseCollectionUseAdHocQueryFilter()
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $finder->withQuery('title', 'My Title')->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'title=');
    }

    public function testCanUseSortWithQuery()
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $finder->withSort('title' , 'asc')->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'sort=');
    }

    public function testCanGetCollectionWithOffset(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $finder->withOffset(10)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'offset=10');
    }

    public function testCanGetCourseCollectionWithLimit(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $finder->withLimit(30)->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'limit=30');
    }

    public function testCanCourseCollectionGetPager(): void
    {
        $loader = $this->newLoaderWithResponses([]);
        $this->resource = new Course($loader, $this->version);
        $finder = new CourseCollectionFinder($this->resource);

        $pager = $finder->withLimit(30)->withOffset(0)->pager();
        $this->assertInstanceOf(CourseCollectionPager::class, $pager);
    }

    private function newLoaderWithResponses(array $responses): ResourceLoaderRest
    {
        $client = $this->newHttpClientWithResponses($responses);
        return new ResourceLoaderRest($this->location, $client);

    }
}
