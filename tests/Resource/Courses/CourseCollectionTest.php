<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 12:28 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\CollectionResourceOptions;
use MiamiOH\BannerApi\Resource\Course\CourseCollection;
use MiamiOH\BannerApi\Resource\Course\CourseModel;
use PHPUnit\Framework\TestCase;

class CourseCollectionTest extends TestCase
{
    /**
     * @var CourseCollection
     */
    private $collection;

    /**
     * @var CollectionResourceOptions
     */
    private $options;

    public function setUp(): void
    {
        $this->collection = new CourseCollection();
        $this->options = new CollectionResourceOptions([]);
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(CourseCollection::class, $this->collection);
    }

    public function testCanBeInitializedWithModels(): void
    {
        $models = [new CourseModel()];

        $this->collection->initialize($models, $this->options);

        $this->assertCount(1, $this->collection);
    }

    public function testCanBeIteratedAsArray(): void
    {
        $this->collection->initialize([
            new CourseModel(),
            new CourseModel(),
        ], $this->options);

        foreach ($this->collection as $model) {
            $this->assertInstanceOf(CourseModel::class, $model);
        }
    }

}
