<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 10:19 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\Resource\Course\Course;
use MiamiOH\BannerApi\Resource\Course\CourseCollectionFinder;
use MiamiOH\BannerApi\Resource\Course\CourseModelFinder;
use PHPUnit\Framework\TestCase;

class CourseTest extends TestCase
{
    /**
     * @var Course
     */
    private $resource;

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();
        $location = ApiFactory::getNullLocation('https://example.com', $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);

        $this->resource = new Course($loader);
    }

    public function testResourceHasCorrectName(): void
    {
        $this->assertEquals('courses', $this->resource->getName());
    }

    public function testResourceHasCorrectSource(): void
    {
        $this->assertEquals('student', $this->resource->getSource());
    }

    public function testCanGetCourseModelFinder(): void
    {
        $this->assertInstanceOf(CourseModelFinder::class, $this->resource->findOne());
    }

    public function testCanGetCourseCollectionFinder(): void
    {
        $this->assertInstanceOf(CourseCollectionFinder::class, $this->resource->find());
    }
}
