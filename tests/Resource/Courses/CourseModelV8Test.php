<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 8/1/17
 * Time: 2:06 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Resource\Course\CourseBilling;
use MiamiOH\BannerApi\Resource\Course\CourseCredit;
use MiamiOH\BannerApi\Resource\Course\CourseCreditCollection;
use MiamiOH\BannerApi\Resource\Course\CourseReportingDetail;
use PHPUnit\Framework\TestCase;
use MiamiOH\BannerApi\Resource\Course\CourseModelV8;
use MiamiOH\BannerApi\Resource\Course\CourseModel;

class CourseModelV8Test extends TestCase
{
    /** @var  CourseModelV8 */
    private $courseModelV8;

    private $testData = [];

    public function setUp(): void
    {
        $this->courseModelV8 = new CourseModelV8();
        $this->testData = [
            'id' => '0d97179e-488e-4717-9510-6672f2a625f7',
            'title' => 'My Courses',
            'description' => 'This is a great course.',
            'subject' => ['id' => '28530025-9551-4d23-af78-51d94fd12cee'],
            'topic' => [
                'id' => '8599a236-958d-4710-97da-cafa9efd3b7e'
            ],
            'categories' => [
                ['id' => 'be6ceea4-c2f8-4b41-a784-c8d7df8067ce'],
                ['id' => '529493fa-c8f4-422f-a45e-4d5b26dd4c1b'],
            ],
            'courseLevels' => [
                [
                    'id' => 'e33898de-6302-4756-8f0c-5f6c5218e02e'
                ]
            ],
            'instructionalMethods' => [
                [
                    'id' => '3a768eea-cbda-4926-a82d-831cb89092aa'
                ]
            ],
            'owningInstitutionUnits' => [
                [
                    'institutionUnit' => [
                        'id' => '61820135-a36b-4308-99f7-28697ab7a0b1'
                    ],
                    'ownershipPercentage' => '50'
                ],
                [
                    'institutionUnit' => [
                        'id' => '00cd9680-9090-4b38-92ce-7b72d50421ba'
                    ],
                    'ownershipPercentage' => '50'
                ]
            ],
            'schedulingStartOn' => '2014-10-16T00:00:00+00:00',
            'schedulingEndOn' => '2014-11-30T23:59:59+00:00',
            'number' => '101',
            'academicLevels' => [
                [
                    'id' => '11cdd494-de5b-460e-9ddf-4e84bad6f596'
                ]
            ],
            'gradeSchemes' => [
                [
                    'id' => 'f14a648d-c757-4c7a-b283-1fa74e561d63'
                ]
            ],
            'credits' => [
                [
                    'creditCategory' => [
                        'creditType' => 'institution',
                        'detail' => [
                            'id' => 'bd582974-7421-4502-8fd4-b3966e08923e'
                        ]
                    ],
                    'measure' => 'credit',
                    'minimum' => '1',
                    'maximum' => '4',
                    'increment' => '1'
                ]
            ],
            'billing' => [
                'minimum' => '1',
                'maximum' => '5',
                'increment' => '1'
            ],
            'reportingDetail' => [
                'type' => 'Nebraska',
                'lab' => [
                    'minimum' => '2',
                    'maximum' => '6',
                    'increment' => '1'
                ],
                'lecture' => [
                    'minimum' => '1',
                    'maximum' => '7',
                    'increment' => '2'
                ],
                'contact' => [
                    'minimum' => '3',
                    'maximum' => '9',
                    'increment' => '3'
                ],
                'courseWeight' => '10'
            ]
        ];
    }

    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(CourseModelV8::class, $this->courseModelV8);
    }

    public function testCanSetDataForV8Attributes(): void
    {
        $this->courseModelV8->setData($this->testData);
        $this->assertInstanceOf(CourseBilling::class, $this->courseModelV8->getBilling());
    }

    public function testCanSetDataForCoreAttributes(): void
    {
        $this->courseModelV8->setData($this->testData);
        $this->assertInstanceOf(CourseCreditCollection::class, $this->courseModelV8->getCredits());
    }

    public function testCanGetAsEthosDataModelArrayForV8Attributes(): void
    {
        $this->courseModelV8->setData($this->testData);
        $data = $this->courseModelV8->asEthosDataModelArray();
        $this->assertEquals($this->testData['billing'], $data['billing']);
    }

    public function testCanGetAsEthosDataModelArrayForCoreAttributes(): void
    {
        $this->courseModelV8->setData($this->testData);
        $data = $this->courseModelV8->asEthosDataModelArray();
        $this->assertInstanceOf(Guid::class, $data['id']);
    }

    public function testCanGetBillingFromModel(): void
    {
        $this->courseModelV8->setData($this->testData);
        $billing = $this->courseModelV8->getBilling();

        $this->assertInstanceOf(
            CourseBilling::class,
            $billing
        );
    }

    public function testCanGetReportingDetailFromModel(): void
    {
        $this->courseModelV8->setData($this->testData);
        $reporting = $this->courseModelV8->getReportingDetail();

        $this->assertInstanceOf(
            CourseReportingDetail::class,
            $reporting
        );
    }
}