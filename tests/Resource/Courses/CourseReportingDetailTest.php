<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/21/17
 * Time: 12:56 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Course;

use MiamiOH\BannerApi\Resource\Course\CourseReportingDetail;
use MiamiOH\BannerApi\Resource\Course\CourseReportingDetailHours;
use PHPUnit\Framework\TestCase;

class CourseReportingDetailTest extends TestCase
{

    public function testCanBeCreatedWithOnlyType(): void
    {
        $this->assertInstanceOf(
            CourseReportingDetail::class,
            new CourseReportingDetail(
                'Nebraska'
            )
        );
    }

    public function testCanBeCreatedWithValues(): void
    {
        $detailHours = [
            'lab' => new CourseReportingDetailHours(2,6, 1),
            'lecture' => new CourseReportingDetailHours(1,7, 2),
            'contact' => new CourseReportingDetailHours(3,9, 3),
        ];

        $this->assertInstanceOf(
            CourseReportingDetail::class,
            new CourseReportingDetail(
                'Nebraska',
                $detailHours,
                10
            )
        );
    }

    public function testCanBeCollectedAsArray(): void
    {
        $detailHours = [
            'lab' => new CourseReportingDetailHours(2,6, 1),
            'lecture' => new CourseReportingDetailHours(1,7, 2),
            'contact' => new CourseReportingDetailHours(3,9, 3),
        ];

        $reporting = new CourseReportingDetail(
            'Nebraska',
            $detailHours,
            10
        );

        $data = $reporting->asEthosDataModelArray();

        $this->assertEquals('Nebraska', $data['type']);
        $this->assertEquals(10, $data['courseWeight']);
        $this->assertEquals(2, $data['lab']['minimum']);
        $this->assertEquals(6, $data['lab']['maximum']);
        $this->assertEquals(1, $data['lab']['increment']);
        $this->assertEquals(1, $data['lecture']['minimum']);
        $this->assertEquals(7, $data['lecture']['maximum']);
        $this->assertEquals(2, $data['lecture']['increment']);
        $this->assertEquals(3, $data['contact']['minimum']);
        $this->assertEquals(9, $data['contact']['maximum']);
        $this->assertEquals(3, $data['contact']['increment']);
    }

    public function testCanBeCollectedAsArrayWithPartial(): void
    {
        $detailHours = [
            'lab' => new CourseReportingDetailHours(2,6, 1),
        ];

        $reporting = new CourseReportingDetail(
            'Nebraska',
            $detailHours
        );

        $data = $reporting->asEthosDataModelArray();

        $this->assertEquals('Nebraska', $data['type']);
        $this->assertEquals(2, $data['lab']['minimum']);
        $this->assertEquals(6, $data['lab']['maximum']);
        $this->assertEquals(1, $data['lab']['increment']);
    }

}
