<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:53 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use MiamiOH\BannerApi\Resource\ResourceError;
use PHPUnit\Framework\TestCase;

class ResourceErrorTest extends TestCase
{

    public function testCanBeCreatedWithValues(): void
    {
        $this->assertInstanceOf(
            ResourceError::class,
            new ResourceError('testcode', 'my message', 'my description')
        );
    }

}
