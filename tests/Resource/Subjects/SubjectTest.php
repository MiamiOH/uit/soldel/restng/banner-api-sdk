<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 9:19 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Subject;

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\Resource\Subject\Subject;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollectionFinder;
use MiamiOH\BannerApi\Resource\Subject\SubjectModelFinder;
use PHPUnit\Framework\TestCase;

class SubjectTest extends TestCase
{
    /**
     * @var Subject
     */
    private $resource;

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();
        $location = ApiFactory::getNullLocation('https://example.com', $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);

        $this->resource = new Subject($loader);
    }

    public function testResourceHasCorrectName(): void
    {
        $this->assertEquals('subjects', $this->resource->getName());
    }

    public function testResourceHasCorrectSource(): void
    {
        $this->assertEquals('student', $this->resource->getSource());
    }

    public function testCanGetSubjectModelFinder(): void
    {
        $this->assertInstanceOf(SubjectModelFinder::class, $this->resource->findOne());
    }

    public function testCanGetSubjectCollectionFinder(): void
    {
        $this->assertInstanceOf(SubjectCollectionFinder::class, $this->resource->find());
    }
}
