<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 9:17 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Subject;

use MiamiOH\BannerApi\ModelResourceOptions;
use MiamiOH\BannerApi\Resource\Subject\SubjectModel;
use PHPUnit\Framework\TestCase;

class SubjectModelTest extends TestCase
{

    /**
     * @var SubjectModel
     */
    private $subject;

    /**
     * @var ModelResourceOptions
     */
    private $options;

    private $testData = [];
    
    public function setUp(): void
    {
        $this->subject = new SubjectModel();
        $this->options = new ModelResourceOptions([]);
        $this->testData = [
            'id' => 'ca60d2ff-ab8a-4d45-94f4-cf5979823bda',
            'abbreviation' => 'MTH',
            'title' => 'Mathematics'
        ];
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(SubjectModel::class, $this->subject);
    }

    public function testCanBeCreatedAsNewModel(): void
    {
        unset($this->testData['id']);

        $this->assertInstanceOf(SubjectModel::class, new SubjectModel());
    }

    public function testCanBeInitializedWithData(): void
    {
        $this->subject->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['id'], $this->subject->getId());
    }

    public function testCanGetAbbreviationFromModel(): void
    {
        $this->subject->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['abbreviation'], $this->subject->getAbbreviation());
    }

    public function testCanGetTitleFromModel(): void
    {
        $this->subject->initialize($this->testData, $this->options);

        $this->assertEquals($this->testData['title'], $this->subject->getTitle());
    }
    
    public function testCanBeCollectedAsArray(): void
    {
        $this->subject->initialize($this->testData, $this->options);
        $this->assertEquals($this->testData, $this->subject->asEthosDataModelArray());
    }

    public function testCanBeConvertedtoJson(): void
    {
        $this->subject->initialize($this->testData, $this->options);
        $this->assertEquals($this->testData, json_decode(json_encode($this->subject), true));
    }

}
