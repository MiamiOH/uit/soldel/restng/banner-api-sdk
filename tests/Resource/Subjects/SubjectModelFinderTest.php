<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 1:06 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Subject;

use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\MissingUniqueIdentifierException;
use MiamiOH\BannerApi\Guid;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Resource\Subject\Subject;
use MiamiOH\BannerApi\Resource\Subject\SubjectModel;
use MiamiOH\BannerApi\Resource\Subject\SubjectModelFinder;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Tests\TestClassBase;

class SubjectModelFinderTest extends TestClassBase
{
    /**
     * @var BannerResource
     */
    private $resource;
    /**
     * @var Location
     */
    private $location;

    private $testData = [];

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();

        $this->location = ApiFactory::getNullLocation('https://example.com', $auth);
        $this->resource = BannerResource::fromString('api-test-ok');

        $this->testData = [
            'id' => 'ca60d2ff-ab8a-4d45-94f4-cf5979823bda',
            'title' => 'Aeronautics',
            'abbreviation' => 'AER',
        ];
    }

    public function testCanBeCreatedWithLocation(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $resourceInterface = new Subject($loader);
        $finder = new SubjectModelFinder($resourceInterface);

        $this->assertInstanceOf(SubjectModelFinder::class, $finder);
    }

    public function testCanNotExecuteWithoutUniqueIdentifier(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $resourceInterface = new Subject($loader);
        $finder = new SubjectModelFinder($resourceInterface);

        $this->expectException(MissingUniqueIdentifierException::class);
        $finder->execute();
    }

    public function testReturnsExpectedModel(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $resourceInterface = new Subject($loader);

        $finder = new SubjectModelFinder($resourceInterface);
        $id = Guid::fromString($this->testData['id']);

        $this->assertInstanceOf(SubjectModel::class, $finder->withId($id)->execute());
    }

    public function testCanFindWithGuid(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        $resourceInterface = new Subject($loader);

        $finder = new SubjectModelFinder($resourceInterface);

        $id = Guid::fromString($this->testData['id']);
        $course = $finder->withId($id)->execute();

        $this->assertEquals($this->testData['id'], $course->getId());
    }

    private function newLoaderWithResponses(array $responses): ResourceLoaderRest
    {
        $client = $this->newHttpClientWithResponses($responses);
        return new ResourceLoaderRest($this->location, $client);

    }
}
