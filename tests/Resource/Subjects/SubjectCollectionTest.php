<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 1:06 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Subject;

use MiamiOH\BannerApi\CollectionResourceOptions;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollection;
use MiamiOH\BannerApi\Resource\Subject\SubjectModel;
use PHPUnit\Framework\TestCase;

class SubjectCollectionTest extends TestCase
{
    /**
     * @var SubjectCollection
     */
    private $collection;

    /**
     * @var CollectionResourceOptions
     */
    private $options;

    public function setUp(): void
    {
        $this->collection = new SubjectCollection();
        $this->options = new CollectionResourceOptions([]);
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(SubjectCollection::class, $this->collection);
    }

    public function testCanBeInitializedWithModels(): void
    {
        $models = [new SubjectModel()];

        $this->collection->initialize($models, $this->options);

        $this->assertCount(1, $this->collection);
    }

    public function testCanBeIteratedAsArray(): void
    {
        $this->collection->initialize([
            new SubjectModel(),
            new SubjectModel(),
        ], $this->options);

        foreach ($this->collection as $model) {
            $this->assertInstanceOf(SubjectModel::class, $model);
        }
    }

}
