<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 3:44 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Subject;

use MiamiOH\BannerApi\Resource\Subject\SubjectCollection;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollectionFinder;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollectionPager;
use MiamiOH\BannerApi\Tests\Resource\TestCollectionPagerBase;

class SubjectCollectionPagerTest extends TestCollectionPagerBase
{

    /**
     * @var SubjectCollectionFinder|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $finder;

    public function setUp(): void
    {
        $this->finderClass = SubjectCollectionFinder::class;
        $this->collectionClass = SubjectCollection::class;
        $this->pagerClass = SubjectCollectionPager::class;
    }
}
