<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/23/17
 * Time: 1:05 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource\Subject;

use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Location;
use MiamiOH\BannerApi\Resource\Subject\Subject;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollection;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollectionFinder;
use MiamiOH\BannerApi\Resource\ResourceLoaderRest;
use MiamiOH\BannerApi\Resource\Subject\SubjectCollectionPager;
use MiamiOH\BannerApi\Tests\TestClassBase;

class SubjectCollectionFinderTest extends TestClassBase
{
    /**
     * @var BannerResource
     */
    private $resource;
    /**
     * @var Location
     */
    private $location;

    private $testData = [];

    public function setUp(): void
    {
        $auth = ApiFactory::getNullAuthentication();

        $this->location = ApiFactory::getNullLocation('https://example.com', $auth);

        $this->testData = [
            [
                'id' => '32e131b2-7eb4-4b78-80e3-2a2a92a272b3',
                'title' => 'Accountancy',
                'abbreviation' => 'ACC',
            ]
        ];
    }

    public function testCanBeCreatedWithLocation(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $loader = new ResourceLoaderRest($this->location, $client);
        $resourceInterface = new Subject($loader);
        $finder = new SubjectCollectionFinder($resourceInterface);

        $this->assertInstanceOf(SubjectCollectionFinder::class, $finder);
    }

    public function testCanGetCollection(): void
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);

        $resourceInterface = new Subject($loader);

        $finder = new SubjectCollectionFinder($resourceInterface);

        $collection = $finder->execute();

        $this->assertInstanceOf(SubjectCollection::class, $collection);
    }

    public function testCanUseAdHocQueryFilter()
    {
        $loader = $this->newLoaderWithResponses([
            new Response(200, [], json_encode($this->testData)),
        ]);
        $resourceInterface = new Subject($loader);

        $finder = new SubjectCollectionFinder($resourceInterface);
        
        $collection = $finder->withQuery('title' , 'Accountancy')->execute();

        $this->assertSingleRequest();
        $this->assertRequestQueryStringContains(1, 'title=');
    }

    public function testCanGetPager(): void
    {
        $loader = $this->newLoaderWithResponses([]);
        $resourceInterface = new Subject($loader);

        $finder = new SubjectCollectionFinder($resourceInterface);

        $pager = $finder->withLimit(30)->withOffset(0)->pager();
        $this->assertInstanceOf(SubjectCollectionPager::class, $pager);
    }

    private function newLoaderWithResponses(array $responses): ResourceLoaderRest
    {
        $client = $this->newHttpClientWithResponses($responses);
        return new ResourceLoaderRest($this->location, $client);

    }
}
