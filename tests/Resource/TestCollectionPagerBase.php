<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/17
 * Time: 8:30 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use MiamiOH\BannerApi\Resource\BaseCollection;
use MiamiOH\BannerApi\Resource\BaseCollectionFinder;
use MiamiOH\BannerApi\Resource\BaseCollectionPager;
use MiamiOH\BannerApi\Resource\CollectionInterface;
use MiamiOH\BannerApi\Resource\CollectionPagerInterface;
use PHPUnit\Framework\TestCase;

class TestCollectionPagerBase extends TestCase
{

    /**
     * @var BaseCollectionFinder|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $finder;

    protected $finderClass = BaseCollectionFinder::class;
    protected $collectionClass = BaseCollection::class;
    protected $pagerClass = BaseCollectionPager::class;

    public function testCanBeCreatedWithFinder(): void
    {
        $pager = $this->createPager();
        $this->assertInstanceOf($this->pagerClass, $pager);
    }

    public function testCanGetInitialOffsetFromFinder(): void
    {
        $pager = $this->createPager(10);
        $this->assertEquals(10, $pager->getOffset());
    }

    public function testCanGetInitialLimitFromFinder(): void
    {
        $pager = $this->createPager(0, 100);
        $this->assertEquals(100, $pager->getLimit());
    }

    public function testCanGetTotalCountAfterExecuting(): void
    {
        $pager = $this->createPager(0, 100);

        $collection = $this->createCollection();

        $collection->expects($this->once())->method('getTotalCount')->willReturn(23);

        $this->finder->expects($this->once())->method('withOffset')
            ->with($this->equalTo(0))
            ->will($this->returnSelf());
        $this->finder->expects($this->once())->method('withLimit')
            ->with($this->equalTo(100))
            ->will($this->returnSelf());
        $this->finder->expects($this->once())->method('execute')->willReturn($collection);

        $pager->next();

        $this->assertEquals(23, $pager->getTotalCount());
    }

    public function testIncrementsOffsetCorrectlyWhileExecuting(): void
    {
        $pager = $this->createPager();

        $collection = $this->createCollection();

        $collection->expects($this->exactly(3))->method('getTotalCount')->willReturn(43);

        $this->finder->expects($this->exactly(3))->method('withOffset')
            ->withConsecutive([$this->equalTo(0)], [$this->equalTo(10)], [$this->equalTo(20)])
            ->will($this->returnSelf());
        $this->finder->expects($this->exactly(3))->method('withLimit')
            ->with($this->equalTo(10))
            ->will($this->returnSelf());
        $this->finder->expects($this->exactly(3))->method('execute')->willReturn($collection);

        $pager->next();
        $pager->next();
        $pager->next();

    }

    public function testReturnsNullWhenTotalCountIsExceeded(): void
    {
        $pager = $this->createPager();

        $collection = $this->createCollection();

        $collection->expects($this->exactly(2))->method('getTotalCount')->willReturn(13);

        $this->finder->expects($this->exactly(2))->method('withOffset')
            ->withConsecutive([$this->equalTo(0)], [$this->equalTo(10)])
            ->will($this->returnSelf());
        $this->finder->expects($this->exactly(2))->method('withLimit')
            ->with($this->equalTo(10))
            ->will($this->returnSelf());
        $this->finder->expects($this->exactly(2))->method('execute')->willReturn($collection);

        $pager->next();
        $pager->next();
        $this->assertNull($pager->next());

    }

    protected function createPager(int $offset = 0, int $limit = 10)
    {
        $this->finder = $this->getMockBuilder($this->finderClass)
            ->disableOriginalConstructor()
            ->setMethods(['getOffset', 'getLimit', 'withOffset', 'withLimit', 'execute'])
            ->getMockForAbstractClass();
        $this->finder->method('getOffset')->willReturn($offset);
        $this->finder->method('getLimit')->willReturn($limit);

        /** @var CollectionPagerInterface|\PHPUnit_Framework_MockObject_MockObject $pager */
        $pager = $this->getMockBuilder($this->pagerClass)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $reflectedClass = new \ReflectionClass($this->pagerClass);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($pager, $this->finder);

        return $pager;
    }

    protected function createCollection()
    {
        /** @var CollectionInterface|\PHPUnit_Framework_MockObject_MockObject $collection */
        $collection = $this->getMockBuilder($this->collectionClass)
            ->setMethods(['getTotalCount'])
            ->getMockForAbstractClass();

        return $collection;
    }
}
