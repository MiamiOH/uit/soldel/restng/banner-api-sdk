<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:53 AM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\Resource\ResourceError;
use MiamiOH\BannerApi\Resource\ResourceErrorCollection;
use PHPUnit\Framework\TestCase;

class ResourceErrorCollectionTest extends TestCase
{

    private $responseData = [];
    private $responseHeaders = [];
    /**
     * @var Response
     */
    private $response;

    public function setUp(): void
    {
        $this->responseData = [
            'errors' => [
                [
                    'code' => 'Global.SchemaValidation.Error',
                    'message' => 'A mapping rule does not exist for SECTIONDETAIL.STATUS.V4',
                    'description' => 'Errors parsing input JSON.'
                ]
            ]
        ];

        $this->responseHeaders = [
            'date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
            'X-Request-ID' => 'cefb16dc-9397-4b1d-968d-d972474fea04',
            'X-Status-Reason' => 'Validation failed',
            'X-hedtech-message' => 'A mapping rule does not exist for SECTIONDETAIL.STATUS.V4'
        ];

        $this->response = new Response(400, $this->responseHeaders, json_encode($this->responseData));
    }

    public function testCanBeCreatedFromResponse(): void
    {
        $this->assertInstanceOf(
            ResourceErrorCollection::class,
            ResourceErrorCollection::fromResponse($this->response)
        );
    }

    public function testCanNotBeCreatedWithInvalidErrorObjects(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new ResourceErrorCollection(
            'cefb16dc-9397-4b1d-968d-d972474fea04',
            'my reason',
            'my message',
            Carbon::now(),
            [new \stdClass()]
        );
    }

    public function testCanBeCreatedWithoutReasonHeader(): void
    {
        $this->responseHeaders = [
            'date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
            'X-Request-ID' => 'cefb16dc-9397-4b1d-968d-d972474fea04',
            'X-hedtech-message' => 'A mapping rule does not exist for SECTIONDETAIL.STATUS.V4'
        ];

        $this->response = new Response(400, $this->responseHeaders, json_encode($this->responseData));

        $this->assertInstanceOf(
            ResourceErrorCollection::class,
            ResourceErrorCollection::fromResponse($this->response)
        );
    }

    public function testCanBeCreatedWithNotFoundResponse(): void
    {
        $this->responseHeaders = [
            'date' => 'Thu, 22 Jun 2017 11:57:49 GMT',
            'X-Request-ID' => 'cefb16dc-9397-4b1d-968d-d972474fea04',
            'X-hedtech-message' => 'Course not found'
        ];

        $this->response = new Response(404, $this->responseHeaders, json_encode($this->responseData));

        $this->assertInstanceOf(
            ResourceErrorCollection::class,
            ResourceErrorCollection::fromResponse($this->response)
        );
    }

    public function testCanBeUsedAsString(): void
    {
        $collection = ResourceErrorCollection::fromResponse($this->response);

        $this->assertContains('Validation failed', (string) $collection);
    }

    public function testCanBeCounted(): void
    {
        $collection = ResourceErrorCollection::fromResponse($this->response);

        $this->assertCount(1, $collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        $collection = ResourceErrorCollection::fromResponse($this->response);

        $errors = $collection->toArray();
        $this->assertInstanceOf(ResourceError::class, $errors[0]);
    }

    public function testCanBeIteratedAsArray(): void
    {
        $collection = ResourceErrorCollection::fromResponse($this->response);

        foreach ($collection as $error) {
            $this->assertInstanceOf(ResourceError::class, $error);
        }
    }

    public function testCanBeIterateAsAssoc(): void
    {
        $collection = ResourceErrorCollection::fromResponse($this->response);

        foreach ($collection as $key => $error) {
            $this->assertInstanceOf(ResourceError::class, $error);
        }
    }

}
