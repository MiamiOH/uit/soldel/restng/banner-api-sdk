<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 12:28 PM
 */

namespace MiamiOH\BannerApi\Tests\Resource;

use Carbon\Carbon;
use MiamiOH\BannerApi\CollectionResourceOptions;
use MiamiOH\BannerApi\Exception\InvalidCollectionEntryException;
use MiamiOH\BannerApi\Resource\BaseCollection;
use MiamiOH\BannerApi\Resource\BaseModel;
use PHPUnit\Framework\TestCase;

class BaseCollectionTest extends TestCase
{
    /**
     * @var BaseCollection
     */
    private $collection;

    /**
     * @var CollectionResourceOptions
     */
    private $options;

    public function setUp(): void
    {
        $this->collection = $this->getMockForAbstractClass(BaseCollection::class);
        $this->collection->setModelClass(get_class($this->getTestModel()));

        $this->options = new CollectionResourceOptions([
            'date' => Carbon::createFromDate(2012, 1, 1),
            'mediaType' => 'json',
            'requestId' => 'abc123',
            'message' => 'test resource',
            'pageMaxSize' => 10,
            'pageOffset' => 0,
            'totalCount' => 23,
        ]);
    }

    public function testCanBeCreatedEmpty(): void
    {
        $this->assertInstanceOf(BaseCollection::class, $this->collection);
    }

    public function testCanBeInitialized(): void
    {
        $models = [
            $this->getTestModel()
        ];

        $this->collection->initialize($models, new CollectionResourceOptions([]));

        $this->assertCount(1, $this->collection);
    }

    public function testRequiresExpectedModelClass(): void
    {
        $models = [
            new \stdClass()
        ];

        $this->expectException(InvalidCollectionEntryException::class);
        $this->collection->initialize($models, new CollectionResourceOptions([]));
    }

    public function testCanBeCounted(): void
    {
        $this->collection->initialize([$this->getTestModel()], $this->options);
        $this->assertCount(1, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        $this->collection->initialize([$this->getTestModel()], $this->options);
        $data = $this->collection->toArray();
        $this->assertInstanceOf(BaseModel::class, $data[0]);
    }

    public function testCanGetRequestDate(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals(Carbon::createFromDate(2012, 1, 1), $this->collection->getRequestDate());
    }

    public function testCanGetRequestMediaType(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals('json', $this->collection->getRequestMediaType());
    }

    public function testCanGetRequestId(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals('abc123', $this->collection->getRequestId());
    }

    public function testCanGetRequestMessage(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals('test resource', $this->collection->getRequestMessage());
    }

    public function testPageMaxSize(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals($this->options->getPageMaxSize(), $this->collection->getPageMaxSize());
    }

    public function testPageOffset(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals($this->options->getPageOffset(), $this->collection->getPageOffset());
    }

    public function testCanGetTotalCount(): void
    {
        $this->collection->initialize([], $this->options);
        $this->assertEquals($this->options->getTotalCount(), $this->collection->getTotalCount());
    }

    public function testCanBeCollectedAsArrayBase(): void
    {
        $expected = [
            ['id' => '0d97179e-488e-4717-9510-6672f2a625f7'],
            ['id' => '0d97179e-488e-4717-9510-6672f2a625f8']
        ];

        $data = [];
        foreach ($expected as $id) {
            $data[] = $this->getTestModel($id);
        }

        $this->collection->initialize($data, $this->options);
        $this->assertEquals($expected, $this->collection->asEthosDataModelArray());
    }

    public function testCanBeConvertedtoJson(): void
    {
        $expected = [
            ['id' => '0d97179e-488e-4717-9510-6672f2a625f7'],
            ['id' => '0d97179e-488e-4717-9510-6672f2a625f8']
        ];

        $data = [];
        foreach ($expected as $id) {
            $data[] = $this->getTestModel($id);
        }

        $this->collection->initialize($data, $this->options);
        $this->assertEquals(json_encode($expected), json_encode($this->collection));
    }

    private function getTestModel($data = []): BaseModel
    {
        /** @var BaseModel $model */
        $model = $this->getMockForAbstractClass(BaseModel::class);
        $model->initialize($data);
         
        return $model;
    }
}
