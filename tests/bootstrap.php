<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 1:18 PM
 */

include_once __DIR__ . '/../vendor/autoload.php';

// Setup a null log handler for running tests
\MiamiOH\BannerApi\LogManager::setConfiguration(__DIR__ . '/logconfig.yml');

\MiamiOH\BannerApi\BannerResource::setResourceConfig(
    yaml_parse_file(__DIR__ . '/../examples/resources-dist.yml')['resources']
);