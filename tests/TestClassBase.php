<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 9:57 PM
 */

namespace MiamiOH\BannerApi\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class TestClassBase extends TestCase
{
    protected $container = [];

    protected function newHttpClientWithResponses(array $responses): Client
    {
        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }

    protected function assertSingleRequest(): void
    {
        $this->assertCount(1, $this->container);
    }

    protected function assertRequestQueryStringContains(int $requestNum, string $string): void
    {
        /** @var Request $request */
        $request = $this->container[$requestNum - 1]['request'];

        $this->assertContains($string, $request->getRequestTarget());
    }
}
