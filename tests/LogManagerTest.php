<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/22/17
 * Time: 8:50 PM
 */

namespace MiamiOH\BannerApi\Tests;

use Cascade\Cascade;
use MiamiOH\BannerApi\LogManager;
use Monolog\Logger;
use Monolog\Registry;
use PHPUnit\Framework\TestCase;

class LogManagerTest extends TestCase
{

    public function setUp(): void
    {
        LogManager::reset();
    }

    public static function tearDownAfterClass(): void
    {
        // Make sure to reset to the null handler
        LogManager::reset();
        LogManager::setConfiguration(__DIR__ . '/logconfig.yml');
    }

    public function testCanBeCreatedWithDefaultConfig(): void
    {
        $logger = LogManager::logger();
        $this->assertInstanceOf(Logger::class, $logger);
        $this->assertEquals('default', $logger->getName());
    }

    public function testCanBeCreatedWithExpectedName(): void
    {
        $logger = LogManager::logger('courses');
        $this->assertEquals('courses', $logger->getName());
    }

    public function testCanSetConfig(): void
    {
        $logConfig = [
            'handlers' => [
                'console' => [
                    'class' => 'Monolog\Handler\StreamHandler',
                    'level' => 'DEBUG',
                    'stream' => 'php://stderr'
                ],
            ],
            'loggers' => [
                'default' => [
                    'handlers' => ['console']
                ],
            ],
        ];

        LogManager::setConfiguration($logConfig);
        $logger = LogManager::logger();
        $this->assertInstanceOf(Logger::class, $logger);
    }

    public function testHandlesErrorByDefault(): void
    {
        $logger = LogManager::logger();
        $this->assertFalse($logger->isHandling(Logger::WARNING));
        $this->assertTrue($logger->isHandling(Logger::ERROR));
    }

    public function testCanChangeLogLevel(): void
    {
        LogManager::setLogLevel('CRITICAL');
        $this->assertEquals('CRITICAL', LogManager::getLogLevel());
    }

    public function testUsesCorrectLogLevel(): void
    {
        LogManager::setLogLevel('CRITICAL');
        $logger = LogManager::logger();
        $this->assertFalse($logger->isHandling(Logger::ERROR));
        $this->assertTrue($logger->isHandling(Logger::CRITICAL));
    }

    public function testCanSetLogFileLocation(): void
    {
        LogManager::setLogFile('banner.log');
        $logger = LogManager::logger();
        $this->assertEquals('banner.log', LogManager::getLogFile());
    }

}
