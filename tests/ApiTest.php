<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 9:57 PM
 */

namespace MiamiOH\BannerApi\Tests;

use GuzzleHttp\Client;
use MiamiOH\BannerApi\Api;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Resource\AcademicLevel\AcademicLevel;
use MiamiOH\BannerApi\Resource\Course\Course;
use MiamiOH\BannerApi\Resource\ResourceLoaderInterface;
use MiamiOH\BannerApi\Resource\Subject\Subject;
use PHPUnit\Framework\TestCase;

class ApiTest extends TestCase
{
    /**
     * @var Api
     */
    private $api;

    public function setUp(): void
    {
        $auth = ApiFactory::getBannerApiAuthentication('bubba', 'secret');
        $location = ApiFactory::getBannerApiLocation('https://example.com', $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);

        $this->api = new Api($loader);
    }

    /*
     * Every resource must have default version and explicit non-default version
     * tests in order to insure proper behavior as Banner API resource versions
     * change.
     *
     * We will test with explicit version numbers in order to force consideration
     * for version changes in the Banner APIs
     */

    /*
     * Resource: courses
     * Banner Student API 9.9 supports courses v6 and v8
     */
    public function testCanGetCoursesResourceDefaultVersion(): void
    {
        $courses = $this->api->getCoursesResource();
        $this->assertInstanceOf(Course::class, $courses);
        $this->assertEquals(8, $courses->getVersion());
    }

    public function testCanGetCoursesResourceExplicitVersion(): void
    {
        $courses = $this->api->getCoursesResource(6);
        $this->assertInstanceOf(Course::class, $courses);
        $this->assertEquals(6, $courses->getVersion());
    }

    /*
     * Resource: subjects
     * Banner Student API 9.9 supports subjects v6 only
     */
    public function testCanGetSubjectsResourceDefaultVersion(): void
    {
        $subjects = $this->api->getSubjectsResource();
        $this->assertInstanceOf(Subject::class, $subjects);
        $this->assertEquals(6, $subjects->getVersion());
    }
    
    public function testCanGetSubjectsResourceExplicitVersion(): void
    {
        $subjects = $this->api->getSubjectsResource(6);
        $this->assertInstanceOf(Subject::class, $subjects);
        $this->assertEquals(6, $subjects->getVersion());
    }

    /*
     * Resource: academic-levels
     * Banner Student API 9.9 supports academic-levels v6 only
     */
    public function testCanGetAcademicLevelsResourceDefaultVersion(): void
    {
        $academicLevels = $this->api->getAcademicLevelsResource();
        $this->assertInstanceOf(AcademicLevel::class, $academicLevels);
        $this->assertEquals(6, $academicLevels->getVersion());
    }

    public function testCanGetAcademicLevelsResourceExplicitVersion(): void
    {
        $academicLevels = $this->api->getAcademicLevelsResource(6);
        $this->assertInstanceOf(AcademicLevel::class, $academicLevels);
        $this->assertEquals(6, $academicLevels->getVersion());
    }

    /*
     * The Banner API packages (Student API and Integration API) contains a resources
     * resource which lists the available resources and represenations (versions). We
     * can use the to introspect and determine supported resources and versions in the
     * SDK.
     */

    public function testCanGetStudentApiPackage(): void
    {
        /** @var ResourceLoaderInterface|\PHPUnit_Framework_MockObject_MockObject $loader */
        $loader = $this->getMockBuilder(ResourceLoaderInterface::class)
            ->setMethods(['getResource', 'getCollectionFilter', 'getResourceById', 'getApiPackage'])
            ->disableOriginalConstructor()
            ->getMock();

        $loader->method('getApiPackage')->willReturn(
            new Api\ApiPackage(Api\ApiPackage::STUDENT, new Api\ResourceDefinitionCollection([]))
        );

        $api = new Api($loader);

        $package = $api->getStudentApiPackage();

        $this->assertInstanceOf(Api\ApiPackage::class, $package);
    }

    public function testCanGetIntegrationApiPackage(): void
    {
        /** @var ResourceLoaderInterface|\PHPUnit_Framework_MockObject_MockObject $loader */
        $loader = $this->getMockBuilder(ResourceLoaderInterface::class)
            ->setMethods(['getResource', 'getCollectionFilter', 'getResourceById', 'getApiPackage'])
            ->disableOriginalConstructor()
            ->getMock();

        $loader->method('getApiPackage')->willReturn(
            new Api\ApiPackage(Api\ApiPackage::INTEGRATION, new Api\ResourceDefinitionCollection([]))
        );

        $api = new Api($loader);

        $package = $api->getIntegrationApiPackage();

        $this->assertInstanceOf(Api\ApiPackage::class, $package);
    }
}
