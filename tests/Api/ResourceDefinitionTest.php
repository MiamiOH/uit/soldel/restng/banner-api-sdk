<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 11:05 AM
 */

namespace MiamiOH\BannerApi\Tests\Api;


use MiamiOH\BannerApi\Api\RepresentationCollection;
use MiamiOH\BannerApi\Api\ResourceDefinition;
use PHPUnit\Framework\TestCase;

class ResourceDefinitionTest extends TestCase
{

    /** @var  ResourceDefinition */
    private $resource;

    public function setUp(): void
    {
        $this->resource = ResourceDefinition::fromArray([
            'name' => 'academic-catalogs',
            'representations' => [
                [
                    'X-Media-Type' => 'application/vnd.hedtech.integration.v6+json',
                    'methods' => [
                        'get'
                    ]
                ]
            ]
        ]);
    }

    public function testCanBeCreatedFromArray(): void
    {
        $this->assertInstanceOf(ResourceDefinition::class, $this->resource);
    }

    public function testCanGetName(): void
    {
        $this->assertEquals('academic-catalogs', $this->resource->getName());
    }

    public function testCanGetRepresentations(): void
    {
        $representations = $this->resource->getRepresentations();

        $this->assertInstanceOf(RepresentationCollection::class, $representations);
        $this->assertCount(1, $representations);
    }
}
