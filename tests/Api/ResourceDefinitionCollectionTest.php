<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 11:06 AM
 */

namespace MiamiOH\BannerApi\Tests\Api;

use MiamiOH\BannerApi\Api\ResourceDefinition;
use MiamiOH\BannerApi\Api\ResourceDefinitionCollection;
use PHPUnit\Framework\TestCase;

class ResourceDefinitionCollectionTest extends TestCase
{

    /** @var  ResourceDefinitionCollection */
    private $collection;

    public function setUp(): void
    {
        $this->collection = ResourceDefinitionCollection::fromArray([
            [
                'name' => 'academic-catalogs',
                'representations' => [
                    [
                        'X-Media-Type' => 'application/vnd.hedtech.integration.v6+json',
                        'methods' => [
                            'get'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testCanBeCreatedFromArray(): void
    {
        $this->assertInstanceOf(ResourceDefinitionCollection::class, $this->collection);
    }

    public function testCanBeCounted(): void
    {
        $this->assertCount(1, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        $this->assertTrue(is_array($this->collection->toArray()));
    }

    public function testCreatesDefinitionsFromDataArray(): void
    {
        foreach ($this->collection as $key => $definition) {
            $this->assertInstanceOf(ResourceDefinition::class, $definition);
        }
    }

    public function testCanNotBeCreatedWithOtherObjects(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new ResourceDefinitionCollection([new \stdClass()]);
    }
}
