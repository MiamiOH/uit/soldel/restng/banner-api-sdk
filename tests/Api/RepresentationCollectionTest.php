<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 11:27 AM
 */

namespace MiamiOH\BannerApi\Tests\Api;

use MiamiOH\BannerApi\Api\Representation;
use MiamiOH\BannerApi\Api\RepresentationCollection;
use PHPUnit\Framework\TestCase;

class RepresentationCollectionTest extends TestCase
{

    /** @var  RepresentationCollection */
    private $collection;

    public function setUp(): void
    {
        $this->collection = RepresentationCollection::fromArray([
            Representation::fromArray([
                'X-Media-Type' => 'application/vnd.hedtech.integration.v6+json',
                'methods' => [
                    'get'
                ]
            ])
        ]);

    }
    public function testCanBeCreatedFromEmptyArray(): void
    {
        $this->assertInstanceOf(
            RepresentationCollection::class,
            RepresentationCollection::fromArray([])
        );
    }

    public function testCanBeCreatedFromArrayOfRepresentations(): void
    {
        $this->assertInstanceOf(RepresentationCollection::class, $this->collection);
    }

    public function testCanNotBeCreatedFromArrayOfOther(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        RepresentationCollection::fromArray([
            new \stdClass()
        ]);
    }

    public function testCanBeCounted(): void
    {
        $this->assertCount(1, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        $this->assertTrue(is_array($this->collection->toArray()));
    }

    public function testCreatesDefinitionsFromDataArray(): void
    {
        foreach ($this->collection as $key => $definition) {
            $this->assertInstanceOf(Representation::class, $definition);
        }
    }

}
