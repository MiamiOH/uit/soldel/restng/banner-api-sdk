<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 4:21 PM
 */

namespace MiamiOH\BannerApi\Tests\Api;

use MiamiOH\BannerApi\Api\ApiPackage;
use MiamiOH\BannerApi\Api\ResourceDefinitionCollection;
use PHPUnit\Framework\TestCase;

class ApiPackageTest extends TestCase
{

    /** @var  ApiPackage */
    private $apiPackage;

    public function setUp(): void
    {
        $this->apiPackage = new ApiPackage(
            ApiPackage::STUDENT,
            new ResourceDefinitionCollection([])
        );
    }

    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(ApiPackage::class, $this->apiPackage);
    }

    public function testCanGetPackageName(): void
    {
        $this->assertEquals(ApiPackage::STUDENT, $this->apiPackage->getName());
    }

    public function testCanGetPackageResourceDefinitions(): void
    {
        $this->assertInstanceOf(ResourceDefinitionCollection::class, $this->apiPackage->getResourceDefinitions());
    }
}
