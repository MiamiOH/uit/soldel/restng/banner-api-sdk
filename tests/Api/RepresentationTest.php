<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 11/23/17
 * Time: 11:27 AM
 */

namespace MiamiOH\BannerApi\Tests\Api;

use MiamiOH\BannerApi\Api\Representation;
use PHPUnit\Framework\TestCase;

class RepresentationTest extends TestCase
{
    /** @var  Representation */
    private $representation;

    public function setUp(): void
    {
        $this->representation = Representation::fromArray([
            'X-Media-Type' => 'application/vnd.hedtech.integration.v6+json',
            'methods' => [
                'get'
            ]
        ]);
    }

    public function testCanBeCreatedFromArray(): void
    {
        $this->assertInstanceOf(Representation::class, $this->representation);
    }

    public function testCanGetMediaType(): void
    {
        $this->assertEquals('application/vnd.hedtech.integration.v6+json', $this->representation->getMediaType());
    }

    public function testCanGetMethods(): void
    {
        $this->assertEquals(['get'], $this->representation->getMethods());
    }
}
