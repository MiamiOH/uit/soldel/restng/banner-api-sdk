<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/15/17
 * Time: 9:22 PM
 */

namespace MiamiOH\BannerApi\Tests;

use GuzzleHttp\Client;
use MiamiOH\BannerApi\Api;
use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\Authentication\BannerApiAuthentication;
use MiamiOH\BannerApi\Authentication\EihAuthentication;
use MiamiOH\BannerApi\Authentication\NullAuthentication;
use MiamiOH\BannerApi\BannerApiLocation;
use MiamiOH\BannerApi\EihLocation;
use PHPUnit\Framework\TestCase;

class ApiFactoryTest extends TestCase
{
    public function testCanGetHttpClient(): void
    {
        $this->assertInstanceOf(Client::class, ApiFactory::getHttpClient());
    }

    public function testCanGetHttpClientWithOptions(): void
    {
        $options = ['base_uri' => 'https://example.com'];
        $client = ApiFactory::getHttpClient($options);
        $this->assertEquals($options['base_uri'], $client->getConfig('base_uri'));
    }

    public function testCanGetNullAuthentication(): void
    {
        $this->assertInstanceOf(NullAuthentication::class, ApiFactory::getNullAuthentication());
    }

    public function testCanGetBannerApiAuthentication(): void
    {
        $this->assertInstanceOf(
            BannerApiAuthentication::class,
            ApiFactory::getBannerApiAuthentication('bubba', 'secret')
        );
    }

    public function testCanGetEihAuthentication(): void
    {
        $this->assertInstanceOf(
            EihAuthentication::class,
            ApiFactory::getEihAuthentication('https://example.com', 'abc123')
        );
    }

    public function testCanGetBannerApiLocation(): void
    {
        $auth = ApiFactory::getBannerApiAuthentication('bubba', 'secret');
        $this->assertInstanceOf(
            BannerApiLocation::class,
            ApiFactory::getBannerApiLocation('https://example.com', $auth)
        );
    }

    public function testCanGetEihLocation(): void
    {
        $auth = ApiFactory::getEihAuthentication('https://example.com', 'abc123');
        $this->assertInstanceOf(
            EihLocation::class,
            ApiFactory::getEihLocation('https://example.com', $auth)
        );
    }

    public function testCanGetApiWithBannerApiLocation(): void
    {
        $auth = ApiFactory::getBannerApiAuthentication('bubba', 'secret');
        $location = ApiFactory::getBannerApiLocation('https://example.com', $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);
        $this->assertInstanceOf(
            Api::class,
            ApiFactory::getApi($loader)
        );
    }

    public function testCanGetApiWithEihLocation(): void
    {
        $auth = ApiFactory::getEihAuthentication('https://example.com', 'abc123');
        $location = ApiFactory::getEihLocation('https://example.com', $auth);
        $loader = ApiFactory::getResourceLoaderRest($location);
        $this->assertInstanceOf(
            Api::class,
            ApiFactory::getApi($loader)
        );
    }

    public function testCanGetBannerApi(): void
    {
        $this->assertInstanceOf(
            Api::class,
            ApiFactory::getBannerApi('https://example.com', 'bubba', 'secr3t')
        );
    }

    public function testCanGetEihApi(): void
    {
        $this->assertInstanceOf(
            Api::class,
            ApiFactory::getEihApi('https://example.com', 'abc123')
        );
    }
}
