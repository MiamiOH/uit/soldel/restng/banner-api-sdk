<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 2:34 PM
 */

namespace MiamiOH\BannerApi\Tests;

use MiamiOH\BannerApi\Authentication\BannerApiAuthentication;
use PHPUnit\Framework\TestCase;

class BannerApiAuthenticationTest extends TestCase
{
    /**
     * @var BannerApiAuthentication
     */
    private $authentication;

    private $username = '';
    private $password = '';

    public function setUp(): void
    {
        $this->username = 'bubba';
        $this->password = 'secr3t';

        $this->authentication = new BannerApiAuthentication($this->username, $this->password);
    }

    public function testCanBeCreatedWithUsernameAndPassword(): void
    {
        $this->assertInstanceOf(BannerApiAuthentication::class, $this->authentication);
    }

    public function testCanAddAuthorizationHeader(): void
    {
        $headers = ['X-test-header' => 'my test header'];
        $newHeaders = $this->authentication->addAuthenticationHeaders($headers);

        $this->assertEquals($this->basicAuthorizationHeader($this->username, $this->password), $newHeaders['Authorization']);
    }

    private function basicAuthorizationHeader(string $username, string $password): string
    {
        return 'Basic ' . base64_encode($username . ':' . $password);
    }
}
