<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/14/17
 * Time: 2:48 PM
 */

namespace MiamiOH\BannerApi\Tests;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use MiamiOH\BannerApi\Authentication\EihAuthentication;
use PHPUnit\Framework\TestCase;

class EihAuthenticationTest extends TestCase
{
    private $baseUrl = '';
    private $token = '';

    private $container = [];

    public function setUp(): void
    {
        $this->baseUrl = 'https://example.com';
        $this->token = 'abc123';
    }

    public function testCanBeCreatedWithUrlAndToken(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        $authentication = new EihAuthentication($client, $this->baseUrl, $this->token);

        $this->assertInstanceOf(EihAuthentication::class, $authentication);
    }

    public function testUsesTokenToRequestJwt(): void
    {
        $jwt = 'jwt-xyz321';
        $client = $this->newHttpClientWithResponses([
            new Response(200, [], $jwt),
        ]);
        $authentication = new EihAuthentication($client, $this->baseUrl, $this->token);

        $authentication->addAuthenticationHeaders([]);

        $this->assertCount(1, $this->container);

        /**
         * @var Request
         */
        $request = $this->container[0]['request'];
        $this->assertEquals(['Bearer ' . $this->token], $request->getHeader('Authorization'));
    }

    public function testCanAddAuthorizationHeader(): void
    {
        $jwt = 'jwt-xyz321';
        $client = $this->newHttpClientWithResponses([
            new Response(200, [], $jwt),
        ]);
        $authentication = new EihAuthentication($client, $this->baseUrl, $this->token);

        $headers = ['X-test-header' => 'my test header'];
        $newHeaders = $authentication->addAuthenticationHeaders($headers);

        $this->assertEquals('Bearer ' . $jwt, $newHeaders['Authorization']);
    }

    public function testCanRefreshExpiredJwt(): void
    {
        $jwt1 = 'jwt-xyz321';
        $jwt2 = 'jwt-efg098';
        $client = $this->newHttpClientWithResponses([
            new Response(200, [], $jwt1),
            new Response(200, [], $jwt2),
        ]);
        $authentication = new EihAuthentication($client, $this->baseUrl, $this->token);

        $headers = $authentication->addAuthenticationHeaders([]);

        $this->assertEquals('Bearer ' . $jwt1, $headers['Authorization']);

        Carbon::setTestNow(Carbon::now()->addMinutes(6));

        $headers = $authentication->addAuthenticationHeaders([]);

        $this->assertEquals('Bearer ' . $jwt2, $headers['Authorization']);

        $this->assertCount(2, $this->container);

    }

    private function newHttpClientWithResponses(array $responses): Client
    {
        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }
}
