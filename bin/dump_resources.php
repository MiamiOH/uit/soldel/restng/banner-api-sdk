<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 11:12 AM
 */

use MiamiOH\BannerApi\ApiFactory;

include_once __DIR__ . '/../vendor/autoload.php';

$config = parse_ini_file(__DIR__ . '/config.ini', true);

#\MiamiOH\BannerApi\LogManager::setConfiguration(__DIR__ . '/logconfig.yml');

$api = ApiFactory::getBannerApi(
    $config['BannerAPI']['base_url'],
    $config['BannerAPI']['username'],
    $config['BannerAPI']['password']
);

$studentApi = $api->getStudentApiPackage();
$integrationApi = $api->getIntegrationApiPackage();

$resources = [];

/** @var \MiamiOH\BannerApi\Api\ApiPackage $package */
foreach ([$studentApi, $integrationApi] as $package) {
    foreach ($package->getResourceDefinitions() as $definition) {
        $resource = [
            'source' => $package->getName(),
            'name' => $definition->getName(),
            'versions' => []
        ];

        foreach ($definition->getRepresentations() as $representation) {
            $resource['versions'][] = $representation->getMediaType();
        }

        $resources[] = $resource;
    }
}

print_r($resources);
