<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 11:12 AM
 */

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\Guid;

include_once __DIR__ . '/../vendor/autoload.php';

$config = parse_ini_file(__DIR__ . '/config.ini', true);

//\MiamiOH\BannerApi\LogManager::setLogLevel('DEBUG');
//\MiamiOH\BannerApi\LogManager::setLogFile('my_log_file.log');
\MiamiOH\BannerApi\LogManager::setConfiguration(__DIR__ . '/logconfig.yml');

$api = ApiFactory::getBannerApi(
    $config['BannerAPI']['base_url'],
    $config['BannerAPI']['username'],
    $config['BannerAPI']['password']
);

$courseResource = $api->getCoursesResource();

$courseList = $courseResource->find()->withNumber('101')->execute();

print "Course List for number 101\n";
foreach ($courseList as $course) {
    print "Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
}

$courseId = '82e3958e-1365-4566-8c9b-6eb668b16177';

$course = $courseResource->findOne()->withId(new Guid($courseId))->execute();

print "\nCourse 82e3958e-1365-4566-8c9b-6eb668b16177\n";

print "Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
