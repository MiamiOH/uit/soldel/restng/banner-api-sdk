<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 11:12 AM
 */

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;

// The SDK class files are autoloaded by composer
include_once __DIR__ . '/../vendor/autoload.php';

// The credentials and configuration can come from whatever source is
// appropriate for your deployment.
$config = parse_ini_file(__DIR__ . '/config.ini', true);

BannerResource::setResourceConfig(
    yaml_parse_file(__DIR__ . '/resources-dist.yml')['resources']
);

// The ApiFactory class can build an Api object configured for direct
// Banner API access, as below, or can return an Api object configured
// to use Ethos Integration Hub by calling ApiFactory::getEihApi() with
// the base URL and API Token.
$api = ApiFactory::getBannerApi(
    $config['BannerAPI']['base_url'],
    $config['BannerAPI']['username'],
    $config['BannerAPI']['password']
);

// The Api object builds and returns resource objects. The resource
// objects provide the interface to the remote resource.
$courseResource = $api->getCoursesResource();

// Banner APIs make extensive use of GUIDs. GUID values are passed to
// and from the API as strings but the Guid value object must be used
// within the SDK. Guid objects can be used as strings.
$courseId = new Guid('41dd136d-2d11-42f0-9738-5b0f4eea3423');

print "Find course by id: $courseId\n\n";

// Resource objects provide methods for interacting with the remote
// API resource.
$course = $courseResource->findOne()->withId($courseId)->execute();

// The returned model has methods for working with its attributes. Some resources
// contain complex data structures, which themselves may be supporting classes.
print "Course " . $course->getId() . "\n";
print "Title: " . $course->getTitle() . "\n";
print "Description: " . $course->getDescription() . "\n";
print "Number: " . $course->getNumber() . "\n";
print "SubjectId: " . $course->getSubject() . "\n";
