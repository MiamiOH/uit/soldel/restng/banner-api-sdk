<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 11:12 AM
 */

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;

include_once __DIR__ . '/../vendor/autoload.php';

$config = parse_ini_file(__DIR__ . '/config.ini', true);

\MiamiOH\BannerApi\LogManager::setLogLevel('INFO');

BannerResource::setResourceConfig(
    yaml_parse_file(__DIR__ . '/resources-dist.yml')['resources']
);

$api = ApiFactory::getBannerApi(
    $config['BannerAPI']['base_url'],
    $config['BannerAPI']['username'],
    $config['BannerAPI']['password']
);

$subjectResource = $api->getSubjectsResource();

$subjectList = $subjectResource->find()->execute();

$subjects = [];

foreach ($subjectList as $subject) {
    $subjects[(string) $subject->getId()] = $subject->getAbbreviation();
}

$courseResource = $api->getCoursesResource();

$courseList = $courseResource->find()->execute();

print "Courses returned for default version: " . $courseResource->getVersion() . "\n\n";
foreach ($courseList as $course) {
    $subjectCode = $subjects[(string) $course->getSubject()];
    print "Course: " . $course->getTitle() . ' ' . $subjectCode . ' ' .
        $course->getNumber() . "\n";
}

$version = 6;
$courseResource = $api->getCoursesResource($version);

$courseList = $courseResource->find()->execute();

print "##########################\n";
print "Courses returned for default version: " . $version . "\n\n";
foreach ($courseList as $course) {
    $subjectCode = $subjects[(string) $course->getSubject()];
    print "Course: " . $course->getTitle() . ' ' . $subjectCode . ' ' .
        $course->getNumber() . "\n";
}
