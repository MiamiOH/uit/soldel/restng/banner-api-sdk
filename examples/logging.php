<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 11:12 AM
 */

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Exception\ResourceModelNotFoundException;
use MiamiOH\BannerApi\Guid;

include_once __DIR__ . '/../vendor/autoload.php';

$config = parse_ini_file(__DIR__ . '/config.ini', true);

// Change the default behavior.
\MiamiOH\BannerApi\LogManager::setLogLevel('INFO');
\MiamiOH\BannerApi\LogManager::setLogFile('my_log_file.log');

// Or provide a complete Monolog Cascade configuration (replaces all defaults).
if (file_exists(__DIR__ . '/logconfig.yml')) {
    \MiamiOH\BannerApi\LogManager::setConfiguration(__DIR__ . '/logconfig.yml');
}

BannerResource::setResourceConfig(
    yaml_parse_file(__DIR__ . '/resources-dist.yml')['resources']
);

// Getting an Api object from the ApiFactory will configure the loggers, so
// make sure you set your configuration first.
$api = ApiFactory::getBannerApi(
    $config['BannerAPI']['base_url'],
    $config['BannerAPI']['username'],
    $config['BannerAPI']['password']
);

$courseResource = $api->getCoursesResource();

// GuzzleHTTP requests/responses are logged (DEBUG log level) using the 'rest'
// logger channel. The current implementation logs to a single line.
$courseList = $courseResource->find()->withNumber('101')->execute();

print "Course List for number 101\n";
foreach ($courseList as $course) {
    print "Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
}

$courseId = '82e3958e-1365-4566-8c9b-6eb668b16177';

$course = $courseResource->findOne()->withId(new Guid($courseId))->execute();

print "\nCourse $courseId\n";

print "Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";

// This course does not exist, and will result in ResourceModelNotFoundException.
// You can handle the exception. The http response will still be logged.
$courseId = '82e3958e-1365-4566-8c9b-6eb668b16178';

try {
    $course = $courseResource->findOne()->withId(new Guid($courseId))->execute();
} catch (ResourceModelNotFoundException $e) {
    print "Could not find $courseId\n";
}