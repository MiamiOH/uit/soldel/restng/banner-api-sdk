<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/23/17
 * Time: 11:12 AM
 */

use MiamiOH\BannerApi\ApiFactory;
use MiamiOH\BannerApi\BannerResource;
use MiamiOH\BannerApi\Guid;

include_once __DIR__ . '/../vendor/autoload.php';

$config = parse_ini_file(__DIR__ . '/config.ini', true);

\MiamiOH\BannerApi\LogManager::setLogLevel('INFO');

BannerResource::setResourceConfig(
    yaml_parse_file(__DIR__ . '/resources-dist.yml')['resources']
);

$api = ApiFactory::getBannerApi(
    $config['BannerAPI']['base_url'],
    $config['BannerAPI']['username'],
    $config['BannerAPI']['password']
);

$subjectResource = $api->getSubjectsResource();

$subjectList = $subjectResource->find()->execute();

$subjects = [];

foreach ($subjectList as $subject) {
    $subjects[(string) $subject->getId()] = $subject->getAbbreviation();
}

$courseResource = $api->getCoursesResource();

// Get up to 100 records from a large collection. This would go into an infinite
// loop if the collection had fewer than 100 entries.
$offset = 0;
$limit = 20;
while ($offset < 100) {
    $courseList = $courseResource->find()->withOffset($offset)->withLimit($limit)->execute();

    foreach ($courseList as $course) {
        $subjectCode = $subjects[(string) $course->getSubject()];
        print "Course: " . $course->getTitle() . ' ' . $subjectCode . ' ' .
            $course->getNumber() . "\n";
    }

    $offset += $limit;
}

// We can manage pages on our own to fetch an entire collection.
$count = 0;
do {
    $courseList = $courseResource->find()->withNumber('101')->withOffset($offset)->withLimit($limit)->execute();

    if ($offset === 0) {
        print "Found " . $courseList->getTotalCount() . " courses\n\n";
    }

    foreach ($courseList as $course) {
        $count++;
        print "$count Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
    }

    $offset += $limit;

} while ($courseList->getPageOffset() + $courseList->getPageMaxSize() < $courseList->getTotalCount());

// The easy way is to let a Pager class handle pages for us.
$offset = 0;
$limit = 30;

print "\n\nFind all courses with number 101\n\n";

$pager = $courseResource->find()->withNumber('101')->withOffset($offset)->withLimit($limit)->pager();

$count = 0;
while ($courseList = $pager->next()) {
    foreach ($courseList as $course) {
        $count++;
        print "$count Title: " . $course->getTitle() . ' (' . $course->getId() . ")\n";
    }
}

